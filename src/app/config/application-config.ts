export const config = {
  // 'KOPBI_MASTER_BASE_URL': 'http://178.128.85.189:5554',
  // 'KOPBI_MEMBER_BASE_URL': 'http://178.128.85.189:5555',
  // 'KOPBI_PENGAJUAN_BASE_URL': 'http://178.128.85.189:5556',
  // 'KOPBI_PINJAMAN_BASE_URL': 'http://178.128.85.189:5557',

  // 'KOPBI_MASTER_BASE_URL': 'http://192.168.0.115:8010/kopbi-master',
  // 'KOPBI_MEMBER_BASE_URL': 'http://192.168.0.115:8010/kopbi-agt',
  // 'KOPBI_PENGAJUAN_BASE_URL': 'http://192.168.0.115:8010/kopbi-pengajuan',
  // 'KOPBI_PINJAMAN_BASE_URL': 'http://192.168.0.115:8010/kopbi-pinjaman',
  // 'KOPBI_ARUSKAS_BASE_URL': 'http://192.168.0.115:8010/kopbi-aruskas'

  // 'KOPBI_MASTER_BASE_URL': 'http://solusi.kopbi.or.id:8888/kopbi-master',
  // 'KOPBI_MEMBER_BASE_URL': 'http://solusi.kopbi.or.id:8888/kopbi-agt',
  // 'KOPBI_PENGAJUAN_BASE_URL': 'http://solusi.kopbi.or.id:8888/kopbi-pengajuan',
  // 'KOPBI_PINJAMAN_BASE_URL': 'http://solusi.kopbi.or.id:8888/kopbi-pinjaman',
  // 'KOPBI_ARUSKAS_BASE_URL': 'http://solusi.kopbi.or.id:8888/kopbi-aruskas',
  // 'KOPBI_REPORT_BASE_URL': 'http://solusi.kopbi.or.id:8888/kopbi-report',
  // 'KOPBI_IMAGES_BASE_URL': 'http://solusi.kopbi.or.id:8888/kopbi-images'

  // 'KOPBI_MASTER_BASE_URL': 'http://solusi.kopbi.or.id:8889/kopbi-master',
  // 'KOPBI_MEMBER_BASE_URL': 'http://solusi.kopbi.or.id:8889/kopbi-agt',
  // 'KOPBI_PENGAJUAN_BASE_URL': 'http://solusi.kopbi.or.id:8889/kopbi-pengajuan',
  // 'KOPBI_PINJAMAN_BASE_URL': 'http://solusi.kopbi.or.id:8889/kopbi-pinjaman',
  // 'KOPBI_ARUSKAS_BASE_URL': 'http://solusi.kopbi.or.id:8889/kopbi-aruskas',
  // 'KOPBI_REPORT_BASE_URL': 'http://solusi.kopbi.or.id:8889/kopbi-report',
  // 'KOPBI_IMAGES_BASE_URL': 'http://solusi.kopbi.or.id:8889/kobi-images'

  'KOPBI_MASTER_BASE_URL': 'http://solusi.kopbi.or.id/api/kopbi-master',
  'KOPBI_MEMBER_BASE_URL': 'http://solusi.kopbi.or.id/api/kopbi-agt',
  'KOPBI_PENGAJUAN_BASE_URL': 'http://solusi.kopbi.or.id/api/kopbi-pengajuan',
  // 'KOPBI_PENGAJUAN_BASE_URL': 'http://192.168.0.120:8010/kopbi-pengajuan',
  'KOPBI_PINJAMAN_BASE_URL': 'http://solusi.kopbi.or.id/api/kopbi-pinjaman',
  'KOPBI_ARUSKAS_BASE_URL': 'http://solusi.kopbi.or.id/api/kopbi-aruskas',
  'KOPBI_REPORT_BASE_URL': 'http://solusi.kopbi.or.id/api/kopbi-report',
  'KOPBI_IMAGES_BASE_URL': 'http://solusi.kopbi.or.id/api/kobi-images',
  'KOPBI_BASE_URL': 'http://solusi.kopbi.or.id/api'
};

export const configKOPBI = {
  persentaseBunga: 8,
  biayaAdmin: 5000
};
