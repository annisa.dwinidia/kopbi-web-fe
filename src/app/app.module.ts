import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { LocationStrategy, HashLocationStrategy, CommonModule } from '@angular/common';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { LoaderComponent } from './views/loader/loader.component';

import { OnlyNumberDirective } from './directive/only-number.directive';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ModalModule, PaginationModule, BsDatepickerModule} from 'ngx-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { FilterPipe } from './pipe/filter.pipe';

import { DashboardComponent } from './views/dashboard/dashboard.component';
import { AgeFilterPipe } from './pipe/age-filter.pipe';
import { MarriageFilterPipe } from './pipe/marriage-filter.pipe';
import { OrderByPipe } from './pipe/order-by.pipe';
import { PengajuanComponent } from './views/transaksi/pengajuan/pengajuan.component';
import { TmbPengajuanComponent } from './views/transaksi/tmb-pengajuan/tmb-pengajuan.component';
import { SimpananComponent } from './views/transaksi/simpanan/simpanan.component';
import { KasComponent } from './views/transaksi/kas/kas.component';
import { ReportComponent } from './views/transaksi/report/report.component';
import { TmbSimpananComponent } from './views/transaksi/tmb-simpanan/tmb-simpanan.component';
import { PengajuanAnggotaComponent } from './views/transaksi/pengajuan-anggota/pengajuan-anggota.component';
import { PerusahaanComponent } from './views/master/perusahaan/perusahaan.component';
import { AnggotaComponent } from './views/master/anggota/anggota.component';
import { SimpananAnggotaComponent } from './views/transaksi/simpanan-anggota/simpanan-anggota.component';
import { PinjamanComponent } from './views/transaksi/pinjaman/pinjaman.component';
import { PinjamanAnggotaComponent } from './views/transaksi/pinjaman-anggota/pinjaman-anggota.component';
import { AngsuranComponent } from './views/transaksi/angsuran/angsuran.component';
import { TmbAngsuranComponent } from './views/transaksi/tmb-angsuran/tmb-angsuran.component';
import { StatusPengajuanPipe } from './pipe/status-pengajuan.pipe';
import { PinjamanBelumLunasComponent } from './views/transaksi/pinjaman-belum-lunas/pinjaman-belum-lunas.component';
import {ProfileComponent} from './views/master/profile/profile.component';
import {BootstrapSelectDirective} from './bootstrap-select/bootstrap-select.directive';
import {CompanySelectComponent} from './bootstrap-select/company-select/company-select.component';
import {ItemSelectComponent} from './bootstrap-select/item-select/item-select.component';
import {LoaderLoginComponent} from './views/loader-login/loader-login.component';
import { TagihanPinjamanComponent } from './views/transaksi/tagihan-pinjaman/tagihan-pinjaman.component';
import { FeePerusahaanComponent } from './views/transaksi/fee-perusahaan/fee-perusahaan.component';
import {MemberSelectComponent} from './bootstrap-select/member-select/member-select.component';
import { DataAnggotaComponent } from './views/transaksi/data-anggota/data-anggota.component';
import { StatusPinjamanPipe } from './pipe/status-pinjaman.pipe';
import { BankComponent } from './views/master/bank/bank.component';
import { KonfederasiComponent } from './views/master/konfederasi/konfederasi.component';
import { StatusAnggotaPipe } from './pipe/status-anggota.pipe';
import { RoleAnggotaPipe } from './pipe/role-anggota.pipe';
import {NumericDirective} from './directive/numeric.directive';
import { BarangComponent } from './views/master/barang/barang.component';
import { TruncatePipe } from './pipe/truncate.pipe';
import { RemovewhitespacesPipe } from './pipe/removewhitespaces.pipe';
import { TagihanSimpananBaruComponent } from './views/transaksi/tagihan-simpanan-baru/tagihan-simpanan-baru.component';
import { TagihanSimpananAktifComponent } from './views/transaksi/tagihan-simpanan-aktif/tagihan-simpanan-aktif.component';
import { PenarikanComponent } from './views/transaksi/penarikan/penarikan.component';
import { TmbPenarikanComponent } from './views/transaksi/tmb-penarikan/tmb-penarikan.component';
import { KendaraanComponent } from './views/master/kendaraan/kendaraan.component';
import { PerumahanComponent } from './views/master/perumahan/perumahan.component';
import { KomponenComponent } from './views/master/komponen/komponen.component';
import { KontenComponent } from './views/master/konten/konten.component';
import { ChangePasswordComponent } from './views/change-password/change-password.component';
import { PenarikanAnggotaComponent } from './views/transaksi/penarikan-anggota/penarikan-anggota.component';
import { InformasiComponent } from './views/master/informasi/informasi.component';
import { CatatanComponent } from './views/master/catatan/catatan.component';
import { KegiatanComponent } from './views/master/kegiatan/kegiatan.component';
import { IklanComponent } from './views/master/iklan/iklan.component';
import { NotifikasiComponent } from './views/master/notifikasi/notifikasi.component';
import { LaporanComponent } from './views/master/laporan/laporan.component';
import { RegistrasiComponent } from './views/transaksi/registrasi/registrasi.component';
import { IklanMerchantComponent } from './views/master/iklan-merchant/iklan-merchant.component';
import { VerifikasiComponent } from './views/transaksi/verifikasi/verifikasi.component';
import { LaporanSimpananComponent } from './views/transaksi/laporan-simpanan/laporan-simpanan.component';
import { PengajuanKonsumerComponent } from './views/transaksi/pengajuan-konsumer/pengajuan-konsumer.component';
import { TmbPengajuanKonsumerComponent } from './views/transaksi/tmb-pengajuan-konsumer/tmb-pengajuan-konsumer.component';

@NgModule({
  imports: [
    BrowserModule,
    ModalModule.forRoot(),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SelectDropDownModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    CommonModule,
    ButtonsModule,
    PaginationModule.forRoot(),
    NgbCarouselModule,
    BsDatepickerModule.forRoot(),
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
    LoaderComponent,
    LoaderLoginComponent,
    OnlyNumberDirective,
    FilterPipe,
    DashboardComponent,
    AgeFilterPipe,
    MarriageFilterPipe,
    OrderByPipe,
    PengajuanComponent,
    TmbPengajuanComponent,
    SimpananComponent,
    KasComponent,
    ReportComponent,
    TmbSimpananComponent,
    PengajuanAnggotaComponent,
    PerusahaanComponent,
    AnggotaComponent,
    SimpananAnggotaComponent,
    PinjamanComponent,
    PinjamanAnggotaComponent,
    AngsuranComponent,
    TmbAngsuranComponent,
    StatusPengajuanPipe,
    PinjamanBelumLunasComponent,
    ProfileComponent,
    BootstrapSelectDirective,
    CompanySelectComponent,
    ItemSelectComponent,
    TagihanPinjamanComponent,
    FeePerusahaanComponent,
    MemberSelectComponent,
    DataAnggotaComponent,
    StatusPinjamanPipe,
    BankComponent,
    KonfederasiComponent,
    StatusAnggotaPipe,
    RoleAnggotaPipe,
    NumericDirective,
    BarangComponent,
    TruncatePipe,
    RemovewhitespacesPipe,
    TagihanSimpananBaruComponent,
    TagihanSimpananAktifComponent,
    PenarikanComponent,
    TmbPenarikanComponent,
    KendaraanComponent,
    PerumahanComponent,
    KomponenComponent,
    KontenComponent,
    ChangePasswordComponent,
    PenarikanAnggotaComponent,
    InformasiComponent,
    CatatanComponent,
    KegiatanComponent,
    IklanComponent,
    NotifikasiComponent,
    LaporanComponent,
    RegistrasiComponent,
    IklanMerchantComponent,
    VerifikasiComponent,
    LaporanSimpananComponent,
    PengajuanKonsumerComponent,
    TmbPengajuanKonsumerComponent
  ],
  exports: [LoaderComponent,
    BootstrapSelectDirective],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
