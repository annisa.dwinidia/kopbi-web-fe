/**
 * Created by Annisa D. Nidia on 7/20/2019
 */

export class Angsuran {
  kodeAngsuran: string;
  nomorPinjaman: string;
  tanggalJatuhTempo: string;
  angsuranKe: string;
  nominalAngsuran: string;
  persenBunga: string;
  nominalBunga: string;
  totalBunga: string;
  biayaAdmin: string;
  totalBayar: string;
  status: string;
  angsuranPokok: string;
}
