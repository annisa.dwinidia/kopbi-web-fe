/**
 * Created by Annisa D. Nidia on 7/17/2019
 */

export class Simpanan {
  idSimpanan: string;
  tanggalSimpanan: Date;
  nomorKtp: string;
  nomorAnggota: string;
  nama: string;
  nomorNik: string;
  nomorHp: string;
  kodePerusahaan: string;
  namaPerusahaan: string;
  alamatPerusahaan: string;
  emailPerusahaan: string;
  kodeSimpanan: string;
  namaSimpanan: string;
  nominalSimpanan: number;
  jenisIuran: string;
  ketIuran: string;
  kodeMediaIuran: string;
  mediaIuran: string;
  bulan: string;
  tahun: string;
  keterangan: string;
  kodeUser: string;
  namaUser: string;
  tanggalUpdate: Date;
  statusAktif: string;
}
