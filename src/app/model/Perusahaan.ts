/**
 * Created by Annisa D. Nidia on 6/30/2019
 */

export class Perusahaan {
    id: number;
    kodePerusahaan: string;
    namaPerusahaan: string;
    alamatPerusahaan: string;
    emailPerusahaan: string;
    registrasiPerusahaan: string;
    teleponPerusahaan: string;


    nomorPerusahaan: string;
    kodeBidangUsaha: string;
    faxPerusahaan: string;
    npwpPerusahaan: string;
    kodeKonfederasi: string;
    namaKonfederasi: string;
    nikPejabat: string;
    namaPejabat: string;
    jabatanPejabat: string;
    extentionPejabat: string;
    hpPejabat: string;
}
