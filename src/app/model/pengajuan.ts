export class Pengajuan {
  kodePengajuan: string;
  kodeTipePengajuan: string;
  tipePengajuan: string;
  nomorKtp: string;
  tanggalPengajuan: string;
  lamaAngsuran: string;
  nominalPengajuan: string;
  nominalAngsuran: string;
  persenBunga: string;
  nominalBunga: string;
  totalBunga: string;
  biayaAdmin: string;
  kodeBarang: string;
  namaBarang: string;
  ambilDariKas: string;
  ketKas: string;
  keterangan: string;
  statusPengajuan: string;
  statusPinjaman: string;
  tanggalPerubahan: string;
  tanggalTempo: string;
  tanggalJatuhTempo: string;
  nomorPinjaman: string;
  simpananWajib: string;
  simpananPokok: string;
  simpananSukarela: string;
}
