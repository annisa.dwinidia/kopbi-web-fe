/**
 * Created by Annisa D. Nidia on 7/3/2019
 */

export class Barang {
  id: number;
  kodeBarang: string;
  namaBarang: string;
  harga: number;
  hargaBeli: number;
  keterangan: string;
  stokBarang: number;
  namaSuplier: string;
  teleponSuplier: string;
}
