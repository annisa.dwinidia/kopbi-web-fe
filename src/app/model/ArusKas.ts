/**
 * Created by Annisa D. Nidia on 7/28/2019
 */

export class ArusKas {
  id: string;
  tanggalTransaksi: string;
  nominal: string;
  keterangan: string;
  akun: string;
  kodeDariKas: string;
  dariKas: string;
  kodeUntukKas: string;
  untukKas: string;
  jenisTransaksi: string;
  ketJenisTransaksi: string;
  ketKas: string;
  tanggalUpdate: string;
  userUpdate: string;
  statusAktif: string;
}
