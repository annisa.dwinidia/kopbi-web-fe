/**
 * Created by Annisa D. Nidia on 7/7/2019
 */

export class Pinjaman {
  kodePinjaman: string;
  tanggalPengajuan: string;
  nomorPinjaman: string;
  kodeTipePengajuan: string;
  tipePengajuan: string;
  nomorKtp: string;
  nomorAnggota: string;
  nama: string;
  nomorNik: string;
  nomorHp: string;
  kodePerusahaan: string;
  namaPerusahaan: string;
  alamatPerusahaan: string;
  emailPerusahaan: string;
  lamaAngsuran: string;
  nominalAngsuran: string;
  persenBunga: string;
  nominalBunga: string;
  totalBunga: string;
  biayaAdmin: string;
  kodeBarang: string;
  namaBarang: string;
  ambilDariKas: string;
  ketKas: string;
  keterangan: string;
  statusPengajuan: string;
  tanggalPerubahan: string;
  tanggalTempo: string;
  tanggalJatuhTempo: string;
  sisaPinjaman: string;
  angsuranKe: string;
  sisaAngsuran: string;
  kodeUser: string;
  namaUser: string;
  tanggalUpdate: string;
  nominalPinjaman: string;
  tanggalPinjaman: string;
}
