export class Bank {
  id: string;
  kodeBank: string;
  namaBank: string;
  cabangPembuka: string;
  nomorRekening: string;
}
