/**
 * Created by Annisa D. Nidia on 9/1/2019
 */

export class Konfederasi {
  id: string;
  kodeKonfederasi: string;
  namaKonfederasi: string;
  alamatKonfederasi: string;
  emailKonfederasi: string;
  teleponKonfederasi: string;
  registrasiKonfederasi: string;
}
