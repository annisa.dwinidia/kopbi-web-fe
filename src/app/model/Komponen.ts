/**
 * Created by Annisa D. Nidia on 4/12/2020
 */

export class Komponen {
  id: string;
  kode: string;
  keterangan: string;
  tipe: string;
  nominal: string;
}

