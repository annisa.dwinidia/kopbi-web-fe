export const navItemsADM = [
  {
    name: 'Beranda',
    url: '/home/dashboard',
    icon: 'fa fa-home'
  },
  {
    name: 'Transaksi Kas',
    url: '/home/kas/list',
    icon: 'fa fa-handshake-o'
  },
  {
    name: 'Simpanan',
    url: '/home/simpanan',
    icon: 'fa fa-suitcase'
  },
  {
    name: 'Pinjaman',
    url: '/home/pinjaman',
    icon: 'fa fa-money',
  },
  {
    name: 'Pengajuan',
    url: '/home/pengajuan',
    icon: 'fa fa-clipboard ',
    children: [
      {
        name: 'Pengajuan Pinjaman',
        url: '/home/pengajuan/pinjaman',
        icon: 'fa'
        // icon: 'fa fa-clipboard'
      },
      {
        name: 'Pengajuan Penarikan',
        url: '/home/pengajuan/penarikan',
        // icon: 'fa fa-clipboard'
        icon: 'fa'
      },
      {
        name: 'Pengajuan Konsumer',
        url: '/home/pengajuan/pengajuan-konsumer',
        // icon: 'fa fa-clipboard'
        icon: 'fa'
      },
    ]
  },
  {
    name: 'Tagihan',
    url: '/home/tagihan',
    icon: 'fa fa-money',
    children: [
      {
        name: 'Simpanan',
        url: '/home/tagihan/simpanan',
        // icon: 'fa fa-suitcase',
        icon: 'fa',
        children: [
          {
            name: 'Anggota Baru',
            url: '/home/tagihan/simpanan/baru',
            // icon: 'fa fa-suitcase'
            icon: 'fa'
          },
          {
            name: 'Anggota Aktif',
            url: '/home/tagihan/simpanan/aktif',
            // icon: 'fa fa-suitcase'
            icon: 'fa'
          }
        ]
      },
      {
        name: 'Pinjaman',
        url: '/home/tagihan/pinjaman',
        // icon: 'fa fa-money'
        icon: 'fa'
      },
    ]
  },
  // {
  //   name: 'Pinjaman',
  //   url: '/pinjaman',
  //   icon: 'fa fa-money',
  //   children: [
  //     {
  //       name: 'Pinjaman Lunas',
  //       url: '/pinjaman/lunas',
  //       icon: 'fa fa-money'
  //     },
  //     {
  //       name: 'Pinjaman Belum Lunas',
  //       url: '/pinjaman/belum-lunas',
  //       icon: 'fa fa-money'
  //     }
  //   ]
  // },
  {
    name: 'Laporan',
    url: '/home/laporan',
    icon: 'fa fa-file',
    children: [
      {
        name: 'Laporan Transaksi',
        url: '/home/laporan/transaksi',
        // icon: 'fa fa-file'
        icon: 'fa'
      },
      {
        name: 'Laporan Simpanan',
        url: '/home/laporan/simpanan',
        // icon: 'fa fa-file'
        icon: 'fa'
      }
    ]
  },
  {
    name: 'Registrasi Anggota',
    url: '/home/registrasi/anggota',
    icon: 'fa fa-user'
  },
  {
    name: 'Master Data',
    url: '/master',
    icon: 'fa fa-database',
    children: [
      {
        name: 'Data Anggota',
        url: '/master/anggota',
        // icon: 'fa fa-user'
        icon: 'fa'
      },
      {
        name: 'Data Bank',
        url: '/master/bank',
        // icon: 'fa fa-money'
        icon: 'fa'
      },
      {
        name: 'Data Barang',
        url: '/master/barang',
        // icon: 'fa fa-shopping-cart'
        icon: 'fa'
      },
      // {
      //   name: 'Master Informasi',
      //   url: '/master/informasi',
      //   icon: 'fa fa-info-circle'
      // },
      {
        name: 'Data Kendaraan',
        url: '/master/kendaraan',
        // icon: 'fa fa-car'
        icon: 'fa'
      },
      {
        name: 'Data Komponen',
        url: '/master/komponen',
        // icon: 'fa fa-cogs'
        icon: 'fa'
      },
      {
        name: 'Data Konfederasi',
        url: '/master/konfederasi',
        // icon: 'fa fa-list'
        icon: 'fa'
      },
      {
        name: 'Data Perumahan',
        url: '/master/perumahan',
        // icon: 'fa fa-home'
        icon: 'fa'
      },
      {
        name: 'Data Perusahaan',
        url: '/master/perusahaan',
        // icon: 'fa fa-bank'
        icon: 'fa'
      }
    ]
  },
  {
    name: 'Master Konten',
    url: '/konten',
    icon: 'fa fa-image',
    children: [
      {
        name: 'Konten Informasi',
        url: '/konten/informasi',
        // icon: 'fa fa-image'
        icon: 'fa'
      },
      {
        name: 'Konten Kegiatan',
        url: '/konten/kegiatan',
        // icon: 'fa fa-image'
        icon: 'fa'
      },
      {
        name: 'Konten Catatan',
        url: '/konten/catatan',
        // icon: 'fa fa-image'
        icon: 'fa'
      },
      {
        name: 'Konten Iklan',
        url: '/konten/iklan',
        // icon: 'fa fa-image'
        icon: 'fa'
      },
      {
        name: 'Konten Iklan Merchant',
        url: '/konten/merchant',
        // icon: 'fa fa-image'
        icon: 'fa'
      },
      {
        name: 'Konten Notifikasi',
        url: '/konten/notifikasi',
        // icon: 'fa fa-image'
        icon: 'fa'
      },
      {
        name: 'Konten Laporan',
        url: '/konten/laporan',
        // icon: 'fa fa-image'
        icon: 'fa'
      }
    ]
  },
  // {
  //   name: 'Logout',
  //   url: '/login',
  //   icon: 'icon-logout',
  //   class: 'mt-auto',
  //   variant: 'danger',
  //   attributes: { target: '_blank', rel: 'noopener' }
  // }
];

export const navItemsHRD = [
  {
    name: 'Beranda',
    url: '/home/dashboard',
    icon: 'fa fa-home'
  },
  // {
  //   name: 'Pengajuan',
  //   url: '/pengajuan',
  //   icon: 'fa fa-clipboard',
  // },
  {
    name: 'Pengajuan',
    url: '/home/pengajuan',
    icon: 'fa fa-clipboard ',
    children: [
      {
        name: 'Pengajuan Pinjaman',
        url: '/home/pengajuan/pinjaman',
        icon: 'fa fa-clipboard'
      },
      {
        name: 'Pengajuan Penarikan',
        url: '/home/pengajuan/penarikan',
        icon: 'fa fa-clipboard'
      },
      {
        name: 'Pengajuan Konsumer',
        url: '/home/pengajuan/pengajuan-konsumer',
        icon: 'fa fa-clipboard'
      },
    ]
  },
  {
    name: 'Tagihan',
    url: '/home/tagihan',
    icon: 'fa fa-money',
    children: [
      {
        name: 'Simpanan',
        url: '/home/tagihan/simpanan',
        icon: 'fa fa-suitcase',
        children: [
          {
            name: 'Anggota Baru',
            url: '/home/tagihan/simpanan/baru',
            icon: 'fa fa-suitcase'
          },
          {
            name: 'Anggota Aktif',
            url: '/home/tagihan/simpanan/aktif',
            icon: 'fa fa-suitcase'
          }
        ]
      },
      {
        name: 'Pinjaman',
        url: '/home/tagihan/pinjaman',
        icon: 'fa fa-money'
      },
    ]
  },
  {
    name: 'Fee Perusahaan',
    url: '/home/fee-perusahaan',
    icon: 'fa fa-dollar',
  },
];

export const navItemsAGT = [
  {
    name: 'Beranda',
    url: '/home/dashboard',
    icon: 'fa fa-home'
  },
  {
    name: 'Simpanan',
    url: '/home/simpanan/anggota',
    icon: 'fa fa-suitcase'
  },
  {
    name: 'Pinjaman',
    url: '/home/pinjaman/anggota',
    icon: 'fa fa-money',
  },
  {
    name: 'Pengajuan Pinjaman',
    url: '/home/pengajuan/pinjaman/anggota',
    icon: 'fa fa-clipboard',
  },
  {
    name: 'Penarikan Simpanan',
    url: '/home/pengajuan/penarikan/anggota',
    icon: 'fa fa-handshake-o',
  }
];

export const navItemsPENGAWAS = [
  {
    name: 'Beranda',
    url: '/home/dashboard',
    icon: 'fa fa-home'
  },
  // {
  //   name: 'Pengajuan',
  //   url: '/pengajuan',
  //   icon: 'fa fa-clipboard',
  // },
  {
    name: 'Pengajuan',
    url: '/home/pengajuan',
    icon: 'fa fa-clipboard ',
    children: [
      {
        name: 'Pengajuan Pinjaman',
        url: '/home/pengajuan/pinjaman',
        icon: 'fa fa-clipboard'
      },
      {
        name: 'Pengajuan Penarikan',
        url: '/home/pengajuan/penarikan',
        icon: 'fa fa-clipboard'
      },
      {
        name: 'Pengajuan Konsumer',
        url: '/home/pengajuan/pengajuan-konsumer',
        icon: 'fa fa-clipboard'
      },
    ]
  },
  {
    name: 'Pinjaman',
    url: '/home/pinjaman',
    icon: 'fa fa-money',
  },
  {
    name: 'Tagihan',
    url: '/home/tagihan',
    icon: 'fa fa-money',
    children: [
      {
        name: 'Simpanan',
        url: '/home/tagihan/simpanan',
        icon: 'fa fa-suitcase',
        children: [
          {
            name: 'Anggota Baru',
            url: '/home/tagihan/simpanan/baru',
            icon: 'fa fa-suitcase'
          },
          {
            name: 'Anggota Aktif',
            url: '/home/tagihan/simpanan/aktif',
            icon: 'fa fa-suitcase'
          }
        ]
      },
      {
        name: 'Pinjaman',
        url: '/home/tagihan/pinjaman',
        icon: 'fa fa-money'
      },
    ]
  },
  {
    name: 'Daftar Anggota',
    url: '/home/data-anggota',
    icon: 'fa fa-users'
  },
];
