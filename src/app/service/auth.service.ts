import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private myRoute: Router) { }
  sendToken(user: string) {
    localStorage.setItem('user', user);
  }

  sendJWTToken(jwtToken: string) {
    localStorage.setItem('jwtToken', jwtToken);
  }

  getToken() {
    return localStorage.getItem('user');
  }

  getJWTToken() {
    return localStorage.getItem('jwtToken');
  }
  isLoggednIn() {
    return this.getToken() !== null;
  }
  logout() {
    this.myRoute.navigate(['/login']);
    localStorage.clear();
  }
}
