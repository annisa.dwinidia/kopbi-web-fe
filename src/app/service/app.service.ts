import { Injectable } from '@angular/core';
import {User} from '../model/user';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {config} from '../config/application-config';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  header;

  constructor(private httpClient: HttpClient, private auth: AuthService) {
  }

  getHeaders() {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    headers = headers.set('token', 'U2FsdGVkX19emypgqSLb6nLxUO5CO3eG7avTQXU045E=');
    headers = headers.set('jwtToken', this.auth.getJWTToken());
    this.header = headers;
  }

  // getUserInfo(username, password): Observable<any> {
  //   return this.httpClient.post(config.KOPBI_MEMBER_BASE_URL + '/login/' + username + '/' + password, '');
  // }

  getUserInfo(userDetail, header): Observable<any> {
    return this.httpClient.post(config.KOPBI_MEMBER_BASE_URL + '/login', userDetail, {headers: header});
  }

  getListPengajuan(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PENGAJUAN_BASE_URL + '/list-pengajuan', param, {headers: this.header});
  }

  getListPengajuanAnggota(nik, param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PENGAJUAN_BASE_URL + '/list-pengajuan/' + nik, param, {headers: this.header});
  }

  getListPerusahaan(): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/list-perusahaan', '{}', {headers: this.header});
  }

  doPostPerusahaan(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/post-perusahaan', param, {headers: this.header});
  }

  getListKonfederasi(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/list-konfederasi', param, {headers: this.header});
  }

  doPostKonfederasi(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/post-konfederasi', param, {headers: this.header});
  }

  getListBank(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/list-bank', param, {headers: this.header});
  }

  doPostBank(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/post-bank', param, {headers: this.header});
  }

  getListAllAnggota(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MEMBER_BASE_URL + '/list-anggota/', param, {headers: this.header});
  }

  getAnggotaByKTP(noKTP): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MEMBER_BASE_URL + '/get-data-anggota/' +  noKTP, {}, {headers: this.header});
  }

  getListAnggotaPerusahaan(kodePerusahaan, start, end): Observable<any> {
    return this.httpClient.post(config.KOPBI_MEMBER_BASE_URL + '/list-anggota/' + kodePerusahaan + '/' + start + '/' + end, '{}', {headers: this.header});
  }

  doPostAnggota(param): Observable<any> {
    // console.log(param);
    // return null;
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MEMBER_BASE_URL + '/post-anggota', param, {headers: this.header});
  }

  doPostUploadFile(param, pathFile, fileName): Observable<any> {
    return this.httpClient.post(config.KOPBI_IMAGES_BASE_URL + '/upload/' + pathFile + '/' + fileName, param);
  }

  getListBarang(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/list-barang/', param, {headers: this.header});
  }

  getListBarangNew(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/list-data-barang/', param, {headers: this.header});
  }

  doPostBarang(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/post-barang', param, {headers: this.header});
  }

  doPostPengajuan(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PENGAJUAN_BASE_URL + '/post-pengajuan', param, {headers: this.header});
  }

  doPostPenarikan(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PINJAMAN_BASE_URL + '/post-penarikan', param, {headers: this.header});
  }

  getListPinjaman(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PINJAMAN_BASE_URL + '/list-pinjaman', param, {headers: this.header});
  }

  getListPinjamanAnggota(nik, param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PINJAMAN_BASE_URL + '/list-pinjaman/' + nik, param, {headers: this.header});
  }

  doPostPinjaman(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PINJAMAN_BASE_URL + '/post-pinjaman', param, {headers: this.header});
  }

  getListSimpanan(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PINJAMAN_BASE_URL + '/list-simpanan', param, {headers: this.header});
  }

  getListSimpananAnggota(nik, param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PINJAMAN_BASE_URL + '/list-simpanan/' + nik, param, {headers: this.header});
  }

  doPostSimpanan(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PINJAMAN_BASE_URL + '/post-simpanan', param, {headers: this.header});
  }

  getListAngsuran(nomorPinjaman): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PINJAMAN_BASE_URL + '/list-angsuran/' + nomorPinjaman, {}, {headers: this.header});
  }

  doPostAngsuran(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PINJAMAN_BASE_URL + '/post-angsuran', param, {headers: this.header});
  }

  getListKas(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_ARUSKAS_BASE_URL + '/list-kas', param, {headers: this.header});
  }

  doPostKas(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_ARUSKAS_BASE_URL + '/post-kas', param, {headers: this.header});
  }

  getListReport(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_REPORT_BASE_URL + '/list-report-kas', param, {headers: this.header});
  }

  getListRekapSaldo(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_REPORT_BASE_URL + '/list-report-rekap', param, {headers: this.header});
  }

  getDashboardPengajuan(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PENGAJUAN_BASE_URL + '/get-dashboard-pengajuan', param, {headers: this.header});
  }

  getDashboardPinjaman(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PINJAMAN_BASE_URL + '/get-dashboard-pinjaman', param, {headers: this.header});
  }

  getDashboardAnggota(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MEMBER_BASE_URL + '/get-dashboard-anggota', param, {headers: this.header});
  }

  getSaldoSimpanan(nomorNik, param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PINJAMAN_BASE_URL + '/get-simpanan/' + nomorNik, param, {headers: this.header});
  }

  getListPersentase(): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/list-komponen/KOMPONEN', '{}', {headers: this.header});
  }

  doPostKomponen(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/post-komponen', param, {headers: this.header});
  }

  getListKonten(kode): Observable<any> {
    let headers = new HttpHeaders();
    // headers = headers.set('Content-Type', 'application/x-www-form-urlencoded');
    headers = headers.set('token', 'U2FsdGVkX19emypgqSLb6nLxUO5CO3eG7avTQXU045E=');
    headers = headers.set('jwtToken', this.auth.getJWTToken());
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/list-konten/' + kode, '{}', {headers: headers});
  }

  doPostKonten(param): Observable<any> {
    let headers = new HttpHeaders();
    // headers = headers.set('Content-Type', 'application/x-www-form-urlencoded');
    headers = headers.set('token', 'U2FsdGVkX19emypgqSLb6nLxUO5CO3eG7avTQXU045E=');
    headers = headers.set('jwtToken', this.auth.getJWTToken());
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/post-konten', param, {headers: headers});
  }

  changePassword(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MEMBER_BASE_URL + '/change-password', param, {headers: this.header});
  }

  resetPassword(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MEMBER_BASE_URL + '/reset-password', param, {headers: this.header});
  }

  getListKomponen(): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/list-komponen/KOMPONEN', '{}', {headers: this.header});
  }

  getListCalculate(): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/list-komponen/CALCULATE', '{}', {headers: this.header});
  }

  getListInfoApp(): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MASTER_BASE_URL + '/list-komponen/INFO_APP', '{}', {headers: this.header});
  }

  getListRegistrasi(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MEMBER_BASE_URL + '/list-registrasi-anggota', param, {headers: this.header});
  }

  doRegistration(param): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    headers = headers.set('token', 'U2FsdGVkX19emypgqSLb6nLxUO5CO3eG7avTQXU045E=');
    // headers = headers.set('jwtToken', '');
    return this.httpClient.post(config.KOPBI_BASE_URL + '/registrasi-anggota', param, {headers: headers});
  }

  doVerification(code): Observable<any> {
    return this.httpClient.get(config.KOPBI_BASE_URL + '/verifikasi-anggota/' + code);
  }

  doRejectRegistration(nomorKTP): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_MEMBER_BASE_URL + '/rejected-registrasi/' + nomorKTP, '{}', {headers: this.header});
  }

  getLaporanSimpanan(param): Observable<any> {
    this.getHeaders();
    return this.httpClient.post(config.KOPBI_PINJAMAN_BASE_URL + '/list-simpanan-anggota/', param, {headers: this.header});
  }

}
