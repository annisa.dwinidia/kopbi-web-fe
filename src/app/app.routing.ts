import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { AuthGuard } from './service/auth.guard';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { PengajuanComponent } from './views/transaksi/pengajuan/pengajuan.component';
import { TmbPengajuanComponent } from './views/transaksi/tmb-pengajuan/tmb-pengajuan.component';
import { SimpananComponent } from './views/transaksi/simpanan/simpanan.component';
import { TmbSimpananComponent } from './views/transaksi/tmb-simpanan/tmb-simpanan.component';
import { KasComponent } from './views/transaksi/kas/kas.component';
import { ReportComponent } from './views/transaksi/report/report.component';
import { PengajuanAnggotaComponent } from './views/transaksi/pengajuan-anggota/pengajuan-anggota.component';
import { PerusahaanComponent } from './views/master/perusahaan/perusahaan.component';
import { AnggotaComponent } from './views/master/anggota/anggota.component';
import { SimpananAnggotaComponent } from './views/transaksi/simpanan-anggota/simpanan-anggota.component';
import {PinjamanComponent} from './views/transaksi/pinjaman/pinjaman.component';
import {PinjamanAnggotaComponent} from './views/transaksi/pinjaman-anggota/pinjaman-anggota.component';
import {AngsuranComponent} from './views/transaksi/angsuran/angsuran.component';
import {TmbAngsuranComponent} from './views/transaksi/tmb-angsuran/tmb-angsuran.component';
import {PinjamanBelumLunasComponent} from './views/transaksi/pinjaman-belum-lunas/pinjaman-belum-lunas.component';
import {ProfileComponent} from './views/master/profile/profile.component';
import {TagihanPinjamanComponent} from './views/transaksi/tagihan-pinjaman/tagihan-pinjaman.component';
import {FeePerusahaanComponent} from './views/transaksi/fee-perusahaan/fee-perusahaan.component';
import {DataAnggotaComponent} from './views/transaksi/data-anggota/data-anggota.component';
import {BankComponent} from './views/master/bank/bank.component';
import {KonfederasiComponent} from './views/master/konfederasi/konfederasi.component';
import {BarangComponent} from './views/master/barang/barang.component';
import {TagihanSimpananBaruComponent} from './views/transaksi/tagihan-simpanan-baru/tagihan-simpanan-baru.component';
import {TagihanSimpananAktifComponent} from './views/transaksi/tagihan-simpanan-aktif/tagihan-simpanan-aktif.component';
import {PenarikanComponent} from './views/transaksi/penarikan/penarikan.component';
import {TmbPenarikanComponent} from './views/transaksi/tmb-penarikan/tmb-penarikan.component';
import {KendaraanComponent} from './views/master/kendaraan/kendaraan.component';
import {PerumahanComponent} from './views/master/perumahan/perumahan.component';
import {KomponenComponent} from './views/master/komponen/komponen.component';
import {KontenComponent} from './views/master/konten/konten.component';
import {ChangePasswordComponent} from './views/change-password/change-password.component';
import {PenarikanAnggotaComponent} from './views/transaksi/penarikan-anggota/penarikan-anggota.component';
import {InformasiComponent} from './views/master/informasi/informasi.component';
import {LaporanComponent} from './views/master/laporan/laporan.component';
import {KegiatanComponent} from './views/master/kegiatan/kegiatan.component';
import {CatatanComponent} from './views/master/catatan/catatan.component';
import {IklanComponent} from './views/master/iklan/iklan.component';
import {NotifikasiComponent} from './views/master/notifikasi/notifikasi.component';
import {RegistrasiComponent} from './views/transaksi/registrasi/registrasi.component';
import {VerifikasiComponent} from './views/transaksi/verifikasi/verifikasi.component';
import {IklanMerchantComponent} from './views/master/iklan-merchant/iklan-merchant.component';
import {LaporanSimpananComponent} from './views/transaksi/laporan-simpanan/laporan-simpanan.component';
import {PengajuanKonsumerComponent} from './views/transaksi/pengajuan-konsumer/pengajuan-konsumer.component';
import {TmbPengajuanKonsumerComponent} from './views/transaksi/tmb-pengajuan-konsumer/tmb-pengajuan-konsumer.component';
export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'change-password',
    component: ChangePasswordComponent,
    data: {
      title: 'Change Password Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: ':code',
    component: VerifikasiComponent,
    pathMatch: 'full',
    data: {
      title: 'Verification Page'
    }
  },
  {
    path: 'home',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
          title: 'Beranda'
        }
      },
      {
        path: 'profile',
        component: ProfileComponent,
        data: {
          title: 'Profil Anggota'
        }
      },
      {
        path: 'data-anggota',
        component: DataAnggotaComponent,
        data: {
          title: 'Data Anggota'
        }
      },
      {
        path: 'pengajuan/pinjaman',
        data: {
          title: 'Pengajuan Pinjaman'
        },
        children: [
          {
            path: '',
            component: PengajuanComponent,
            data: {
              title: 'Daftar Pengajuan Pinjaman'
            }
          },
          {
            path: 'anggota',
            component: PengajuanAnggotaComponent,
            data: {
              title: 'Daftar Pengajuan Pinjaman Anggota'
            }
          },
          {
            path: 'new',
            component: TmbPengajuanComponent,
            data: {
              title: 'Pengajuan Pinjaman Baru'
            }
          },
          {
            path: 'anggota/new',
            component: TmbPengajuanComponent,
            data: {
              title: 'Pengajuan Pinjaman Baru'
            }
          }
        ]
      },
      {
        path: 'pengajuan/penarikan',
        data: {
          title: 'Pengajuan Penarikan'
        },
        children: [
          {
            path: '',
            component: PenarikanComponent,
            data: {
              title: 'Daftar Pengajuan Penarikan'
            }
          },
          {
            path: 'anggota',
            component: PenarikanAnggotaComponent,
            data: {
              title: 'Daftar Pengajuan Penarikan Anggota'
            }
          },
          {
            path: 'new',
            component: TmbPenarikanComponent,
            data: {
              title: 'Pengajuan Penarikan Baru'
            }
          },
          {
            path: 'anggota/new',
            component: TmbPenarikanComponent,
            data: {
              title: 'Pengajuan Penarikan Baru'
            }
          }
        ]
      },
      {
        path: 'pengajuan/pengajuan-konsumer',
        data: {
          title: 'Pengajuan Barang Konsumer'
        },
        children: [
          {
            path: '',
            component: PengajuanKonsumerComponent,
            data: {
              title: 'Daftar Pengajuan Konsumer'
            }
          },
          {
            path: 'new',
            component: TmbPengajuanKonsumerComponent,
            data: {
              title: 'Pengajuan Barang Konsumer'
            }
          }
        ]
      },
      {
        path: 'pinjaman',
        data: {
          title: 'Pinjaman Lunas'
        },
        children: [
          {
            path: '',
            component: PinjamanComponent,
            data: {
              title: 'Daftar Pinjaman'
            }
          },
          {
            path: 'lunas',
            component: PinjamanComponent,
            data: {
              title: 'Daftar Pinjaman Lunas'
            }
          },
          {
            path: 'belum-lunas',
            component: PinjamanBelumLunasComponent,
            data: {
              title: 'Daftar Pinjaman Belum Lunas'
            }
          },
          {
            path: 'anggota',
            component: PinjamanAnggotaComponent,
            data: {
              title: 'Daftar Pinjaman Anggota'
            }
          }
        ]
      },
      {
        path: 'pinjaman/angsuran',
        data: {
          title: 'Angsuran'
        },
        children: [
          {
            path: '',
            component: AngsuranComponent,
            data: {
              title: 'Daftar Angsuran'
            }
          },
          {
            path: 'new',
            component: TmbAngsuranComponent,
            data: {
              title: 'Angsuran Baru'
            }
          },
        ]
      },
      {
        path: 'simpanan',
        data: {
          title: 'Simpanan'
        },
        children: [
          {
            path: '',
            component: SimpananComponent,
            data: {
              title: 'Daftar Simpanan'
            }
          },
          {
            path: 'anggota',
            component: SimpananAnggotaComponent,
            data: {
              title: 'Daftar Simpanan Anggota'
            }
          },
          {
            path: 'new',
            component: TmbSimpananComponent,
            data: {
              title: 'Simpanan Baru'
            }
          },
          {
            path: 'anggota/new',
            component: TmbSimpananComponent,
            data: {
              title: 'Simpanan Baru'
            }
          }
        ]
      },
      {
        path: 'kas',
        data: {
          title: 'Transaksi Kas'
        },
        children: [
          {
            path: 'list',
            component: KasComponent,
            data: {
              title: 'Daftar Transaksi Kas'
            }
          }
        ]
      },
      {
        path: 'tagihan',
        data: {
          title: 'Tagihan'
        },
        children: [
          {
            path: 'pinjaman',
            component: TagihanPinjamanComponent,
            data: {
              title: 'Daftar Tagihan Pinjaman'
            }
          },
          {
            path: 'pinjaman/anggota',
            component: PinjamanAnggotaComponent,
            data: {
              title: 'Daftar Tagihan Pinjaman Anggota'
            }
          },
          {
            path: 'simpanan/baru',
            component: TagihanSimpananBaruComponent,
            data: {
              title: 'Daftar Tagihan Simpanan Anggota Baru'
            }
          },
          {
            path: 'simpanan/aktif',
            component: TagihanSimpananAktifComponent,
            data: {
              title: 'Daftar Tagihan Simpanan Anggota Aktif'
            }
          }
        ]
      },
      {
        path: 'fee-perusahaan',
        data: {
          title: 'Fee Perusahaan'
        },
        children: [
          {
            path: '',
            component: FeePerusahaanComponent,
            data: {
              title: 'Daftar Fee Perusahaan'
            }
          }
        ]
      },
      {
        path: 'laporan',
        data: {
          title: 'Laporan'
        },
        children: [
          {
            path: 'transaksi',
            component: ReportComponent,
            data: {
              title: 'Laporan / Transaksi'
            }
          },
          {
            path: 'simpanan',
            component: LaporanSimpananComponent,
            data: {
              title: 'Laporan / Simpanan'
            }
          }
        ]
      },
      {
        path: 'registrasi/anggota',
        component: RegistrasiComponent,
        data: {
          title: 'Registrasi Anggota'
        }
      },
    ]
  },
  {
    path: 'master',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'perusahaan',
        component: PerusahaanComponent,
        data: {
          title: 'Master / Perusahaan'
        }
      },
      {
        path: 'anggota',
        component: AnggotaComponent,
        data: {
          title: 'Master / Anggota'
        }
      },
      {
        path: 'bank',
        component: BankComponent,
        data: {
          title: 'Master / Bank'
        }
      },
      {
        path: 'konfederasi',
        component: KonfederasiComponent,
        data: {
          title: 'Master / Konfederasi'
        }
      },
      {
        path: 'barang',
        component: BarangComponent,
        data: {
          title: 'Master / Barang'
        }
      },
      {
        path: 'kendaraan',
        component: KendaraanComponent,
        data: {
          title: 'Master / Kendaraan'
        }
      },
      {
        path: 'perumahan',
        component: PerumahanComponent,
        data: {
          title: 'Master / Perumahan'
        }
      },
      {
        path: 'komponen',
        component: KomponenComponent,
        data: {
          title: 'Master / Komponen'
        }
      }
    ]
  },
  {
    path: 'konten',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Konten'
    },
    children: [
      {
        path: 'konten',
        component: KontenComponent,
        data: {
          title: 'Master / Konten'
        }
      },
      {
        path: 'informasi',
        component: InformasiComponent,
        data: {
          title: 'Master / Informasi'
        }
      },
      {
        path: 'laporan',
        component: LaporanComponent,
        data: {
          title: 'Master / Laporan'
        }
      },
      {
        path: 'kegiatan',
        component: KegiatanComponent,
        data: {
          title: 'Master / Kegiatan'
        }
      },
      {
        path: 'catatan',
        component: CatatanComponent,
        data: {
          title: 'Master / Catatan'
        }
      },
      {
        path: 'iklan',
        component: IklanComponent,
        data: {
          title: 'Master / Iklan'
        }
      },
      {
        path: 'merchant',
        component: IklanMerchantComponent,
        data: {
          title: 'Master / Iklan Merchant'
        }
      },
      {
        path: 'notifikasi',
        component: NotifikasiComponent,
        data: {
          title: 'Master / Notifikasi'
        }
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(
    routes, {
      useHash: true,
      onSameUrlNavigation: 'reload'
    }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
