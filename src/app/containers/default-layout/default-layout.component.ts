import { Component, OnDestroy, OnInit } from '@angular/core';
import { navItemsADM, navItemsHRD, navItemsAGT, navItemsPENGAWAS } from './../../_nav';
import { AuthService } from '../../service/auth.service';
import { environment } from '../../../environments/environment';
import { User } from '../../model/user';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppService} from '../../service/app.service';
import {AESEncryptDecryptService} from '../../service/aesencrypt-decrypt-service.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./style.css']
})
export class DefaultLayoutComponent implements OnInit, OnDestroy {
  public navItemsADM = navItemsADM;
  public navItemsAGT = navItemsAGT;
  public navItemsHRD = navItemsHRD;
  public navItemsPENGAWAS = navItemsPENGAWAS;
  public sidebarMinimized = false;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;

  dataUser: User;
  today = new Date();
  version: string = environment.VERSION;

  formBuilder: FormGroup;
  loading = false;

  constructor(public auth: AuthService, private fb: FormBuilder, private toastr: ToastrService,
              private service: AppService, private encryption: AESEncryptDecryptService) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: [ 'class' ]
    });
  }

  ngOnInit() {
    this.dataUser = JSON.parse(this.auth.getToken());

    this.formBuilder = this.fb.group({
      passwordLama: ['', [Validators.required, Validators.minLength(4)]],
      passwordBaru: ['', [Validators.required, Validators.minLength(4)]],
      konfirmasiPasswordBaru: ['', [Validators.required, Validators.minLength(4)]]
    });
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  clearPassword() {
    this.formBuilder.reset();
  }

  doChangePassword() {
    const op = this.formBuilder.get('passwordLama').value;
    const np = this.formBuilder.get('passwordBaru').value;
    const npc = this.formBuilder.get('konfirmasiPasswordBaru').value;
    if (np !== npc) {
      this.toastr.error('Konfirmasi Password Baru tidak sesuai', 'Ooops!');
    } else {
      const param: any = {
        userName: this.dataUser.role === 'HRD' ? this.dataUser.emailPerusahaan : this.dataUser.nomorHp,
        oldPassword: this.encryption.encrypt(op),
        password: this.encryption.encrypt(np)
      };
      this.service.changePassword(param).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success('Password Berhasil Diganti', 'Success!');
        } else {
          this.toastr.error('Penggantian Password Gagal!!', 'Ooops!');
        }
      }, (err) => {
        this.loading = false;
        this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
      });
    }
  }
}
