import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'roleAnggota'
})
export class RoleAnggotaPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === 'AGT') {
      return 'Anggota';
    } else if (value === 'PENGAWAS') {
      return 'Pengawas';
    } else if (value === 'HRD') {
      return 'HRD';
    } else if (value === 'ADM') {
      return 'Administrator';
    } else {
      return '-';
    }
  }


}
