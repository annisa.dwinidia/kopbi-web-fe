import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'statusAnggota'
})
export class StatusAnggotaPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === 'A') {
      return 'Aktif';
    } else if (value === 'I') {
      return 'Non Aktif';
    } else if (value === 'N') {
      return 'Baru';
    } else if (value === 'REG') {
      return 'Registrasi';
    } else {
      return '-';
    }
  }


}
