import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'statusPinjaman'
})
export class StatusPinjamanPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === 'BELUM') {
      return 'Belum Lunas';
    } else if (value === 'LUNAS') {
      return 'Lunas';
    } else {
      return '-';
    }
  }

}
