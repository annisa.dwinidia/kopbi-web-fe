import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'marriageFilter'
})
export class MarriageFilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === 1) {
      return 'Lajang';
    } else if (value === 2) {
      return 'Menikah';
    } else if (value === 3){
      return 'Cerai';
    } else {
      return '';
    }
  }

}
