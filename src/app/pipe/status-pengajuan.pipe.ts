import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'statusPengajuan'
})
export class StatusPengajuanPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value === 'NEW') {
      return 'Menunggu Verifikasi HRD';
    } else if (value === 'APP') {
      return 'Menunggu Verifikasi Pengawas';
    } else if (value === 'VRF') {
      return 'Menunggu Proses Admin';
    } else if (value === 'CAN') {
      return 'Batal';
    } else if (value === 'CAN-HRD') {
      return 'Ditolak HRD';
    } else if (value === 'CAN-PENGAWAS') {
      return 'Ditolak Pengawas';
    } else if (value === 'PROC') {
      return 'Selesai';
    }
  }

}
