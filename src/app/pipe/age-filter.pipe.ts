import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ageFilter'
})
export class AgeFilterPipe implements PipeTransform {

  transform(birthDate: any, args?: any): any {
    if (birthDate) {
      if (birthDate === '0000-00-00') {
        return '-';
      } else {
        const timeDiff: any = Math.abs(Date.now() - new Date(birthDate).getTime());
        const age: any = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
        return age;
      }
    }
  }

}
