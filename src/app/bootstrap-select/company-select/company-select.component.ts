import {Component, EventEmitter, Output} from '@angular/core';
import { BaseBootstrapSelect } from '../base-bootstrap-select';

@Component({
  selector: 'app-company-select',
  templateUrl: './company-select.component.html'
})

export class CompanySelectComponent extends BaseBootstrapSelect {
  mySelections;

  @Output()
  someChildEvent = new EventEmitter<string>();
  onChange(value) {
    this.someChildEvent.emit(value);
  }

}

