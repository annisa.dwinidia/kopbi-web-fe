import {Component, EventEmitter, Output} from '@angular/core';
import { BaseBootstrapSelect } from '../base-bootstrap-select';

@Component({
  selector: 'app-member-select',
  templateUrl: './member-select.component.html'
})

export class MemberSelectComponent extends BaseBootstrapSelect {
  mySelections;

  @Output()
  someChildEvent = new EventEmitter<string>();
  onChange(value) {
    this.someChildEvent.emit(value);
  }

}

