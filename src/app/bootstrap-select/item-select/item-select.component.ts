import {Component, EventEmitter, Input, Output} from '@angular/core';
import { BaseBootstrapSelect } from '../base-bootstrap-select';

@Component({
  selector: 'app-item-select',
  templateUrl: './item-select.component.html'
})

export class ItemSelectComponent extends BaseBootstrapSelect {
  mySelections;
  @Input() title: string;

  @Output()
  someChildEvent = new EventEmitter<string>();
  onChange(value) {
    this.someChildEvent.emit(value);
  }

}

