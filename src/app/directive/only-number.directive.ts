import {Directive, Input, HostListener} from '@angular/core';
import * as _ from 'lodash';
import * as numeral from 'numeral';

@Directive({
  selector: '[appOnlyNumber]'
})
export class OnlyNumberDirective {
  @Input('appOnlyNumber')
  val: string;

  numberArr = ['48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '0'];

  constructor() {
  }

  @HostListener('keyup', ['$event'])
  onKeyUp(event: any) {
    if (this.val) {
      event.target.value = (numeral(Number(this.val.replace(new RegExp(',', 'g'), ''))).format('0,0'));
    } else {
      return false;
    }

  }

  @HostListener('keypress', ['$event'])
  onKeyPress(event: any) {
    if (_.indexOf(this.numberArr, event.charCode.toString()) === -1) {
      event.preventDefault();
    } else {
    }
  }

  @HostListener('paste', ['$event'])
  onPaste(event: any) {
    if (_.isNumber(event.target.value) === false) {
      event.preventDefault();
    } else {
    }
  }

  @HostListener('blur', ['$event'])
  onBlur(event: any) {

  }
}
