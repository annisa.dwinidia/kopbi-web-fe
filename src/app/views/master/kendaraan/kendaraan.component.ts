import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../service/app.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import { Router } from '@angular/router';
import {Barang} from '../../../model/Barang';

@Component({
  selector: 'app-kendaraann',
  templateUrl: './kendaraan.component.html',
  styleUrls: ['./kendaraan.component.scss']
})
export class KendaraanComponent implements OnInit {

  formBuilder: FormGroup;

  loading = false;

  addModal;
  selectedBarang = new Barang();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';

  listBarang:     Barang[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 100;

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.doGetListBarang();
    this.formBuilder = this.fb.group({
      kodeBarang: ['', Validators.required],
      namaBarang: ['', Validators.required],
      harga: ['', Validators.required],
      hargaBeli: ['', Validators.required],
      keterangan: ['', Validators.required],
      stokBarang: ['', Validators.required],
      namaSuplier: ['', Validators.required],
      teleponSuplier: ['', Validators.required],
      kategori: [''],
    });
  }

  doGetListBarang() {
    this.listBarang = [];
    this.loading = true;
    const param = {
      search: this.filterText,
      mulai: (this.currentPage - 1) * this.itemsPerPage + 1,
      akhir: this.itemsPerPage
    };
    this.service.getListBarang(param).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      for (let x = 0; x < data.length; x++) {
        if (data[x].kategori === 'kendaraan') {
          this.listBarang.push(data[x]);
        }
      }
      this.listBarang = _.sortBy(this.listBarang, ['id']);
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Barang!', 'Ooops!');
    });
  }

  doOpenFormAdd() {
    this.modalTitle = 'Tambah';
    this.formBuilder.reset();
  }

  doOpenFormEdit() {
    this.modalTitle = 'Ubah';
    this.formBuilder.get('kodeBarang').setValue(this.selectedBarang.kodeBarang);
    this.formBuilder.get('namaBarang').setValue(this.selectedBarang.namaBarang);
    this.formBuilder.get('harga').setValue(numeral(Number(this.selectedBarang.harga)).format('0,0'));
    this.formBuilder.get('hargaBeli').setValue(numeral(Number(this.selectedBarang.hargaBeli)).format('0,0'));
    this.formBuilder.get('namaSuplier').setValue(this.selectedBarang.namaSuplier);
    this.formBuilder.get('teleponSuplier').setValue(this.selectedBarang.teleponSuplier);
    this.formBuilder.get('keterangan').setValue(this.selectedBarang.keterangan);
    this.formBuilder.get('stokBarang').setValue(numeral(Number(this.selectedBarang.stokBarang)).format('0,0'));
  }

  submit() {
    let param: any = {};
    if (this.modalTitle === 'Tambah') {
      param = {
        kodeBarang: this.formBuilder.get('kodeBarang').value,
        namaBarang: this.formBuilder.get('namaBarang').value,
        harga: this.formBuilder.get('harga').value.toString().replace(new RegExp(',', 'g'), ''),
        hargaBeli: this.formBuilder.get('hargaBeli').value.toString().replace(new RegExp(',', 'g'), ''),
        namaSuplier: this.formBuilder.get('namaSuplier').value,
        teleponSuplier: this.formBuilder.get('teleponSuplier').value,
        keterangan: this.formBuilder.get('keterangan').value,
        stokBarang: this.formBuilder.get('stokBarang').value.toString().replace(new RegExp(',', 'g'), ''),
        kategori: 'kendaraan'
      };
    } else if (this.modalTitle === 'Ubah') {
      param = {
        id: this.selectedBarang.id,
        kodeBarang: this.formBuilder.get('kodeBarang').value,
        namaBarang: this.formBuilder.get('namaBarang').value,
        harga: this.formBuilder.get('harga').value.toString().replace(new RegExp(',', 'g'), ''),
        hargaBeli: this.formBuilder.get('hargaBeli').value.toString().replace(new RegExp(',', 'g'), ''),
        namaSuplier: this.formBuilder.get('namaSuplier').value,
        teleponSuplier: this.formBuilder.get('teleponSuplier').value,
        keterangan: this.formBuilder.get('keterangan').value,
        stokBarang: this.formBuilder.get('stokBarang').value.toString().replace(new RegExp(',', 'g'), ''),
        kategori: 'kendaraan'
      };
    }
    this.service.doPostBarang(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListBarang();
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  setSelectedBarang(data: Barang) {
    this.selectedBarang = data;
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListBarang();
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }
}
