import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AppService} from '../../../service/app.service';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import {User} from '../../../model/user';
import {AuthService} from '../../../service/auth.service';
import {config} from '../../../config/application-config';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  loading = false;
  sub: any;
  id: number;
  dataUser: User;

  now = new Date();
  linkImage = config.KOPBI_IMAGES_BASE_URL;

  constructor(private route: ActivatedRoute, private service: AppService, private fb: FormBuilder,
              private _location: Location, private toastr: ToastrService, private auth: AuthService) { }

  ngOnInit() {
    this.dataUser = JSON.parse(this.auth.getToken());
    this.sub = this.route.queryParams.subscribe(params => {
      this.id = params['id'];
      if (this.id != null) {
        // this.getDataPelamar();
      }
    });
  }

  // getDataPelamar() {
  //   this.service.getDataEmployee(this.id).subscribe((res) => {
  //     this.loading = false;
  //     this.dataPelamar = res.data[0];
  //     this.status = this.dataPelamar.status;
  //   }, (err) => {
  //     this.loading = false;
  //     this.toastr.error('Kesalahan koneksi!', 'Ooops!');
  //   });
  // }
  //
  backClicked() {
    this._location.back();
  }

}
