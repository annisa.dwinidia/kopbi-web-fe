import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatatanComponent } from './catatan.component';

describe('CatatanComponent', () => {
  let component: CatatanComponent;
  let fixture: ComponentFixture<CatatanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatatanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatatanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
