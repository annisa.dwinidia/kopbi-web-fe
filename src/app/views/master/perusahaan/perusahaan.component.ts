import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../service/app.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Bank } from '../../../model/bank';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import {Perusahaan} from "../../../model/Perusahaan";
import {Konfederasi} from '../../../model/Konfederasi';
import {DatePipe} from '@angular/common';
import {AESEncryptDecryptService} from '../../../service/aesencrypt-decrypt-service.service';

@Component({
  selector: 'app-perusahaan',
  templateUrl: './perusahaan.component.html',
  styleUrls: ['./perusahaan.component.scss'],
  providers: [DatePipe]
})
export class PerusahaanComponent implements OnInit {

  formBuilder: FormGroup;

  loading = false;

  addModal;
  selectedPerusahaan = new Perusahaan();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';

  listPerusahaan:     Perusahaan[] = [];
  listKonfederasi:    any[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;

  isDesc = true;
  column: string;
  direction: number;

  configSelectKonfederasi = null;
  selectedKonfederasi;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private router: Router, private datePipe: DatePipe, private encryption: AESEncryptDecryptService) { }

  ngOnInit() {
    this.doGetListCompany();
    this.doGetListKonfederasi();
    this.formBuilder = this.fb.group({
      kodePerusahaan: ['', Validators.required],
      namaPerusahaan: ['', Validators.required],
      alamatPerusahaan: ['', Validators.required],
      emailPerusahaan: ['', Validators.required],
      registrasiPerusahaan: ['', Validators.required],
      teleponPerusahaan: ['', Validators.required],
      nomorPerusahaan:  [''],
      kodeBidangUsaha:  ['', Validators.required],
      faxPerusahaan:  ['', Validators.required],
      npwpPerusahaan:  ['', Validators.required],
      selectedKonfederasi:  ['', Validators.required],
      kodeKonfederasi:  [''],
      namaKonfederasi:  [''],
      nikPejabat:  ['', Validators.required],
      namaPejabat:  ['', Validators.required],
      jabatanPejabat:  ['', Validators.required],
      extentionPejabat:  ['', Validators.required],
      hpPejabat:  ['', Validators.required]
    });
    this.configSelectKonfederasi = {
      displayKey: 'namaKonfederasi',
      search: true,
      height: '200px',
      placeholder: 'Pilih Konfederasi',
      customComparator: () => {},
      limitTo: this.listKonfederasi.length,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Konfederasi..',
      // searchOnKey: 'departmentCode'
    };
  }

  doGetListKonfederasi() {
    this.loading = true;
    const param = {
      search: this.filterText
    };
    this.service.getListKonfederasi(param).subscribe((res) => {
      this.loading = false;
      const resp = JSON.parse(res.data);
      for (let x = 0; x < resp.length; x++) {
        const data = {
          kodeKonfederasi: resp[x].kodeKonfederasi,
          namaKonfederasi: resp[x].namaKonfederasi
        };
        this.listKonfederasi.push(data);
      }
      this.listKonfederasi = _.sortBy(this.listKonfederasi, ['id']);
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Konfederasi!', 'Ooops!');
    });
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      this.totalItems = res.recordsTotal;
      this.totalFiltered = res.recordsFiltered;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doOpenFormAdd() {
    this.modalTitle = 'Tambah';
    this.formBuilder.reset();
    this.listKonfederasi = [...this.listKonfederasi];
  }

  doOpenFormEdit() {
    this.modalTitle = 'Ubah';
    this.listKonfederasi = [...this.listKonfederasi];
    this.formBuilder.get('nomorPerusahaan').setValue(this.selectedPerusahaan.nomorPerusahaan);
    this.formBuilder.get('kodePerusahaan').setValue(this.selectedPerusahaan.kodePerusahaan);
    this.formBuilder.get('namaPerusahaan').setValue(this.selectedPerusahaan.namaPerusahaan);
    this.formBuilder.get('alamatPerusahaan').setValue(this.selectedPerusahaan.alamatPerusahaan);
    this.formBuilder.get('emailPerusahaan').setValue(this.selectedPerusahaan.emailPerusahaan);
    this.formBuilder.get('registrasiPerusahaan').setValue(this.selectedPerusahaan.registrasiPerusahaan);
    this.formBuilder.get('teleponPerusahaan').setValue(this.selectedPerusahaan.teleponPerusahaan);
    this.formBuilder.get('kodeBidangUsaha').setValue(this.selectedPerusahaan.kodeBidangUsaha);
    this.formBuilder.get('faxPerusahaan').setValue(this.selectedPerusahaan.faxPerusahaan);
    this.formBuilder.get('npwpPerusahaan').setValue(this.selectedPerusahaan.npwpPerusahaan);
    this.formBuilder.get('kodeKonfederasi').setValue(this.selectedPerusahaan.kodeKonfederasi);
    this.formBuilder.get('namaKonfederasi').setValue(this.selectedPerusahaan.namaKonfederasi);
    this.formBuilder.get('nikPejabat').setValue(this.selectedPerusahaan.nikPejabat);
    this.formBuilder.get('jabatanPejabat').setValue(this.selectedPerusahaan.jabatanPejabat);
    this.formBuilder.get('extentionPejabat').setValue(this.selectedPerusahaan.extentionPejabat);
    this.formBuilder.get('hpPejabat').setValue(this.selectedPerusahaan.hpPejabat);
    this.formBuilder.get('namaPejabat').setValue(this.selectedPerusahaan.namaPejabat);
    const konfederasi: any = {
      kodeKonfederasi: this.selectedPerusahaan.kodeKonfederasi,
      namaKonfederasi: this.selectedPerusahaan.namaKonfederasi
    };
    this.formBuilder.get('selectedKonfederasi').setValue(konfederasi);
  }

  submit() {
    let param: any = {};
    if (this.modalTitle === 'Tambah') {
      param = {
        kodePerusahaan: this.formBuilder.get('kodePerusahaan').value,
        namaPerusahaan: this.formBuilder.get('namaPerusahaan').value,
        alamatPerusahaan: this.formBuilder.get('alamatPerusahaan').value,
        emailPerusahaan: this.formBuilder.get('emailPerusahaan').value,
        teleponPerusahaan: this.formBuilder.get('teleponPerusahaan').value,
        registrasiPerusahaan: this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
        // nomorPerusahaan:  this.formBuilder.get('nomorPerusahaan').value,
        kodeBidangUsaha:  this.formBuilder.get('kodeBidangUsaha').value,
        faxPerusahaan:  this.formBuilder.get('faxPerusahaan').value,
        npwpPerusahaan:  this.formBuilder.get('npwpPerusahaan').value,
        kodeKonfederasi:  this.formBuilder.get('selectedKonfederasi').value.kodeKonfederasi,
        namaKonfederasi:  this.formBuilder.get('selectedKonfederasi').value.namaKonfederasi,
        nikPejabat: this.formBuilder.get('nikPejabat').value,
        namaPejabat: this.formBuilder.get('namaPejabat').value,
        jabatanPejabat: this.formBuilder.get('jabatanPejabat').value,
        extentionPejabat: this.formBuilder.get('extentionPejabat').value,
        hpPejabat: this.formBuilder.get('hpPejabat').value,
      };
    } else if (this.modalTitle === 'Ubah') {
      param = {
        id: this.selectedPerusahaan.id,
        kodePerusahaan: this.formBuilder.get('kodePerusahaan').value,
        namaPerusahaan: this.formBuilder.get('namaPerusahaan').value,
        alamatPerusahaan: this.formBuilder.get('alamatPerusahaan').value,
        emailPerusahaan: this.formBuilder.get('emailPerusahaan').value,
        teleponPerusahaan: this.formBuilder.get('teleponPerusahaan').value,
        registrasiPerusahaan: this.formBuilder.get('registrasiPerusahaan').value,
        nomorPerusahaan:  this.formBuilder.get('nomorPerusahaan').value,
        kodeBidangUsaha:  this.formBuilder.get('kodeBidangUsaha').value,
        faxPerusahaan:  this.formBuilder.get('faxPerusahaan').value,
        npwpPerusahaan:  this.formBuilder.get('npwpPerusahaan').value,
        kodeKonfederasi:  this.formBuilder.get('selectedKonfederasi').value.kodeKonfederasi,
        namaKonfederasi:  this.formBuilder.get('selectedKonfederasi').value.namaKonfederasi,
        nikPejabat: this.formBuilder.get('nikPejabat').value,
        namaPejabat: this.formBuilder.get('namaPejabat').value,
        jabatanPejabat: this.formBuilder.get('jabatanPejabat').value,
        extentionPejabat: this.formBuilder.get('extentionPejabat').value,
        hpPejabat: this.formBuilder.get('hpPejabat').value,
      };
    }
    this.service.doPostPerusahaan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListCompany();
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  resetPassword() {
    const param: any = {
      userName: this.selectedPerusahaan.emailPerusahaan,
      // oldPassword: this.encryption.encrypt(''),
      password: this.encryption.encrypt('123456')
    };
    this.service.resetPassword(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success('Password Berhasil Diganti', 'Success!');
      } else {
        this.toastr.error('Penggantian Password Gagal!!', 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  setSelectedCompany(data: Perusahaan) {
    this.selectedPerusahaan = data;
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListCompany();
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  goToProfile(id: number) {
    this.router.navigate(['/home/recruitment/profile'], { queryParams: { id: id }, skipLocationChange: false });
  }
}
