import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { AppService } from '../../../service/app.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import { Router } from '@angular/router';
import {Barang} from '../../../model/Barang';
import {config} from '../../../config/application-config';

@Component({
  selector: 'app-iklan',
  templateUrl: './iklan.component.html',
  styleUrls: ['./iklan.component.scss']
})
export class IklanComponent implements OnInit {

  formBuilder: FormGroup;
  @ViewChild('imageUpload') imageUpload: ElementRef;

  @ViewChild('imageUpload2') imageUpload2: ElementRef;

  loading = false;

  addModal;
  selectedKonten;
  selectedKodeKategori = '';
  selectedIdKategori = '';

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';

  listKonten:     any[] = [];
  isImageFound = true;
  isImage2Found = true;

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;

  isDesc = true;
  column: string;
  direction: number;

  imageUploadDokumen: File;
  image2UploadDokumen: File;
  linkImage = config.KOPBI_IMAGES_BASE_URL;
  linkURL = '';
  linkURL2 = '';

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.doGetListKonten();
    this.formBuilder = this.fb.group({
      kodeKategori: ['iklan', Validators.required],
      namaKonten: ['', Validators.required],
      namaKategori: ['', Validators.required],
      keterangan: ['', Validators.required]
    });
  }

  doGetListKonten() {
    this.loading = true;
    const param = {
      search: this.filterText
    };
    this.service.getListKonten('iklan').subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.listKonten = datas;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Konten!', 'Ooops!');
    });
  }

  doOpenFormAdd() {
    this.isImageFound = false;
    this.selectedKonten = null;
    this.selectedIdKategori = '';
    this.selectedKodeKategori = '';
    this.modalTitle = 'Tambah';
    if (this.imageUpload) {
      this.imageUpload.nativeElement.value = '';
    }
    if (this.imageUpload2) {
      this.imageUpload2.nativeElement.value = '';
    }
    this.formBuilder.reset();
  }

  doOpenFormEdit() {
    this.formBuilder.reset();
    if (this.imageUpload2) {
      this.imageUpload2.nativeElement.value = '';
    }
    if (this.imageUpload2) {
      this.imageUpload2.nativeElement.value = '';
    }
    this.isImageFound = true;
    this.modalTitle = 'Ubah';
    this.linkURL = this.linkImage + '/informasi/' + this.selectedIdKategori + '.jpg';
    this.linkURL2 = this.linkImage + '/informasi/' + this.selectedIdKategori + '.png';
    this.formBuilder.get('kodeKategori').setValue(this.selectedKonten.kodeKategori);
    this.formBuilder.get('namaKonten').setValue(this.selectedKonten.namaKonten);
    this.formBuilder.get('namaKategori').setValue(this.selectedKonten.namaKategori);
    this.formBuilder.get('keterangan').setValue(this.selectedKonten.keterangan);
  }

  updateImageActive() {
    this.isImageFound = !this.isImageFound;
  }

  updateImage2Active() {
    this.isImage2Found = !this.isImage2Found;
  }

  submit() {
    let paramText: any = {};
    let formDataDokumen = new FormData();
    if (this.modalTitle === 'Tambah') {
      paramText = {
        kodeKategori: this.formBuilder.get('kodeKategori').value,
        namaKonten: this.formBuilder.get('namaKonten').value,
        namaKategori: this.formBuilder.get('namaKategori').value,
        keterangan: this.formBuilder.get('keterangan').value
      };
    } else if (this.modalTitle === 'Ubah') {
      paramText = {
        id: this.selectedKonten.id,
        kodeKategori: this.formBuilder.get('kodeKategori').value,
        namaKonten: this.formBuilder.get('namaKonten').value,
        namaKategori: this.formBuilder.get('namaKategori').value,
        keterangan: this.formBuilder.get('keterangan').value
      };
    }

    if (this.imageUploadDokumen !== undefined) {
      formDataDokumen.append('image', this.imageUploadDokumen);
    }

    formDataDokumen.append('param', JSON.stringify(paramText));

    if (this.imageUploadDokumen === undefined) {
      this.toastr.error('Silahkan Upload Image/File terlebih dahulu!', 'Ooops!');
    } else {
      this.service.doPostKonten(formDataDokumen).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success(this.successMessage, 'Success!');
          this.doGetListKonten();
        } else {
          this.toastr.error(this.errorMesssage, 'Ooops!');
        }
      }, (err) => {
        this.loading = false;
        this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
      });
    }

    if (this.image2UploadDokumen !== undefined && this.modalTitle === 'Ubah') {
      const formDataDokumen2: FormData = new FormData();
      formDataDokumen2.append('file', this.image2UploadDokumen);
      this.service.doPostUploadFile(formDataDokumen2, 'informasi', this.selectedIdKategori).subscribe((res) => {
        this.loading = false;
      }, (err) => {
        this.loading = false;
        this.toastr.error('Terjadi kesalahan dalam upload Gambar 2!', 'Ooops!');
      });
    }

    if (this.imageUploadDokumen !== undefined && this.modalTitle === 'Ubah') {
      const formDataDokumen3: FormData = new FormData();
      formDataDokumen3.append('file', this.imageUploadDokumen);
      this.service.doPostUploadFile(formDataDokumen3, 'informasi', this.selectedIdKategori).subscribe((res) => {
        this.loading = false;
      }, (err) => {
        this.loading = false;
        this.toastr.error('Terjadi kesalahan dalam upload Gambar 2!', 'Ooops!');
      });
    }
    this.doGetListKonten();
  }

  onImageChanged(event) {
    this.imageUploadDokumen = event.item(0);
  }

  onImage2Changed(event) {
    this.image2UploadDokumen = event.item(0);
  }

  setSelectedKonten(data: any) {
    this.selectedKonten = data;
    this.selectedKodeKategori = data.kodeKategori;
    this.selectedIdKategori = data.id;
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListKonten();
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }
}
