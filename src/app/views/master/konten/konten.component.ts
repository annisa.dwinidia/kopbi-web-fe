import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { AppService } from '../../../service/app.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import { Router } from '@angular/router';
import {Barang} from '../../../model/Barang';
import {config} from '../../../config/application-config';

@Component({
  selector: 'app-konten',
  templateUrl: './konten.component.html',
  styleUrls: ['./konten.component.scss']
})
export class KontenComponent implements OnInit {

  formBuilder: FormGroup;
  @ViewChild('fileUpload') fileUpload: ElementRef;
  @ViewChild('imageUpload') imageUpload: ElementRef;

  loading = false;

  addModal;
  selectedKonten;
  selectedKodeKategori = '';
  selectedIdKategori = '';

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';

  listKonten:     any[] = [];
  isImageFound = true;

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;

  isDesc = true;
  column: string;
  direction: number;

  fileUploadDokumen: File;
  imageUploadDokumen: File;

  linkImage = config.KOPBI_IMAGES_BASE_URL;
  linkURL = '';

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.doGetListKonten();
    this.formBuilder = this.fb.group({
      kodeKategori: ['', Validators.required],
      namaKonten: ['', Validators.required],
      namaKategori: ['', Validators.required]
    });
  }

  // preview(files) {
  //   if (files.length === 0) {
  //     return;
  //   }
  //
  //   // let mimeType = files[0].type;
  //   // if (mimeType.match(/image\/*/) == null) {
  //   //   // this.message = "Only images are supported.";
  //   //   return;
  //   // }
  //
  //   this.isImageFound = true;
  //   let reader = new FileReader();
  //   // this.imagePath = files;
  //   reader.readAsDataURL(files[0]);
  //   reader.onload = (_event) => {
  //     this.linkURL = reader.result;
  //   };
  // }

  doGetListKonten() {
    this.selectedKodeKategori = '';
    this.selectedIdKategori = '';
    this.linkURL = '';
    this.loading = true;
    const param = {
      search: this.filterText
    };
    this.service.getListKonten('ALL').subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.listKonten = _.filter(datas, (data) => {
        return data.kodeKategori === 'kegiatan' || data.kodeKategori === 'informasi' || data.kodeKategori === 'laporan';
      });
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Konten!', 'Ooops!');
    });
  }

  doOpenFormAdd() {
    this.isImageFound = false;
    this.selectedKonten = null;
    this.selectedIdKategori = '';
    this.selectedKodeKategori = '';
    this.modalTitle = 'Tambah';
    this.fileUpload.nativeElement.value = '';
    this.imageUpload.nativeElement.value = '';
    this.formBuilder.reset();
  }

  doOpenFormEdit() {
    this.formBuilder.reset();
    this.fileUpload.nativeElement.value = '';
    this.imageUpload.nativeElement.value = '';
    this.isImageFound = true;
    this.linkURL = this.linkImage + '/' + this.selectedKodeKategori + '/' + this.selectedIdKategori + '.jpg'
    this.modalTitle = 'Ubah';
    this.formBuilder.get('kodeKategori').setValue(this.selectedKonten.kodeKategori);
    this.formBuilder.get('namaKonten').setValue(this.selectedKonten.namaKonten);
    this.formBuilder.get('namaKategori').setValue(this.selectedKonten.namaKategori);
  }

  updateImageActive() {
    this.isImageFound = !this.isImageFound;
  }

  submit() {
    let paramText: any = {};
    let paramImage: any = {};
    let formDataDokumen = new FormData();
    if (this.modalTitle === 'Tambah') {
      paramText = {
        kodeKategori: this.formBuilder.get('kodeKategori').value,
        namaKonten: this.formBuilder.get('namaKonten').value,
        namaKategori: this.formBuilder.get('namaKategori').value,
        keterangan: ''
      };
    } else if (this.modalTitle === 'Ubah') {
      paramText = {
        id: this.selectedKonten.id,
        kodeKategori: this.formBuilder.get('kodeKategori').value,
        namaKonten: this.formBuilder.get('namaKonten').value,
        namaKategori: this.formBuilder.get('namaKategori').value,
        keterangan: ''
      };
    }

    if (this.fileUploadDokumen !== undefined) {
      formDataDokumen.append('image', this.fileUploadDokumen);
    }

    if (this.imageUploadDokumen !== undefined) {
      formDataDokumen.append('image', this.imageUploadDokumen);
    }

    formDataDokumen.append('param', JSON.stringify(paramText));

    // console.log(formDataDokumen);
    // for (const key of formDataDokumen.keys()) {
    //   console.log(key);
    // }
    if (this.imageUploadDokumen === undefined && this.fileUploadDokumen === undefined) {
      this.toastr.error('Silahkan Upload Image/File terlebih dahulu!', 'Ooops!');
    } else {
      this.service.doPostKonten(formDataDokumen).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success(this.successMessage, 'Success!');
          this.doGetListKonten();
        } else {
          this.toastr.error(this.errorMesssage, 'Ooops!');
        }
      }, (err) => {
        this.loading = false;
        this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
      });
    }

    // let maxId  = 0;
    // this.listKonten.forEach(data => {
    //   if (data.id > maxId) {
    //     maxId = data.id;
    //   }
    // });
    // console.log(maxId);
    // if (this.fileUploadDokumen !== undefined) {
    //   const formDataDokumen: FormData = new FormData();
    //   formDataDokumen.append('file', this.fileUploadDokumen);
    //   let pathFile;
    //   const kode = this.formBuilder.get('kodeKategori').value;
    //   if (kode === 'informasi' || kode === 'laporan' || kode === 'catatan' || kode === 'iklan') {
    //     pathFile = 'informasi';
    //   } else if (kode === 'kegiatan') {
    //     pathFile = 'kegiatan';
    //   }
    //   this.service.doPostUploadFile(formDataDokumen, pathFile, maxId + 1).subscribe((res) => {
    //     this.loading = false;
    //   }, (err) => {
    //     this.loading = false;
    //     this.toastr.error('Terjadi kesalahan dalam upload File!', 'Ooops!');
    //   });
    // }
    //
    // if (this.imageUploadDokumen !== undefined) {
    //   const formDataDokumen: FormData = new FormData();
    //   formDataDokumen.append('file', this.imageUploadDokumen);
    //   let pathFile;
    //   const kode = this.formBuilder.get('kodeKategori').value;
    //   if (kode === 'informasi' || kode === 'laporan' || kode === 'catatan' || kode === 'iklan') {
    //     pathFile = 'informasi';
    //   } else if (kode === 'kegiatan') {
    //     pathFile = 'kegiatan';
    //   }
    //   this.service.doPostUploadFile(formDataDokumen, pathFile, maxId + 1).subscribe((res) => {
    //     this.loading = false;
    //   }, (err) => {
    //     this.loading = false;
    //     this.toastr.error('Terjadi kesalahan dalam upload Image!', 'Ooops!');
    //   });
    // }
    this.doGetListKonten();
  }

  onImageChanged(event) {
    this.isImageFound = true;
    this.imageUploadDokumen = event.item(0);
  }

  onFileChanged(event) {
    this.fileUploadDokumen = event.item(0);
  }


  setSelectedKonten(data: any) {
    this.selectedKonten = data;
    this.selectedKodeKategori = data.kodeKategori;
    this.selectedIdKategori = data.id;
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListKonten();
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

}
