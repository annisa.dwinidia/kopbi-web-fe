import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../service/app.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Bank } from '../../../model/bank';
import * as _ from 'lodash';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss']
})
export class BankComponent implements OnInit {

  formBuilder: FormGroup;

  loading = false;

  addModal;
  selectedBank = new Bank();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';

  listBank:     Bank[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.doGetListBank();
    this.formBuilder = this.fb.group({
      kodeBank: ['', Validators.required],
      namaBank: ['', Validators.required],
      cabangPembuka: ['', Validators.required],
      nomorRekening: ['', Validators.required],
    });
  }

  doGetListBank() {
    this.loading = true;
    const param = {
      search: this.filterText
    };
    this.service.getListBank(param).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listBank = _.sortBy(data, ['id']);
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Bank!', 'Ooops!');
    });
  }

  doOpenFormAdd() {
    this.modalTitle = 'Tambah';
    this.formBuilder.reset();
  }

  doOpenFormEdit() {
    this.modalTitle = 'Ubah';
    this.formBuilder.get('kodeBank').setValue(this.selectedBank.kodeBank);
    this.formBuilder.get('namaBank').setValue(this.selectedBank.namaBank);
    this.formBuilder.get('cabangPembuka').setValue(this.selectedBank.cabangPembuka);
    this.formBuilder.get('nomorRekening').setValue(this.selectedBank.nomorRekening);
  }

  submit() {
    const param = this.formBuilder.getRawValue();
    if (this.modalTitle === 'Ubah') {
      param.id = this.selectedBank.id;
    }
    // console.log(param);
    this.service.doPostBank(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListBank();
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  setSelectedBank(data: Bank) {
    this.selectedBank = data;
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListBank();
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }
}
