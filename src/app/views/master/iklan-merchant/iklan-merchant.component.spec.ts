import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IklanMerchantComponent } from './iklan-merchant.component';

describe('IklanMerchantComponent', () => {
  let component: IklanMerchantComponent;
  let fixture: ComponentFixture<IklanMerchantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IklanMerchantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IklanMerchantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
