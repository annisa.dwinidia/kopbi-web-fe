import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { AppService } from '../../../service/app.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import { Router } from '@angular/router';
import {Barang} from '../../../model/Barang';
import {config} from '../../../config/application-config';

@Component({
  selector: 'app-barang',
  templateUrl: './barang.component.html',
  styleUrls: ['./barang.component.scss']
})
export class BarangComponent implements OnInit {

  @ViewChild('fileUpload') fileUpload: ElementRef;
  formBuilder: FormGroup;

  loading = false;

  addModal;
  selectedBarang = new Barang();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';

  listBarang:     Barang[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;

  isDesc = true;
  column: string;
  direction: number;

  isImageFound = true;
  linkURL = '';
  imageUploadDokumen: File;
  linkImage = config.KOPBI_IMAGES_BASE_URL;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.doGetListBarang();
    this.formBuilder = this.fb.group({
      kodeBarang: ['', Validators.required],
      namaBarang: ['', Validators.required],
      harga: ['', Validators.required],
      hargaBeli: ['', Validators.required],
      keterangan: ['', Validators.required],
      stokBarang: ['', Validators.required],
      namaSuplier: ['', Validators.required],
      teleponSuplier: ['', Validators.required],
      kategori: ''
    });
  }

  doGetListBarang() {
    this.loading = true;
    const param = {
      search: this.filterText,
      mulai: (this.currentPage - 1) * this.itemsPerPage + 1,
      akhir: this.itemsPerPage,
      // kategori: 'barang',
    };
    this.service.getListBarangNew(param).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.totalItems = data.totalRow;
      // for (let x = 0; x < data.length; x++) {
      //   if (data[x].kategori === 'barang') {
      //     this.listBarang.push(data[x]);
      //   }
      // }
      // this.listBarang = _.sortBy(this.listBarang, ['id']);
      this.listBarang = data.data;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Barang!', 'Ooops!');
    });
  }

  doOpenFormAdd() {
    this.modalTitle = 'Tambah';
    this.isImageFound = false;
    this.formBuilder.reset();
    this.imageUploadDokumen = undefined;
    this.fileUpload.nativeElement.value = '';
  }

  doOpenFormEdit() {
    this.modalTitle = 'Ubah';
    this.isImageFound = true;
    this.imageUploadDokumen = undefined;
    this.fileUpload.nativeElement.value = '';
    this.linkURL = this.linkImage + '/barang/' + this.selectedBarang.kodeBarang + '.jpg';
    this.formBuilder.get('kodeBarang').setValue(this.selectedBarang.kodeBarang);
    this.formBuilder.get('namaBarang').setValue(this.selectedBarang.namaBarang);
    this.formBuilder.get('harga').setValue(numeral(Number(this.selectedBarang.harga)).format('0,0'));
    this.formBuilder.get('hargaBeli').setValue(numeral(Number(this.selectedBarang.hargaBeli)).format('0,0'));
    this.formBuilder.get('namaSuplier').setValue(this.selectedBarang.namaSuplier);
    this.formBuilder.get('teleponSuplier').setValue(this.selectedBarang.teleponSuplier);
    this.formBuilder.get('keterangan').setValue(this.selectedBarang.keterangan);
    this.formBuilder.get('stokBarang').setValue(numeral(Number(this.selectedBarang.stokBarang)).format('0,0'));
  }

  submit() {
    let param: any = {};
    if (this.modalTitle === 'Tambah') {
      param = {
        kodeBarang: this.formBuilder.get('kodeBarang').value,
        namaBarang: this.formBuilder.get('namaBarang').value,
        harga: this.formBuilder.get('harga').value.toString().replace(new RegExp(',', 'g'), ''),
        hargaBeli: this.formBuilder.get('hargaBeli').value.toString().replace(new RegExp(',', 'g'), ''),
        namaSuplier: this.formBuilder.get('namaSuplier').value,
        teleponSuplier: this.formBuilder.get('teleponSuplier').value,
        keterangan: this.formBuilder.get('keterangan').value,
        stokBarang: this.formBuilder.get('stokBarang').value.toString().replace(new RegExp(',', 'g'), ''),
        kategori: 'barang'
      };
    } else if (this.modalTitle === 'Ubah') {
      param = {
        id: this.selectedBarang.id,
        kodeBarang: this.formBuilder.get('kodeBarang').value,
        namaBarang: this.formBuilder.get('namaBarang').value,
        harga: this.formBuilder.get('harga').value.toString().replace(new RegExp(',', 'g'), ''),
        hargaBeli: this.formBuilder.get('hargaBeli').value.toString().replace(new RegExp(',', 'g'), ''),
        namaSuplier: this.formBuilder.get('namaSuplier').value,
        teleponSuplier: this.formBuilder.get('teleponSuplier').value,
        keterangan: this.formBuilder.get('keterangan').value,
        stokBarang: this.formBuilder.get('stokBarang').value.toString().replace(new RegExp(',', 'g'), ''),
        kategori: 'barang'
      };
    }
    this.service.doPostBarang(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListBarang();
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
    if (this.imageUploadDokumen !== undefined) {
      const formDataDokumen: FormData = new FormData();
      formDataDokumen.append('file', this.imageUploadDokumen);
      this.service.doPostUploadFile(formDataDokumen, 'barang', this.selectedBarang.kodeBarang).subscribe((res) => {
        this.loading = false;
      }, (err) => {
        this.loading = false;
        this.toastr.error('Terjadi kesalahan dalam upload Gambar!', 'Ooops!');
      });
    }
  }

  updateImageActive() {
    this.isImageFound = !this.isImageFound;
  }

  onImageChanged(event) {
    this.imageUploadDokumen = event.item(0);
  }

  setSelectedBarang(data: Barang) {
    this.selectedBarang = data;
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListBarang();
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }
}
