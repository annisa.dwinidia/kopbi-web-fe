import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../service/app.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import { Router } from '@angular/router';
import {Komponen} from '../../../model/Komponen';

@Component({
  selector: 'app-komponen',
  templateUrl: './komponen.component.html',
  styleUrls: ['./komponen.component.scss']
})
export class KomponenComponent implements OnInit {

  formBuilder: FormGroup;

  loading = false;

  addModal;
  selectedKomponen = new Komponen();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';

  listKomponen:     Komponen[] = [];
  listCalculate:     Komponen[] = [];
  listInfoApp:     Komponen[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.doGetListKomponenAll();
    this.formBuilder = this.fb.group({
      keterangan: ['', Validators.required],
      tipe: ['', Validators.required],
      nominal: ['', Validators.required],
      kode: [{value: '', disabled: true}, Validators.required]
    });
  }

  doGetListKomponenAll() {
    this.doGetListKomponen();
    this.doGetListCalculate();
    this.doGetListInfoApp();
  }

  doGetListKomponen() {
    this.listKomponen = [];
    this.loading = true;
    this.service.getListKomponen().subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.listKomponen = datas;
      // for (let x = 0; x < datas.length; x++) {
      //   const data = {
      //     id: datas[x].id,
      //     kode: datas[x].kode,
      //     keterangan: datas[x].keterangan,
      //     tipe: datas[x].tipe,
      //     nominal: datas[x].nominal
      //   };
      //   this.listKomponen.push(data);
      // }
      // console.log(datas);
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Komponen!', 'Ooops!');
    });
  }

  doGetListCalculate() {
    this.listCalculate = [];
    this.loading = true;
    this.service.getListCalculate().subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.listCalculate = datas;
      // for (let x = 0; x < datas.length; x++) {
      //   const data = {
      //     id: datas[x].id,
      //     kode: datas[x].kode,
      //     keterangan: datas[x].keterangan,
      //     tipe: datas[x].tipe,
      //     nominal: datas[x].nominal
      //   };
      //   this.listKomponen.push(data);
      // }
      // console.log(datas);
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Calculate!', 'Ooops!');
    });
  }

  doGetListInfoApp() {
    this.listInfoApp = [];
    this.loading = true;
    this.service.getListInfoApp().subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.listInfoApp = datas;
      // for (let x = 0; x < datas.length; x++) {
      //   const data = {
      //     id: datas[x].id,
      //     kode: datas[x].kode,
      //     keterangan: datas[x].keterangan,
      //     tipe: datas[x].tipe,
      //     nominal: datas[x].nominal
      //   };
      //   this.listKomponen.push(data);
      // }
      // console.log(datas);
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Info App!', 'Ooops!');
    });
  }

  doOpenFormAdd() {
    this.modalTitle = 'Tambah';
    this.formBuilder.reset();
  }

  doOpenFormEdit() {
    this.modalTitle = 'Ubah';
    this.formBuilder.get('kode').setValue(this.selectedKomponen.kode);
    this.formBuilder.get('keterangan').setValue(this.selectedKomponen.keterangan);
    this.formBuilder.get('tipe').setValue(this.selectedKomponen.tipe);
    if (this.selectedKomponen.tipe === 'INFO_APP') {
      this.formBuilder.get('nominal').setValue(this.selectedKomponen.nominal);
    } else {
      this.formBuilder.get('nominal').setValue(numeral(Number(this.selectedKomponen.nominal)).format('0,0'));
    }
  }

  submit() {
    let param: any = {};
    if (this.modalTitle === 'Tambah') {
      param = {
        kode: this.formBuilder.get('kode').value,
        keterangan: this.formBuilder.get('keterangan').value,
        tipe: this.formBuilder.get('tipe').value,
        nominal: this.formBuilder.get('nominal').value.toString().replace(new RegExp(',', 'g'), ''),
      };
    } else if (this.modalTitle === 'Ubah') {
      param = {
        id: this.selectedKomponen.id,
        kode: this.formBuilder.get('kode').value,
        keterangan: this.formBuilder.get('keterangan').value,
        tipe: this.formBuilder.get('tipe').value,
        nominal: this.formBuilder.get('nominal').value.toString().replace(new RegExp(',', 'g'), ''),
      };
    }
    this.service.doPostKomponen(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListKomponenAll();
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  setSelectedKomponen(data: Komponen) {
    this.selectedKomponen = data;
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListKomponen();
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }
}
