import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { AppService } from '../../../service/app.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import { Router } from '@angular/router';
import {Barang} from '../../../model/Barang';
import {config} from '../../../config/application-config';

@Component({
  selector: 'app-laporan',
  templateUrl: './laporan.component.html',
  styleUrls: ['./laporan.component.scss']
})
export class LaporanComponent implements OnInit {

  formBuilder: FormGroup;
  @ViewChild('fileUpload') fileUpload: ElementRef;
  @ViewChild('imageUpload') imageUpload: ElementRef;

  loading = false;

  addModal;
  selectedKonten;
  selectedKodeKategori = '';
  selectedIdKategori = '';

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';

  listKonten:     any[] = [];
  isImageFound = true;

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;

  isDesc = true;
  column: string;
  direction: number;

  fileUploadDokumen: File;
  imageUploadDokumen: File;
  linkImage = config.KOPBI_IMAGES_BASE_URL;
  linkURL = '';

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.doGetListKonten();
    this.formBuilder = this.fb.group({
      kodeKategori: ['laporan', Validators.required],
      namaKonten: ['', Validators.required],
      namaKategori: ['', Validators.required]
    });
  }

  doGetListKonten() {
    this.loading = true;
    const param = {
      search: this.filterText
    };
    this.service.getListKonten('laporan').subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.listKonten = datas;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Konten!', 'Ooops!');
    });
  }

  doOpenFormAdd() {
    this.isImageFound = false;
    this.selectedKonten = null;
    this.selectedIdKategori = '';
    this.selectedKodeKategori = '';
    this.modalTitle = 'Tambah';
    if (this.imageUpload) {
      this.imageUpload.nativeElement.value = '';
    }
    if (this.fileUpload) {
      this.fileUpload.nativeElement.value = '';
    }
    this.formBuilder.reset();
    this.formBuilder.get('kodeKategori').setValue('laporan');
  }

  doOpenFormEdit() {
    this.formBuilder.reset();
    // this.fileUpload.nativeElement.value = '';
    if (this.imageUpload) {
      this.imageUpload.nativeElement.value = '';
    }
    if (this.fileUpload) {
      this.fileUpload.nativeElement.value = '';
    }
    this.isImageFound = true;
    this.modalTitle = 'Ubah';
    this.linkURL = this.linkImage + '/informasi/' + this.selectedIdKategori + '.jpg';
    this.formBuilder.get('kodeKategori').setValue(this.selectedKonten.kodeKategori);
    this.formBuilder.get('namaKonten').setValue(this.selectedKonten.namaKonten);
    this.formBuilder.get('namaKategori').setValue(this.selectedKonten.namaKategori);
  }

  updateImageActive() {
    this.isImageFound = !this.isImageFound;
  }

  submit() {
    let paramText: any = {};
    let formDataDokumen = new FormData();
    if (this.modalTitle === 'Tambah') {
      paramText = {
        kodeKategori: this.formBuilder.get('kodeKategori').value,
        namaKonten: this.formBuilder.get('namaKonten').value,
        namaKategori: this.formBuilder.get('namaKategori').value,
        keterangan: ''
      };
    } else if (this.modalTitle === 'Ubah') {
      paramText = {
        id: this.selectedKonten.id,
        kodeKategori: this.formBuilder.get('kodeKategori').value,
        namaKonten: this.formBuilder.get('namaKonten').value,
        namaKategori: this.formBuilder.get('namaKategori').value,
        keterangan: ''
      };
    }

    if (this.fileUploadDokumen !== undefined) {
      formDataDokumen.append('image', this.fileUploadDokumen);
    }

    if (this.imageUploadDokumen !== undefined) {
      formDataDokumen.append('image', this.imageUploadDokumen);
    }

    formDataDokumen.append('param', JSON.stringify(paramText));

    // console.log(formDataDokumen);
    // for (const key of formDataDokumen.keys()) {
    //   console.log(key);
    // }
    if (this.imageUploadDokumen === undefined && this.fileUploadDokumen === undefined) {
      this.toastr.error('Silahkan Upload Image/File terlebih dahulu!', 'Ooops!');
    } else {
      this.service.doPostKonten(formDataDokumen).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success(this.successMessage, 'Success!');
          this.doGetListKonten();
        } else {
          this.toastr.error(this.errorMesssage, 'Ooops!');
        }
      }, (err) => {
        this.loading = false;
        this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
      });
    }
    this.doGetListKonten();
  }

  onImageChanged(event) {
    this.imageUploadDokumen = event.item(0);
  }

  onFileChanged(event) {
    this.fileUploadDokumen = event.item(0);
  }


  setSelectedKonten(data: any) {
    this.selectedKonten = data;
    this.selectedKodeKategori = data.kodeKategori;
    this.selectedIdKategori = data.id;
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListKonten();
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }
}
