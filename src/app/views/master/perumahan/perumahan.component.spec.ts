import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerumahanComponent } from './perumahan.component';

describe('PerumahanComponent', () => {
  let component: PerumahanComponent;
  let fixture: ComponentFixture<PerumahanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerumahanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerumahanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
