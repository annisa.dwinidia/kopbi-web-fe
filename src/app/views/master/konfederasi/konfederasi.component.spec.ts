import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KonfederasiComponent } from './konfederasi.component';

describe('KonfederasiComponent', () => {
  let component: KonfederasiComponent;
  let fixture: ComponentFixture<KonfederasiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KonfederasiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KonfederasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
