import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../service/app.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import { Konfederasi } from "../../../model/Konfederasi";

@Component({
  selector: 'app-konfederasi',
  templateUrl: './konfederasi.component.html',
  styleUrls: ['./konfederasi.component.scss']
})
export class KonfederasiComponent implements OnInit {

  formBuilder: FormGroup;

  loading = false;

  addModal;
  selectedKonfederasi = new Konfederasi();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';

  listKonfederasi:     Konfederasi[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.doGetListKonfederasi();
    this.formBuilder = this.fb.group({
      kodeKonfederasi: ['', Validators.required],
      namaKonfederasi: ['', Validators.required],
      alamatKonfederasi: ['', Validators.required],
      emailKonfederasi: ['', [Validators.required]],
      teleponKonfederasi: ['', Validators.required],
      registrasiKonfederasi: ['', Validators.required],
    });
  }

  doGetListKonfederasi() {
    this.loading = true;
    const param = {
      search: this.filterText
    };
    this.service.getListKonfederasi(param).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listKonfederasi = _.sortBy(data, ['id']);
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Konfederasi!', 'Ooops!');
    });
  }

  doOpenFormAdd() {
    this.modalTitle = 'Tambah';
    this.formBuilder.reset();
  }

  doOpenFormEdit() {
    this.modalTitle = 'Ubah';
    this.formBuilder.get('kodeKonfederasi').setValue(this.selectedKonfederasi.kodeKonfederasi);
    this.formBuilder.get('namaKonfederasi').setValue(this.selectedKonfederasi.namaKonfederasi);
    this.formBuilder.get('alamatKonfederasi').setValue(this.selectedKonfederasi.alamatKonfederasi);
    this.formBuilder.get('emailKonfederasi').setValue(this.selectedKonfederasi.emailKonfederasi);
    this.formBuilder.get('teleponKonfederasi').setValue(this.selectedKonfederasi.teleponKonfederasi);
    this.formBuilder.get('registrasiKonfederasi').setValue(this.selectedKonfederasi.registrasiKonfederasi);
  }

  submit() {
    let param: any = {};
    if (this.modalTitle === 'Tambah') {
      param = {
        kodeKonfederasi: this.formBuilder.get('kodeKonfederasi').value,
        namaKonfederasi: this.formBuilder.get('namaKonfederasi').value,
        alamatKonfederasi: this.formBuilder.get('alamatKonfederasi').value,
        emailKonfederasi: this.formBuilder.get('emailKonfederasi').value,
        teleponKonfederasi: this.formBuilder.get('teleponKonfederasi').value,
        registrasiKonfederasi: this.formBuilder.get('registrasiKonfederasi').value
      };
    } else if (this.modalTitle === 'Ubah') {
      param = {
        id: this.selectedKonfederasi.id,
        kodeKonfederasi: this.formBuilder.get('kodeKonfederasi').value,
        namaKonfederasi: this.formBuilder.get('namaKonfederasi').value,
        alamatKonfederasi: this.formBuilder.get('alamatKonfederasi').value,
        emailKonfederasi: this.formBuilder.get('emailKonfederasi').value,
        teleponKonfederasi: this.formBuilder.get('teleponKonfederasi').value,
        registrasiKonfederasi: this.formBuilder.get('registrasiKonfederasi').value
      };
    }
    this.service.doPostKonfederasi(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListKonfederasi();
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  setSelectedKonfederasi(data: Konfederasi) {
    this.selectedKonfederasi = data;
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListKonfederasi();
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }
}
