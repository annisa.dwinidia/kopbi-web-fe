import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../model/user';
import { HttpHeaders } from '@angular/common/http';
import { Observable, timer} from 'rxjs/';
import { config } from '../../config/application-config';
import { Title } from '@angular/platform-browser';
import { AppService } from '../../service/app.service';
import { AuthService } from '../../service/auth.service';
import { environment } from '../../../environments/environment';
import { AESEncryptDecryptService } from '../../service/aesencrypt-decrypt-service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('userNameInput')
  userNameInput: ElementRef;

  @ViewChild('userPasswordInput')
  userPasswordInput: ElementRef;

  un = '';
  pw = '';
  token: string;
  isErrorFound = false;
  today: number = Date.now();
  loading = false;
  errorMessage;

  dataUser: any;

  version: string = environment.VERSION;

  constructor(private router: Router, private projectService: AppService, private titleService: Title,
              private auth: AuthService, private encryption: AESEncryptDecryptService) {}

  ngOnInit() {
    localStorage.clear();
  }

  doSetFocusUserName() {
    timer(30).subscribe(() => {
      this.userNameInput.nativeElement.focus();
    });
  }

  doSetFocusUserPasswd() {
    timer(30).subscribe(() => {
      this.userPasswordInput.nativeElement.focus();
    });
  }

  doLogin() {
    if (this.un && this.un.length > 0 && this.pw && this.pw.length > 0) {
      this.loading = true;
      const param = {
        userName: this.un,
        password: this.encryption.encrypt(this.pw)
      };
      let headers = new HttpHeaders();
      headers = headers.set('Content-Type', 'application/json; charset=utf-8');
      headers = headers.set('token', 'U2FsdGVkX19emypgqSLb6nLxUO5CO3eG7avTQXU045E=');
      this.projectService.getUserInfo(param, headers)
        .subscribe((res) => {
          if (res.success) {
            const usr: string = JSON.stringify(res.data.user);
            const jwtToken = res.data.session.jwt;
            this.auth.sendToken(usr);
            this.auth.sendJWTToken(jwtToken);
            this.loading = false;
            if (this.pw === '123456') {
              this.router.navigate(['/change-password']);
            } else {
              this.router.navigate(['/home/dashboard'], { queryParams: { imageShow: 'true'}, skipLocationChange: false });
            }
          } else {
            this.un = '';
            this.pw = '';
            this.errorMessage = 'Wrong Username or Password';
            this.isErrorFound = true;
            this.loading = false;
          }
        }, (err) => {
          this.errorMessage = 'Connection failed. Please try again';
          this.isErrorFound = true;
          console.log(err);
          this.loading = false;
        })
    } else {
      this.errorMessage = 'Wrong Username or Password';
      this.isErrorFound = true;
      this.router.navigate(['/login']);
    }
  }
}

