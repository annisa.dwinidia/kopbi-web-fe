import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import { FormBuilder, FormGroup} from '@angular/forms';
import { AppService } from '../../service/app.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import { User} from '../../model/user';
import { AuthService} from '../../service/auth.service';
import { config } from '../../config/application-config';
import {ModalDirective} from 'ngx-bootstrap';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  navigationSubscription;
  @ViewChild('autoShownModal') autoShownModal: ModalDirective;
  dataUser: User;
  loading = false;
  listRekap: any[] = [];

  totalAnggota = 0;
  totalAnggotaBaru = 0;
  totalAnggotaAktif = 0;
  totalAnggotaNonAktif = 0;

  totalPengajuan = 0;
  totalPengajuanBaru = 0;
  totalPengajuanApproveHrd = 0;
  totalPengajuanApprovePengawas = 0;
  totalPengajuanCancel = 0;
  totalPengajuanClose = 0;

  totalPenarikan = 0;
  totalPenarikanBaru = 0;
  totalPenarikanApproveHrd = 0;
  totalPenarikanApprovePengawas = 0;
  totalPenarikanCancel = 0;
  totalPenarikanClose = 0;

  totalPinjaman = 0;
  totalPinjamanLunas = 0;

  saldoSimpanan;
  sisaPinjaman = 0;
  isModalShown = true;
  linkImage = config.KOPBI_IMAGES_BASE_URL;
  selectedIdImage = '36';
  sub: any;
  tanggalRegistrasi = '';

  constructor(private service: AppService, private toastr: ToastrService, private auth: AuthService,
              private router: Router, private route: ActivatedRoute) {
    this.dataUser = JSON.parse(this.auth.getToken());
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.sub = this.route.queryParams.subscribe(params => {
          if (params['imageShow']) {
            if (params['imageShow'] === 'true') {
              this.isModalShown = true;
            }
          } else {
            this.isModalShown = false;
          }
        });
        this.initialiseInvites();
      }
    });
  }

  initialiseInvites() {
    this.doGetDashboardAnggota();
    this.doGetDashboardPengajuan();
    this.doGetDashboardPinjaman();
    this.doGetListRekapSaldo();
    if (this.dataUser.role === 'AGT') {
      this.doGetSaldoSimpanan();
      this.doGetSisaPinjaman();
    }
    if (this.dataUser.role === 'HRD') {
      this.doGetListCompany();
    }
  }

  onHidden(): void {
    this.isModalShown = false;
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.dataUser = JSON.parse(this.auth.getToken());
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      const comp = _.find(data, ['kodePerusahaan', this.dataUser.kodePerusahaan]);
      this.tanggalRegistrasi = comp.registrasiPerusahaan;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  goToProfile(id: number) {
    this.router.navigate(['/home/profile'], { queryParams: { id: id }, skipLocationChange: false });
  }

  goToDataAnggota(kodePerusahaan) {
    this.router.navigate(['/home/data-anggota'], { queryParams: { id: kodePerusahaan }, skipLocationChange: false });
  }

  goToFeePerusahaan() {
    this.router.navigate(['/home/fee-perusahaan']);
  }

  gotoLaporanSaldo() {
    this.router.navigate(['/home/laporan/transaksi']);
  }

  gotoPengajuanPinjaman(nik: any, name: any) {
    if (this.dataUser.role === 'AGT') {
      this.router.navigate(['/home/pengajuan/pinjaman/anggota'], {queryParams: {nik: nik, name: name}, skipLocationChange: false});
    } else {
      this.router.navigate(['/home/pengajuan/pinjaman']);
    }
  }

  gotoPengajuanPenarikan(nik: any, name: any) {
    if (this.dataUser.role === 'AGT') {
      this.router.navigate(['/home/pengajuan/penarikan/anggota'], {queryParams: {nik: nik, name: name}, skipLocationChange: false});
    } else {
      this.router.navigate(['/home/pengajuan/penarikan']);
    }
  }

  doGetSaldoSimpanan() {
    this.loading = true;
    this.saldoSimpanan = null;
    this.service.getSaldoSimpanan(this.dataUser.nomorAnggota, {}).subscribe((res) => {
      this.loading = false;
      if (res.data) {
        const data = JSON.parse(res.data);
        this.saldoSimpanan = data;
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Saldo Simpanan!', 'Ooops!');
    });
  }

  doGetSisaPinjaman() {
    this.loading = true;
    this.sisaPinjaman = 0;
    const param: any = {
      tanggalMulai: '',
      tanggalAkhir: '',
      statusPinjaman: 'BELUM',
      mulai: 1,
      akhir: 100,
      nomorAnggota: this.dataUser.kodeAnggota
    };
    this.service.getListPinjaman(param).subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      if (datas.data.length > 0) {
        const dt = datas.data;
        for (let x = 0; x < dt.length; x++) {
          this.sisaPinjaman += parseFloat(dt[x].sisaPinjaman);
        }
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Saldo Simpanan!', 'Ooops!');
    });
  }

  gotoSimpanan(nomorAnggota: any, name: any, kodePerusahaan: any) {
    if (this.dataUser.role === 'AGT') {
      this.router.navigate(['/home/simpanan/anggota'], {queryParams: {kodeAnggota: nomorAnggota, name: name, company: kodePerusahaan}, skipLocationChange: false});
    } else {
      this.router.navigate(['/home/simpanan']);
    }
  }

  gotoPinjaman(nomorAnggota: any, name: any) {
    if (this.dataUser.role === 'AGT') {
      this.router.navigate(['/home/pinjaman/anggota'], {queryParams: {kodeAnggota: nomorAnggota, name: name}, skipLocationChange: false});
    } else {
      this.router.navigate(['/home/pinjaman']);
    }
  }

  doGetDashboardAnggota() {
    this.loading = true;
    const param = {
      kodePerusahaan: this.dataUser.role === 'HRD' ? this.dataUser.kodePerusahaan : ''
    };
    this.service.getDashboardAnggota(param).subscribe((res) => {
      this.loading = false;
      const resp = JSON.parse(res.data);
      this.totalAnggota = resp.totalAnggota;
      this.totalAnggotaBaru = resp.totalAnggotaBaru;
      this.totalAnggotaAktif = resp.totalAnggotaAktif;
      this.totalAnggotaNonAktif = resp.totalAnggotaNonAktif;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Anggota!', 'Ooops!');
    });
  }

  doGetDashboardPengajuan() {
    this.loading = true;
    const param = {
      kodePerusahaan: this.dataUser.role === 'HRD' ? this.dataUser.kodePerusahaan : ''
    };
    this.service.getDashboardPengajuan(param).subscribe((res) => {
      this.loading = false;
      const resp = JSON.parse(res.data);
      this.totalPengajuanBaru = resp.totalPengajuanBaru;
      this.totalPengajuanApproveHrd = resp.totalPengajuanApproveHrd;
      this.totalPengajuanApprovePengawas = resp.totalPengajuanApprovePengawas;
      this.totalPengajuanCancel = resp.totalPengajuanCancel;
      this.totalPengajuanClose = resp.totalPengajuanClose;
      this.totalPenarikanBaru = resp.totalPengajuanPenarikanBaru;
      this.totalPenarikanApproveHrd = resp.totalPengajuanPenarikanApproveHrd;
      this.totalPenarikanApprovePengawas = resp.totalPengajuanPenarikanApprovePengawas;
      this.totalPenarikanCancel = resp.totalPengajuanPenarikanCancel;
      this.totalPenarikanClose = resp.totalPengajuanPenarikanClose;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Pengajuan Pinjaman!', 'Ooops!');
    });
  }

  doGetDashboardPinjaman() {
    this.loading = true;
    const param = {
      kodePerusahaan: this.dataUser.role === 'HRD' ? this.dataUser.kodePerusahaan : ''
    };
    this.service.getDashboardPinjaman(param).subscribe((res) => {
      this.loading = false;
      const resp = JSON.parse(res.data);
      this.totalPinjaman = resp.totalPinjaman;
      this.totalPinjamanLunas = resp.totalPinjamanLunas;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Pinjaman!', 'Ooops!');
    });
  }

  doGetListRekapSaldo() {
    this.loading = true;
    this.service.getListRekapSaldo({}).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listRekap =  data.data;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Rekap Saldo!', 'Ooops!');
    });
  }

}
