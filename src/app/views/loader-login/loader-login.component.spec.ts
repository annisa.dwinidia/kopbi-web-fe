import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoaderLoginComponent } from './loader-login.component';

describe('LoaderLoginComponent', () => {
  let component: LoaderLoginComponent;
  let fixture: ComponentFixture<LoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoaderLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoaderLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
