import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { Router } from '@angular/router';
import { config } from '../../config/application-config';
import { AppService } from '../../service/app.service';
import { AuthService } from '../../service/auth.service';
import { environment } from '../../../environments/environment';
import { AESEncryptDecryptService } from '../../service/aesencrypt-decrypt-service.service';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import { Perusahaan} from '../../model/Perusahaan';
import { Konfederasi} from '../../model/Konfederasi';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
declare let $: any;
import * as _ from 'lodash';

@Component({
  selector: 'app-register',
  templateUrl: 'register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [DatePipe]
})
export class RegisterComponent implements OnInit {
  token: string;
  isErrorFound = false;
  today: number = Date.now();
  loading = false;
  dataUser: any;
  version: string = environment.VERSION;

  @ViewChild('fileFoto')
  fileFoto: ElementRef;
  @ViewChild('fileAnggota')
  fileAnggota: ElementRef;

  formBuilder: FormGroup;
  linkImage = config.KOPBI_IMAGES_BASE_URL;
  successMessage    = 'Data anda telah sukses di simpan, untuk memverifikasi data anda silahkan buka email anda. Terimakasih';
  errorMesssage     = 'Terjadi kesalahan dalam Registrasi! Silahkan coba lagi!';
  listKonfederasi:   Konfederasi[] = [];
  listPerusahaan: Perusahaan[] = [];
  configSelectKonfederasi = null;

  uploadedFileKtp: File;
  uploadedFileAnggota: File;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private router: Router, private auth: AuthService, private datePipe: DatePipe,
              private encryption: AESEncryptDecryptService) { }

  ngOnInit() {
    this.dataUser = JSON.parse(this.auth.getToken());
    this.formBuilder = this.fb.group({
      kodeAnggota: [''],
      nomorAnggota:  [''],
      nama:  ['', Validators.required],
      nomorKtp:  ['', Validators.required],
      nomorNik:  ['', Validators.required],
      jenisKelamin:  ['', Validators.required],
      tempatLahir:  ['', Validators.required],
      tanggalLahir:  ['', Validators.required],
      status:  ['', Validators.required],
      pekerjaan:  ['', Validators.required],
      alamat:  ['', Validators.required],
      nomorHp:  ['', Validators.required],
      kodePerusahaan:  [''],
      namaPerusahaan:  [''],
      alamatPerusahaan:  [''],
      emailPerusahaan:  [''],
      lokasiPenempatan: ['', Validators.required],
      kodeJabatan:  [''],
      namaJabatan:  ['', Validators.required],
      kodeBank:  [''],
      namaBank:  ['', Validators.required],
      cabangBank:  ['', Validators.required],
      nomorRekening:  ['', Validators.required],
      tanggalRegistrasi: [''],
      password: [''],
      statusAnggota: [''],
      role: [''],
      namaKonfederasi: [''],
      pendapatan: [''],
      simpananWajib: [''],
      simpananSukarela: [''],
      jabatanKeanggotaan: [''],
      namaSaudaraDekat: ['', Validators.required],
      hubunganSaudara: ['', Validators.required],
      alamatSaudara: ['', Validators.required],
      nomorHpSaudara: ['', Validators.required],
      emailPribadi: ['', Validators.required],
      selectedKonfederasi: ''
    });
    // this.doGetListKonfederasi();
    this.configSelectKonfederasi = {
      displayKey: 'namaKonfederasi',
      search: true,
      height: '200px',
      placeholder: 'Pilih Konfederasi',
      customComparator: () => {},
      limitTo: this.listKonfederasi.length,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Konfederasi..',
      // searchOnKey: 'departmentCode'
    };
    // this.doGetListCompany();
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetListKonfederasi() {
    this.loading = true;
    const param = {
      search: ''
    };
    this.service.getListKonfederasi(param).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listKonfederasi = data;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Konfederasi!', 'Ooops!');
    });
  }

  setKonfederasi() {
    const konfederasi = this.formBuilder.get('selectedKonfederasi').value;
    // console.log(konfederasi);
    this.formBuilder.get('namaKonfederasi').setValue(konfederasi.namaKonfederasi);
  }

  submit() {
    this.loading = true;
    const  param = {
      // kodeAnggota: '',
      nomorAnggota:  '',
      nama:  this.formBuilder.get('nama').value,
      nomorKtp: this.formBuilder.get('nomorKtp').value,
      nomorNik:  this.formBuilder.get('nomorNik').value,
      jenisKelamin:  this.formBuilder.get('jenisKelamin').value,
      tempatLahir:  this.formBuilder.get('tempatLahir').value,
      tanggalLahir:  this.formBuilder.get('tanggalLahir').value,
      status:  this.formBuilder.get('status').value,
      pekerjaan:  this.formBuilder.get('pekerjaan').value,
      alamat:  this.formBuilder.get('alamat').value,
      nomorHp:  this.formBuilder.get('nomorHp').value,
      // kodePerusahaan:  this.formBuilder.get('kodePerusahaan').value,
      // namaPerusahaan:  this.formBuilder.get('namaPerusahaan').value,
      // alamatPerusahaan: this.formBuilder.get('alamatPerusahaan').value,
      // emailPerusahaan:  this.formBuilder.get('emailPerusahaan').value,
      kodePerusahaan:  'PRBD',
      namaPerusahaan:  '',
      alamatPerusahaan: '',
      emailPerusahaan:  '',
      lokasiPenempatan: this.formBuilder.get('lokasiPenempatan').value,
      kodeJabatan:  'AGT',
      namaJabatan: 'Anggota',
      kodeBank:  '',
      namaBank:  this.formBuilder.get('namaBank').value,
      cabangBank:  this.formBuilder.get('cabangBank').value,
      nomorRekening:  this.formBuilder.get('nomorRekening').value,
      // tanggalRegistrasi: this.formBuilder.get('tanggalRegistrasi').value,
      tanggalRegistrasi: this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
      password: '',
      // statusAnggota: this.formBuilder.get('statusAnggota').value,
      statusAnggota: 'REG',
      // role: this.formBuilder.get('role').value,
      role: 'AGT',
      // namaKonfederasi: this.formBuilder.get('namaKonfederasi').value,
      namaKonfederasi: '',
      pendapatan: !this.formBuilder.get('pendapatan').value ? '' : this.formBuilder.get('pendapatan').value.toString().replace(new RegExp(',', 'g'), ''),
      simpananWajib: !this.formBuilder.get('simpananWajib').value ? '' : this.formBuilder.get('simpananWajib').value.toString().replace(new RegExp(',', 'g'), ''),
      simpananSukarela: !this.formBuilder.get('simpananSukarela').value ? '' : this.formBuilder.get('simpananSukarela').value.toString().replace(new RegExp(',', 'g'), ''),
      // jabatanKeanggotaan: this.formBuilder.get('jabatanKeanggotaan').value,
      jabatanKeanggotaan: this.formBuilder.get('namaJabatan').value,
      namaSaudaraDekat: this.formBuilder.get('namaSaudaraDekat').value,
      hubunganSaudara: this.formBuilder.get('hubunganSaudara').value,
      alamatSaudara: this.formBuilder.get('alamatSaudara').value,
      nomorHpSaudara: this.formBuilder.get('nomorHpSaudara').value,
      emailPribadi: this.formBuilder.get('emailPribadi').value
    };
    this.service.doRegistration(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.goToLogin();
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  onFileChangedAnggota(event) {
    this.uploadedFileAnggota = event.item(0);
  }

  onFileChangedKtp(event) {
    this.uploadedFileKtp = event.item(0);
  }

  goToLogin() {
    this.router.navigate(['/login']);
  }
}

