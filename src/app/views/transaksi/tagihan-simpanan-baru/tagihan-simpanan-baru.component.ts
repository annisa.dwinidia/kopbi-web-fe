import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Simpanan} from '../../../model/Simpanan';
import {AppService} from '../../../service/app.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';
import {User} from "../../../model/user";
import {AuthService} from "../../../service/auth.service";
import {Perusahaan} from '../../../model/Perusahaan';
declare let $: any;
@Component({
  selector: 'app-tagihan-simpanan-baru',
  templateUrl: './tagihan-simpanan-baru.component.html',
  styleUrls: ['./tagihan-simpanan-baru.component.scss'],
  providers: [DatePipe]
})
export class TagihanSimpananBaruComponent implements OnInit {

  formBuilder: FormGroup;

  loading = false;

  sub: any;
  nik;
  name;
  company;
  dataUser: User;
  addModal;

  selectedSimpanan = new Simpanan();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';

  listSimpanan:     any[] = [];
  listPerusahaan:      Perusahaan[] = [];
  selectedPerusahaan;

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;
  tanggalMulai = '';
  tanggalAkhir = '';
  currentMonth = new Date().getMonth() + 1;
  currentYear = new Date().getFullYear();

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private route: ActivatedRoute,
              private auth: AuthService) { }

  ngOnInit() {
    $('select').selectpicker();
    this.dataUser = JSON.parse(this.auth.getToken());
    this.sub = this.route.queryParams.subscribe(params => {
      this.nik = !params['nik'] ? this.dataUser.nomorNik : params['nik'];
      this.name = !params['name'] ? this.dataUser.nama : params['name'];
      this.company = !params['company'] ? this.dataUser.kodePerusahaan : params['company'];
    });
    this.tanggalMulai = this.datePipe.transform(new Date(this.currentYear + '-' + this.currentMonth + '-01'), 'yyyy-MM-dd');
    this.tanggalAkhir = this.datePipe.transform(new Date(this.currentYear, this.currentMonth, 0), 'yyyy-MM-dd');
    this.doGetListCompany();
    this.doGetListSimpanan();
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetListSimpanan() {
    this.loading = true;
    let selectedPerusahaan;
    if (this.dataUser.role === 'HRD') {
      selectedPerusahaan = this.dataUser.kodePerusahaan;
    } else {
      selectedPerusahaan = !this.selectedPerusahaan ? '' : JSON.parse(this.selectedPerusahaan).kodePerusahaan;
    }
    const param = {
      kodePerusahaan: selectedPerusahaan,
      search: '',
      mulai: (this.currentPage - 1) * this.itemsPerPage + 1,
      akhir: this.itemsPerPage,
      status: 'N'
    };
    this.service.getListAllAnggota(param).subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.totalItems = datas.totalRow;
      const data = datas.data;
      this.listSimpanan =  _.orderBy(data, function(dateObj) {
        return new Date(dateObj.tanggalRegistrasi);
      }, 'desc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Anggota!', 'Ooops!');
    });
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  gotoList(nik: number, name: string, kodePerusahaan: string, kodeAnggota: string) {
    this.router.navigate(['/home/simpanan/anggota'], { queryParams: { kodeAnggota: kodeAnggota, name: name, company: kodePerusahaan }, skipLocationChange: false });
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListSimpanan();
  }

  backClicked() {
    this.router.navigate(['/home/simpanan']);
  }
}
