import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagihanSimpananBaruComponent } from './tagihan-simpanan-baru.component';

describe('TagihanSimpananBaruComponent', () => {
  let component: TagihanSimpananBaruComponent;
  let fixture: ComponentFixture<TagihanSimpananBaruComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagihanSimpananBaruComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagihanSimpananBaruComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
