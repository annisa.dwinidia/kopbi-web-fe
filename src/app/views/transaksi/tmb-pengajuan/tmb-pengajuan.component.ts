import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import {Pengajuan} from '../../../model/pengajuan';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppService} from '../../../service/app.service';
import {DatePipe} from '@angular/common';
import {User} from '../../../model/user';
import {AuthService} from '../../../service/auth.service';
import {Perusahaan} from "../../../model/Perusahaan";
import {Anggota} from "../../../model/anggota";
import {Barang} from "../../../model/Barang";
import {configKOPBI} from '../../../config/application-config';

declare let $: any;

@Component({
  selector: 'app-tmb-pengajuan',
  templateUrl: './tmb-pengajuan.component.html',
  styleUrls: ['./tmb-pengajuan.component.scss'],
  providers: [DatePipe]
})
export class TmbPengajuanComponent implements OnInit {

  formBuilder: FormGroup;
  dataUser: User;

  loading = false;
  addModal;
  selectedPengajuan = new Pengajuan();
  selectedPerusahaan;
  selectedAnggota;
  selectedItem;
  selectedLamaAngsuran;
  selectedJenisAngsuran;

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';
  tipePengajuan     = '';

  listPengajuan:     Pengajuan[] = [];
  listPerusahaan: Perusahaan[] = [];
  listAnggota: Anggota[] = [];
  listLamaAngsuran: any;
  listSimulasi: any[] = [];
  listItem: Barang[] = [];
  listKendaraan: Barang[] = [];
  listPerumahan: Barang[] = [];
  listJenisAngsuran: any[] = [];

  configSelectPerusahaan = null;
  configSelectAnggota = null;
  configSelectItem = null;
  configSelectLamaAngsuran = null;
  configSelectJenisAngsuran = null;

  totalAngsuran = 0;
  totalBunga = 0;
  totalBiayaAdmin = 0;
  totalTagihan = 0;

  persentaseBunga = 0;
  bungaPinjaman = 0;
  bungaPinjaman2 = 0;
  biayaAdmin = 0;
  bungaKendaraan = 0;
  bungaPerumahan = 0;

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;

  isDesc = true;
  column: string;
  direction: number;
  selectedName = '';
  saldoSimpanan;

  fileUploadDokumen: File;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    $('select').selectpicker();
    this.dataUser = JSON.parse(this.auth.getToken());
    this.configSelectPerusahaan = {
      displayKey: 'namaPerusahaan',
      search: true,
      height: 'auto',
      placeholder: 'Pilih Perusahaan',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Perusahaan..',
      // searchOnKey: 'departmentCode'
    };
    this.configSelectAnggota = {
      displayKey: 'nama',
      search: true,
      height: 'auto',
      placeholder: 'Pilih Anggota',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Anggota..',
      // searchOnKey: 'departmentCode'
    };
    this.configSelectItem = {
      displayKey: 'namaBarang',
      search: true,
      height: 'auto',
      placeholder: 'Pilih Barang',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Barang..',
      // searchOnKey: 'departmentCode'
    };
    this.configSelectLamaAngsuran = {
      displayKey: '',
      search: true,
      height: 'auto',
      placeholder: 'Pilih Lama Angsuran',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Lama Angsuran (maks. 24 Bulan)..',
      // searchOnKey: 'departmentCode'
    };
    this.configSelectJenisAngsuran = {
      displayKey: 'name',
      search: false,
      height: 'auto',
      placeholder: 'Pilih Jenis Angsuran',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Jenis Angsuran',
      // searchOnKey: 'departmentCode'
    };

    this.listLamaAngsuran = Array(24).fill([], 0, 24).map((x, i) => i + 1);
    this.formBuilder = this.fb.group({
      kodeTipePengajuan: ['', Validators.required],
      tipePengajuan: ['', Validators.required],
      nomorKtp: [''],
      nama: [''],
      nomorAnggota: [''],
      tanggalPengajuan: [''],
      lamaAngsuran: ['', Validators.required],
      nominalAngsuran: [''],
      nominalPengajuan: ['', Validators.required],
      persenBunga: [''],
      nominalBunga: [''],
      totalBunga: [''],
      biayaAdmin: [''],
      kodeBarang: [''],
      namaBarang: [''],
      ambilDariKas: [''],
      ketKas: [''],
      keterangan: ['', Validators.required],
      statusPengajuan: [''],
      tanggalPerubahan: [''],
      tanggalTempo: [''],
      tanggalJatuhTempo: [''],
      nomorPinjaman: [''],
      kodePerusahaan: ['']
    });
    this.formBuilder.get('lamaAngsuran').valueChanges.subscribe(data => {
      this.clearListSimulation();
      this.getSimulation(data);
    });
    this.formBuilder.get('kodeTipePengajuan').valueChanges.subscribe(data => {
      this.setTipengajuan(data);
    });
    this.listJenisAngsuran = [
      { 'code': 'UANG', 'name': 'Uang'},
      { 'code': 'BRG', 'name': 'Barang'},
      { 'code': 'PERUM', 'name': 'Perumahan'},
    ];
    this.doGetListCompany();
    this.doGetListItem();
    this.doGetListPersentase();
    if (this.dataUser.role === 'AGT') {
      this.doGetSaldoSimpanan(this.dataUser);
    }
  }

  doGetListMember() {
    // this.loading = true;
    const param = {
      kodePerusahaan: !this.selectedPerusahaan ? this.dataUser.kodePerusahaan : JSON.parse(this.selectedPerusahaan).kodePerusahaan,
      search: this.selectedName,
      mulai: 1,
      akhir: 50,
      status: ''
    };
    this.service.getListAllAnggota(param).subscribe((res) => {
    // const perusahaan = !this.selectedPerusahaan ? '' : JSON.parse(this.selectedPerusahaan);
    // this.service.getListAnggotaPerusahaan(perusahaan.kodePerusahaan, 1, 100).subscribe((res) => {
    //   this.loading = false;
      const data = JSON.parse(res.data);
      this.listAnggota = _.sortBy(data.data, ['id']);
    }, (err) => {
      // this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Anggota!', 'Ooops!');
    });
  }

  doGetListPersentase() {
    this.loading = true;
    this.service.getListPersentase().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      for (let x = 0; x < data.length; x++) {
        if (data[x].kode === 'BUNGA_PINJAMAN') {
          this.bungaPinjaman = parseFloat(data[x].nominal);
        } else if (data[x].kode === 'BUNGA_PINJAMAN2') {
          this.bungaPinjaman2 = parseFloat(data[x].nominal);
        } else if (data[x].kode === 'BIAYA_ADMIN') {
          this.biayaAdmin = parseFloat(data[x].nominal);
        } else if (data[x].kode === 'BUNGA_KENDARAAN') {
          this.bungaKendaraan = parseFloat(data[x].nominal);
        } else if (data[x].kode === 'BUNGA_PERUMAHAN') {
          this.bungaPerumahan = parseFloat(data[x].nominal);
        }
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Komponen!', 'Ooops!');
    });
  }

  doGetSaldoSimpanan(member) {
    this.loading = true;
    this.saldoSimpanan = null;
    const nomorNik = member.nomorAnggota;
    this.service.getSaldoSimpanan(nomorNik, {}).subscribe((res) => {
      this.loading = false;
      if (res.data) {
        const datas = JSON.parse(res.data);
        this.saldoSimpanan = datas;
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Saldo Simpanan!', 'Ooops!');
    });
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetListItem() {
    this.listItem = [];
    this.listPerumahan = [];
    this.listKendaraan = [];
    this.loading = true;
    const param = {
      search: ''
    };
    this.service.getListBarang(param).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      for (let x = 0; x < data.length; x++) {
        if (parseFloat(data[x].stokBarang) >= 1 && data[x].kategori === 'barang') {
          this.listItem.push(data[x]);
        } else if (parseFloat(data[x].stokBarang) >= 1 && data[x].kategori === 'kendaraan') {
          this.listKendaraan.push(data[x]);
        } else if (parseFloat(data[x].stokBarang) >= 1 && data[x].kategori === 'perumahan') {
          this.listPerumahan.push(data[x]);
        }
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Barang!', 'Ooops!');
    });
  }

  rounding(nominal) {
    return Math.round(nominal / 100) * 100;
  }

  getSimulation(lamaAngsuran: number) {
    this.listSimulasi = [];
    if (this.formBuilder.get('nominalPengajuan').value !== '' && this.formBuilder.get('lamaAngsuran').value !== '') {
      const nominalPengajuan = Number(this.formBuilder.get('nominalPengajuan').value.toString().replace(new RegExp(',', 'g'), ''));
      const tipePengajuan = this.formBuilder.get('kodeTipePengajuan').value;
      let currentYear = new Date().getFullYear();
      let currentMonth = new Date().getMonth() + 1;
      const totalSimpanan = parseFloat(this.saldoSimpanan.totalSimpanan.toString());
      if (nominalPengajuan < totalSimpanan) {
        this.persentaseBunga = this.bungaPinjaman2;
        console.log('BUNGA_PINJAMAN2:' + this.bungaPinjaman2);
      } else if (nominalPengajuan > totalSimpanan) {
        if (tipePengajuan === 'UANG' || this.tipePengajuan === 'BRG') {
          this.persentaseBunga = this.bungaPinjaman;
          console.log('BUNGA_PINJAMAN:' + this.bungaPinjaman);
        } else if (tipePengajuan === 'KENDARAAN') {
          this.persentaseBunga = this.bungaKendaraan;
          console.log('BUNGA_KENDARAAN:' + this.bungaKendaraan);
        } else if (tipePengajuan === 'PERUM') {
          this.persentaseBunga = this.bungaPerumahan;
          console.log('BUNGA_Perumahan:' + this.bungaPerumahan);
        }
      }
      // console.log('SIMPANAN: ' + totalSimpanan);
      // console.log('BUNGAN: ' + this.persentaseBunga);
      for (let x = 0; x < lamaAngsuran; x++) {
        const angsuranPokok = this.rounding(parseFloat(nominalPengajuan.toString()) / parseFloat(lamaAngsuran.toString()));
        const biayaBunga = this.rounding((parseFloat(nominalPengajuan.toString()) * (this.persentaseBunga / 100)));
        const jumlahTagihan = angsuranPokok + biayaBunga + this.biayaAdmin;
        if (currentMonth < 12) {
          currentMonth += 1;
        } else {
          currentMonth = 1;
          currentYear += 1;
        }
        const dataSimulasi = {
          tanggalJatuhTempo: new Date(currentYear + '-' + currentMonth + '-15'),
          angsuranPokok: angsuranPokok,
          biayaBunga: biayaBunga,
          biayaAdmin: this.biayaAdmin,
          jumlahTagihan:  jumlahTagihan,
          angsuranPokokAwal: angsuranPokok,
          biayaBungaAwal: biayaBunga,
          jumlahTagihanAwal:  jumlahTagihan
        };
        this.listSimulasi.push(dataSimulasi);
        // console.log(this.listSimulasi);
      }
      this.getTotalSimulation();
    }
  }

  doSubmitPengajuan() {
    if (this.dataUser.role === 'AGT') {
      this.doSubmitPengajuanAnggota();
    } else {
      this.doSubmitPengajuanAdmin();
    }
    if (this.fileUploadDokumen !== undefined) {
      const formDataDokumen: FormData = new FormData();
      formDataDokumen.append('file', this.fileUploadDokumen);
      // formData.append("fotoKtp", this.uploadedFileKtp);
      // formData.append("param", param);
      this.service.doPostUploadFile(formDataDokumen, 'pengajuan', this.dataUser.nomorAnggota).subscribe((res) => {
        this.loading = false;
      }, (err) => {
        this.loading = false;
        this.toastr.error('Terjadi kesalahan dalam upload Dokumen!', 'Ooops!');
      });
    }
  }

  doSubmitPengajuanAnggota() {
    this.loading = true;
    const lamaAngsuran = !this.selectedLamaAngsuran ? '' : this.selectedLamaAngsuran.split(' ');
    const param: any = {
      kodeUser: this.dataUser.kodeAnggota,
      namaUser: this.dataUser.nama,
      tanggalUpdate: new Date(),
      // kodePengajuan: '',
      kodeTipePengajuan: this.formBuilder.get('kodeTipePengajuan').value,
      tipePengajuan: this.formBuilder.get('tipePengajuan').value,
      nomorKtp: this.dataUser.nomorKtp,
      nomorAnggota: this.dataUser.nomorAnggota,
      nama: this.dataUser.nama,
      nomorNik: this.dataUser.nomorNik,
      nomorHp: this.dataUser.nomorHp,
      kodePerusahaan: this.dataUser.kodePerusahaan,
      namaPerusahaan: this.dataUser.namaPerusahaan,
      alamatPerusahaan: this.dataUser.alamatPerusahaan,
      emailPerusahaan: this.dataUser.emailPerusahaan,
      tanggalPengajuan: new Date(),
      lamaAngsuran: this.formBuilder.get('lamaAngsuran').value,
      nominalPengajuan: Math.round(this.formBuilder.get('nominalPengajuan').value.toString().replace(new RegExp(',', 'g'), '')),
      nominalAngsuran: Math.round(this.listSimulasi[this.listSimulasi.length - 1].jumlahTagihan),
      persenBunga: this.persentaseBunga,
      nominalBunga: this.listSimulasi[0].biayaBunga,
      totalBunga: this.totalBunga,
      biayaAdmin: this.biayaAdmin,
      kodeBarang: this.formBuilder.get('kodeBarang').value,
      namaBarang: this.formBuilder.get('namaBarang').value,
      // ambilDariKas: this.formBuilder.get('ambilDariKas').value,
      ambilDariKas: '',
      ketKas: 'K',
      keterangan: this.formBuilder.get('keterangan').value,
      statusPengajuan: 'NEW',
      tanggalPerubahan: '',
      tanggalTempo: '15',
      tanggalJatuhTempo: this.listSimulasi[this.listSimulasi.length - 1].tanggalJatuhTempo,
      nomorPinjaman: '',
      kategoriPengajuan: 'PINJAMAN',
      lokasiPenempatan: this.dataUser.lokasiPenempatan
    };
    this.service.doPostPengajuan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.formBuilder.reset();
        this.toastr.success(this.successMessage, 'Success!');
        if (this.dataUser.role === 'AGT') {
          this.router.navigate(['/home/pengajuan/pinjaman/anggota']);
        } else {
          this.router.navigate(['/home/pengajuan/pinjaman']);
        }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  backClicked() {
    if (this.dataUser.role === 'AGT') {
      this.router.navigate(['/home/pengajuan/pinjaman/anggota']);
    } else {
      this.router.navigate(['/home/pengajuan/pinjaman']);
    }
  }

  doSubmitPengajuanAdmin() {
    this.loading = true;
    // const anggota = JSON.parse(this.selectedAnggota);
    const anggota = this.selectedAnggota;
    const param: any = {
      kodeUser: anggota.kodeAnggota,
      namaUser: anggota.nama,
      tanggalUpdate: new Date(),
      // kodePengajuan: '',
      kodeTipePengajuan: this.formBuilder.get('kodeTipePengajuan').value,
      tipePengajuan: this.formBuilder.get('tipePengajuan').value,
      nomorKtp: anggota.nomorKtp,
      nomorAnggota: anggota.nomorAnggota,
      nama: anggota.nama,
      nomorNik: anggota.nomorNik,
      nomorHp: anggota.nomorHp,
      kodePerusahaan: anggota.kodePerusahaan,
      namaPerusahaan: anggota.namaPerusahaan,
      alamatPerusahaan: anggota.alamatPerusahaan,
      emailPerusahaan: anggota.emailPerusahaan,
      tanggalPengajuan: new Date(),
      lamaAngsuran: this.formBuilder.get('lamaAngsuran').value,
      nominalPengajuan: Number(this.formBuilder.get('nominalPengajuan').value.toString().replace(new RegExp(',', 'g'), '')),
      nominalAngsuran: Number(this.listSimulasi[this.listSimulasi.length - 1].jumlahTagihan),
      persenBunga: this.persentaseBunga,
      nominalBunga: this.listSimulasi[0].biayaBunga,
      totalBunga: this.totalBunga,
      biayaAdmin: this.biayaAdmin,
      kodeBarang: this.formBuilder.get('kodeBarang').value,
      namaBarang: this.formBuilder.get('namaBarang').value,
      // ambilDariKas: this.formBuilder.get('ambilDariKas').value,
      ambilDariKas: '',
      ketKas: 'K',
      keterangan: this.formBuilder.get('keterangan').value,
      statusPengajuan: 'APP',
      tanggalPerubahan: '',
      tanggalTempo: '15',
      tanggalJatuhTempo: this.listSimulasi[this.listSimulasi.length - 1].tanggalJatuhTempo,
      nomorPinjaman: '',
      kategoriPengajuan: 'PINJAMAN',
      lokasiPenempatan: anggota.lokasiPenempatan
    };
    // console.log(this.selectedAnggota);
    this.service.doPostPengajuan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.formBuilder.reset();
        this.toastr.success(this.successMessage, 'Success!');
        if (this.dataUser.role === 'AGT') {
          this.router.navigate(['/home/pengajuan/pinjaman/anggota']);
        } else {
          this.router.navigate(['/home/pengajuan/pinjaman']);
        }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  getTotalSimulation() {
    this.totalAngsuran = 0;
    this.totalBunga = 0;
    this.totalBiayaAdmin = 0;
    this.totalTagihan = 0;
    for (let i = 0; i < this.listSimulasi.length; i++) {
      this.totalAngsuran += Math.round(parseFloat(this.listSimulasi[i].angsuranPokokAwal.toString()));
      this.totalBunga +=  Math.round(parseFloat(this.listSimulasi[i].biayaBungaAwal.toString()));
      this.totalBiayaAdmin += Math.round(parseFloat(this.listSimulasi[i].biayaAdmin.toString()));
      this.totalTagihan += Math.round(parseFloat(this.listSimulasi[i].jumlahTagihanAwal.toString()));
    }
  }

  clearListSimulation() {
    this.listSimulasi = [];
  }

  setTipengajuan(value) {
    this.tipePengajuan = this.formBuilder.get('kodeTipePengajuan').value;
    if (this.tipePengajuan === 'UANG') {
      this.formBuilder.get('tipePengajuan').setValue('Uang');
    } else if (this.tipePengajuan === 'BRG') {
      this.formBuilder.get('tipePengajuan').setValue('Barang');
    } else if (this.tipePengajuan === 'PERUM') {
      this.formBuilder.get('tipePengajuan').setValue('Perumahan');
    } else if (this.tipePengajuan === 'KENDARAAN') {
      this.formBuilder.get('tipePengajuan').setValue('Kendaraan');
    }
  }

  setHargaBarang() {
    const item = JSON.parse(this.selectedItem);
    this.formBuilder.get('kodeBarang').setValue(item.kodeBarang);
    this.formBuilder.get('namaBarang').setValue(item.namaBarang);
    this.formBuilder.get('nominalPengajuan').setValue(numeral(Number(item.harga)).format('0,0'));
    this.getSimulation(this.formBuilder.get('lamaAngsuran').value);
  }


  onMemberChanged(val: string) {
    const name = val.split(' - ');
    console.log(name[1]);
    if (this.listAnggota.length > 0) {
      const selectedMember = this.getSelectedMemberByNik(name[1]);
      this.selectedAnggota = selectedMember;
      if (this.selectedAnggota !== null) {
        this.doGetSaldoSimpanan(selectedMember);
      }
    }
    // console.log(this.selectedAnggota);
  }

  getSelectedMemberByNik(selectedNik: string) {
    return this.listAnggota.find(member => member.nomorNik === selectedNik);
  }

  onFileChangedDokumen(event){
    this.fileUploadDokumen = event.item(0);
  }
}
