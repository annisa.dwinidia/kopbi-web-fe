import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmbPengajuanComponent } from './tmb-pengajuan.component';

describe('TmbPengajuanComponent', () => {
  let component: TmbPengajuanComponent;
  let fixture: ComponentFixture<TmbPengajuanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmbPengajuanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmbPengajuanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
