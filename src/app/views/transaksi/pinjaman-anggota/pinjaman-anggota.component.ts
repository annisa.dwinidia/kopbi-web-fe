import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Pengajuan} from '../../../model/pengajuan';
import {AppService} from '../../../service/app.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe, Location} from '@angular/common';
import * as _ from 'lodash';
import {User} from "../../../model/user";
import {AuthService} from "../../../service/auth.service";
import {Pinjaman} from '../../../model/Pinjaman';
import {Perusahaan} from '../../../model/Perusahaan';
declare let $: any;
@Component({
  selector: 'app-pinjaman-anggota',
  templateUrl: './pinjaman-anggota.component.html',
  styleUrls: ['./pinjaman-anggota.component.scss'],
  providers: [DatePipe]
})
export class PinjamanAnggotaComponent implements OnInit {

  formBuilder: FormGroup;

  loading = false;

  sub: any;
  nik;
  name;
  company;
  kodeAnggota;
  dataUser: User;
  addModal;

  selectedPinjaman = new Pinjaman();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';

  listPinjaman:     Pinjaman[] = [];
  listPerusahaan: Perusahaan[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;
  tanggalMulai = '';
  tanggalAkhir = '';
  kodePerusahaan = '';
  selectedPerusahaan;
  currentMonth = new Date().getMonth() + 1;
  currentYear = new Date().getFullYear();
  statusPinjaman = '';

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private route: ActivatedRoute,
              private auth: AuthService, private location: Location) { }

  ngOnInit() {
    $('select').selectpicker();
    this.dataUser = JSON.parse(this.auth.getToken());
    // this.tanggalMulai = this.datePipe.transform(new Date(this.currentYear + '-' + this.currentMonth + '-01'), 'yyyy-MM-dd');
    // this.tanggalAkhir = this.datePipe.transform(new Date(this.currentYear, this.currentMonth, 0), 'yyyy-MM-dd');
    this.sub = this.route.queryParams.subscribe(params => {
      this.nik = !params['nik'] ? this.dataUser.nomorNik : params['nik'];
      this.name = !params['name'] ? this.dataUser.nama : params['name'];
      this.company = !params['company'] ? this.dataUser.kodePerusahaan : params['company'];
      this.kodeAnggota = !params['kodeAnggota'] ? this.dataUser.nomorAnggota : params['kodeAnggota'];
    });
    // if (this.dataUser.role === 'HRD') {
    //   this.statusPinjaman = 'NEW';
    //   $('select[name=status]').selectpicker('val', 'NEW');
    // } else if (this.dataUser.role === 'PENGAWAS') {
    //   this.statusPinjaman = 'APP';
    //   $('select[name=status]').selectpicker('val', 'APP');
    // } else if (this.dataUser.role === 'ADM') {
    //   this.statusPinjaman = 'VRF';
    //   $('select[name=status]').selectpicker('val', 'VRF');
    // }
    this.doGetListCompany();
    this.doGetListPinjaman();
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetListPinjaman() {
    this.loading = true;
    // const param: any = {
    //   tanggalMulai: this.tanggalMulai,
    //   tanggalAkhir: this.tanggalAkhir,
    //   // kodePerusahaan: '',
    //   // search: this.filterText,
    //   // statusPinjaman: '',
    //   tipePengajuan: '',
    //   statusPengajuan: '',
    //   mulai: ((this.currentPage - 1) * this.itemsPerPage) + 1,
    //   akhir: this.itemsPerPage
    // };
    const param: any = {
      tanggalMulai: this.tanggalMulai,
      tanggalAkhir: this.tanggalAkhir,
      // kodePerusahaan: this.company,
      // search: this.nik,
      statusPinjaman: this.statusPinjaman,
      mulai: ((this.currentPage - 1) * this.itemsPerPage) + 1,
      akhir: this.itemsPerPage,
      // nomorNik: this.nik
      // nik: this.nik,
      // nama: this.name,
      nomorAnggota: this.kodeAnggota
    };
    this.service.getListPinjaman(param).subscribe((res) => {
      if (res) {
        const datas = JSON.parse(res.data);
        this.totalItems = datas.totalRow;
        /*res.recordsTotal;*/
        const data = datas.data;
        this.listPinjaman = _.orderBy(data, ['tanggalPinjaman'], 'desc');
        this.loading = false;
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Pinjaman!', 'Ooops!');
    });
  }

  setSelectedPinjaman(data: Pinjaman) {
    this.selectedPinjaman = data;
  }

  submit() {
    this.loading = true;
    const param: any = this.selectedPinjaman;
    param.tanggalUpdate = new Date();
    param.statusPengajuan = 'CAN';
    this.service.doPostPengajuan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListPinjaman();
        // if (this.dataUser.role === 'AGT') {
        //     this.router.navigate(['/pinjaman/anggota']);
        // } else {
        //     this.router.navigate(['/pinjaman']);
        // }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  gotoListAngsuran(nomorPinjaman, nama, data) {
    this.router.navigate(['/home/pinjaman/angsuran'], { queryParams: { nomorPinjaman: nomorPinjaman, name: nama, data: JSON.stringify(data) }, skipLocationChange: false });
  }

  gotoNew(id: number) {
    // this.router.navigate(['/transaksi/pengajuan/tmb-pengajuan'], { queryParams: { id: id }, skipLocationChange: false });
    if (this.dataUser.role === 'AGT') {
      this.router.navigate(['/home/pinjaman/anggota/new']);
    } else {
      this.router.navigate(['/home/pinjaman/new']);
    }
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListPinjaman();
  }

  backClicked() {
    this.location.back();
   // this.router.navigate(['/pinjaman']);
  }
}
