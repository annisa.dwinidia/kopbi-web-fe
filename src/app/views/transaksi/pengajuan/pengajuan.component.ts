import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Pengajuan} from '../../../model/pengajuan';
import {AppService} from '../../../service/app.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import {User} from "../../../model/user";
import {AuthService} from "../../../service/auth.service";
import {BsDatepickerConfig} from 'ngx-bootstrap';
import {config, configKOPBI} from '../../../config/application-config';
import {Barang} from '../../../model/Barang';
import {Perusahaan} from '../../../model/Perusahaan';
import {Observable, timer} from 'rxjs';
import {Bank} from '../../../model/bank';

declare let $: any;

@Component({
  selector: 'app-pengajuan',
  templateUrl: './pengajuan.component.html',
  styleUrls: ['./pengajuan.component.scss'],
  providers: [DatePipe]
})
export class PengajuanComponent implements OnInit {

  formBuilder: FormGroup;
  dataUser: User;

  loading = false;

  addModal;

  selectedPengajuan = new Pengajuan();

  statusPengajuanTitle = '';
  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';


  configSelectItem = null;
  configSelectPerumahan = null;
  configSelectKendaraan = null;
  selectedItem;
  listPengajuan:     Pengajuan[] = [];
  listLamaAngsuran: any;
  listSimulasi: any[] = [];
  listItem: any[] = [];
  listKendaraan: any[] = [];
  listPerumahan: any[] = [];
  listPerusahaan: Perusahaan[] = [];
  listBank: Bank[] = [];

  totalAngsuran = 0;
  totalBunga = 0;
  totalBiayaAdmin = 0;
  totalTagihan = 0;
  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;
  tanggalMulai = '';
  tanggalAkhir = '';
  kodePerusahaan = '';
  selectedPerusahaan;
  statusPengajuan = '';
  currentMonth = new Date().getMonth() + 1;
  currentYear = new Date().getFullYear();

  isDesc = true;
  column: string;
  direction: number;

  persentaseBunga = 0;
  bungaPinjaman = 0;
  bungaPinjaman2 = 0;
  biayaAdmin = 0;
  bungaKendaraan = 0;
  bungaPerumahan = 0;

  saldoSimpanan;

  public dpConfig: Partial<BsDatepickerConfig> = new BsDatepickerConfig();

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private auth: AuthService) {
    this.dpConfig.containerClass = 'theme-green';
    this.dpConfig.showWeekNumbers = false;
  }

  ngOnInit() {
    $('select').selectpicker();
    this.dataUser = JSON.parse(this.auth.getToken());
    this.formBuilder = this.fb.group({
      lamaAngsuran: '',
      nominalPengajuan: '',
      totalAngsuranPerBulan: '',
      kodeBarang: '',
      namaBarang: '',
      ambilDariKas: '',
      catatan: ''
    });
    // this.tanggalMulai = this.datePipe.transform(new Date(this.currentYear + '-' + this.currentMonth + '-01'), 'yyyy-MM-dd');
    // this.tanggalAkhir = this.datePipe.transform(new Date(this.currentYear, this.currentMonth, 0), 'yyyy-MM-dd');
    this.listLamaAngsuran = Array(24).fill([], 0, 24).map((x, i) => i + 1);
    this.doGetListItem();
    this.configSelectItem = {
      displayKey: 'namaBarang',
      search: true,
      height: '200px',
      placeholder: 'Pilih Barang',
      customComparator: () => {},
      limitTo: this.listItem.length,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Barang..',
      // searchOnKey: 'departmentCode'
    };
    this.configSelectKendaraan = {
      displayKey: 'namaBarang',
      search: true,
      height: '200px',
      placeholder: 'Pilih Kendaraan',
      customComparator: () => {},
      limitTo: this.listKendaraan.length,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Kendaraan..',
      // searchOnKey: 'departmentCode'
    };
    this.configSelectPerumahan = {
      displayKey: 'namaBarang',
      search: true,
      height: '200px',
      placeholder: 'Pilih Perumahan',
      customComparator: () => {},
      limitTo: this.listPerumahan.length,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Perumahan..',
      // searchOnKey: 'departmentCode'
    };
    if (this.dataUser.role === 'HRD') {
      this.statusPengajuan = 'NEW';
      $('select[name=status]').selectpicker('val', 'NEW');
    } else if (this.dataUser.role === 'PENGAWAS') {
      this.statusPengajuan = 'APP';
      $('select[name=status]').selectpicker('val', 'APP');
    } else if (this.dataUser.role === 'ADM') {
      this.statusPengajuan = 'VRF';
      $('select[name=status]').selectpicker('val', 'VRF');
    }
    this.doGetListPengajuan();
    this.doGetListCompany();
    this.doGetListBank();
    this.doGetListPersentase();
  }

  doGetListPersentase() {
    this.loading = true;
    this.service.getListPersentase().subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      for (let x = 0; x < datas.length; x++) {
        if (datas[x].kode === 'BUNGA_PINJAMAN') {
          this.bungaPinjaman = parseFloat(datas[x].nominal);
        } else if (datas[x].kode === 'BUNGA_PINJAMAN2') {
          this.bungaPinjaman2 = parseFloat(datas[x].nominal);
        } else if (datas[x].kode === 'BIAYA_ADMIN') {
          this.biayaAdmin = parseFloat(datas[x].nominal);
        } else if (datas[x].kode === 'BUNGA_KENDARAAN') {
          this.bungaKendaraan = parseFloat(datas[x].nominal);
        } else if (datas[x].kode === 'BUNGA_PERUMAHAN') {
          this.bungaPerumahan = parseFloat(datas[x].nominal);
        }
      }
      // console.log('BUNGA PINJAMAN1: ' + this.persentaseBunga);
      // console.log('BUNGA PINJAMAN2: ' + this.persentaseBunga2);
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Komponen!', 'Ooops!');
    });
  }

  doGetListBank() {
    this.loading = true;
    const param = {
      search: ''
    };
    this.service.getListBank(param).subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.listBank = datas;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Bank!', 'Ooops!');
    });
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(datas, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetListItem() {
    this.listItem = [];
    this.listPerumahan = [];
    this.listKendaraan = [];
    this.loading = true;
    const param = {
      search: ''
    };
    this.service.getListBarang(param).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      for (let x = 0; x < data.length; x++) {
        if (parseFloat(data[x].stokBarang) >= 1 && data[x].kategori === 'barang') {
          const datax = {
            kodeBarang: data[x].kodeBarang,
            namaBarang: data[x].namaBarang
          };
          this.listItem.push(datax);
        } else if (parseFloat(data[x].stokBarang) >= 1 && data[x].kategori === 'perumahan') {
          const datax = {
            kodeBarang: data[x].kodeBarang,
            namaBarang: data[x].namaBarang
          };
          this.listPerumahan.push(datax);
        } else if (parseFloat(data[x].stokBarang) >= 1 && data[x].kategori === 'kendaraan') {
          const datax = {
            kodeBarang: data[x].kodeBarang,
            namaBarang: data[x].namaBarang
          };
          this.listKendaraan.push(datax);
        }
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Barang!', 'Ooops!');
    });
  }

  doGetListPengajuan() {
    this.loading = true;
    let selectedPerusahaan;
    if (this.dataUser.role === 'HRD') {
      selectedPerusahaan = this.dataUser.kodePerusahaan;
    } else {
      selectedPerusahaan = !this.selectedPerusahaan ? '' : JSON.parse(this.selectedPerusahaan).kodePerusahaan;
    }
    const param: any = {
      mulai: ((this.currentPage - 1) * this.itemsPerPage) + 1,
      akhir: this.itemsPerPage,
      tanggalMulai: this.tanggalMulai,
      tanggalAkhir: this.tanggalAkhir,
      kodePerusahaan: selectedPerusahaan,
      // search: this.filterText,
      statusPengajuan: this.statusPengajuan,
      nomorNik: '',
      nama: '',
      kategoriPengajuan: 'PINJAMAN'
    };
    this.service.getListPengajuan(param).subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.totalItems = datas.totalRow; /*res.recordsTotal;*/
      // this.listPengajuan = _.sortBy(data, ['id']);
      this.listPengajuan =   _.orderBy(datas.data, function(dateObj) {
        return new Date(dateObj.tanggalPengajuan);
      }, 'desc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Pengajuan!', 'Ooops!');
    });
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  gotoNew(id: number) {
    // this.router.navigate(['/transaksi/pengajuan/tmb-pengajuan'], { queryParams: { id: id }, skipLocationChange: false });
    this.router.navigate(['/home/pengajuan/pinjaman/new']);
  }

  gotoList(nik: number, name: string, kodePerusahaan: string, kodeAnggota) {
    this.router.navigate(['/home/pengajuan/pinjaman/anggota'], { queryParams: { nik: nik, name: name, company: kodePerusahaan, kodeAnggota: kodeAnggota }, skipLocationChange: false });
  }

  gotoPinjaman(nik: number, name: string, kodePerusahaan: string, kodeAnggota: string) {
    this.router.navigate([]).then(result => {  window.open('#/home/pinjaman/anggota?kodeAnggota=' + kodeAnggota + '&name=' + name, '_blank'); });
    // this.router.navigate(['/pinjaman/anggota'], { queryParams: { nik: nik, name: name }, skipLocationChange: false });
  }

  gotoImage(nomorPengajuan: string) {
    this.router.navigate([]).then(result => {  window.open(config.KOPBI_IMAGES_BASE_URL + '/pengajuan/' + nomorPengajuan + '.jpg', '_blank'); });
    // this.router.navigate(['/pinjaman/anggota'], { queryParams: { nik: nik, name: name }, skipLocationChange: false });
  }

  gotoSimpanan(nik: number, name: string, kodePerusahaan: string, kodeAnggota: string) {
    this.router.navigate([]).then(result => {  window.open('#/home/simpanan/anggota?kodeAnggota=' + kodeAnggota + '&name=' + name + '&company=' + kodePerusahaan, '_blank'); });
    // this.router.navigate(['/simpanan/anggota'], { queryParams: { nik: nik, name: name }, skipLocationChange: false });
  }

  doGetSaldoSimpanan(nik) {
    this.loading = true;
    this.saldoSimpanan = null;
    this.service.getSaldoSimpanan(nik, {}).subscribe((res) => {
      this.loading = false;
      if (res.data) {
        const datas = JSON.parse(res.data);
        this.saldoSimpanan = datas;
      }
      this.getSimulation(this.formBuilder.get('lamaAngsuran').value);
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Saldo Simpanan!', 'Ooops!');
    });
  }

  setSelectedPengajuan(data: any, status, title) {
    this.selectedPengajuan = data;
    this.statusPengajuanTitle = status;
    this.modalTitle = title;
    this.formBuilder.get('catatan').reset();
    this.doGetSaldoSimpanan(data.nomorAnggota);
    this.setAdminConfirmation();
  }

  setAdminConfirmation() {
    const arr = [];
    const item = {
      kodeBarang: this.selectedPengajuan.kodeBarang,
      namaBarang: this.selectedPengajuan.namaBarang
    };
    arr.push(item);
    this.selectedItem = arr;
    console.log(this.selectedItem);
    $('select[name=lamaAngsuran]').val(this.selectedPengajuan.lamaAngsuran);
    $('.selectpicker').selectpicker('refresh');
    this.formBuilder.get('kodeBarang').setValue(this.selectedPengajuan.kodeBarang);
    this.formBuilder.get('namaBarang').setValue(this.selectedPengajuan.namaBarang);
    this.formBuilder.get('nominalPengajuan').setValue(numeral(Number(this.selectedPengajuan.nominalPengajuan)).format('0,0'));
    this.formBuilder.get('lamaAngsuran').setValue(this.selectedPengajuan.lamaAngsuran);
    this.formBuilder.get('totalAngsuranPerBulan').setValue(numeral(Number(this.selectedPengajuan.nominalAngsuran)).format('0,0'));
  }

  rounding(nominal) {
    return Math.round(nominal / 100) * 100;
  }

  getSimulation(lamaAngsuran: number) {
    if (this.formBuilder.get('nominalPengajuan').value !== '' && this.formBuilder.get('lamaAngsuran').value !== '') {
      const nominalPengajuan = Number(this.formBuilder.get('nominalPengajuan').value.toString().replace(new RegExp(',', 'g'), ''));
      const tipePengajuan = this.selectedPengajuan.kodeTipePengajuan;
      console.log(tipePengajuan);
      let currentYear = new Date().getFullYear();
      let currentMonth = new Date().getMonth() + 1;
      const totalSimpanan = parseFloat(this.saldoSimpanan.totalSimpanan.toString());
      if (nominalPengajuan < totalSimpanan) {
        this.persentaseBunga = this.bungaPinjaman2;
        console.log('BUNGA_PINJAMAN2:' + this.bungaPinjaman2);
      } else if (nominalPengajuan > totalSimpanan) {
        // this.persentaseBunga = this.bungaPinjaman;
        if (tipePengajuan === 'UANG' || tipePengajuan === 'BRG') {
          this.persentaseBunga = this.bungaPinjaman;
          console.log('BUNGA_PINJAMAN:' + this.bungaPinjaman);
        } else if (tipePengajuan === 'KENDARAAN') {
          this.persentaseBunga = this.bungaKendaraan;
          console.log('BUNGA_KENDARAAN:' + this.bungaKendaraan);
        } else if (tipePengajuan === 'PERUM') {
          this.persentaseBunga = this.bungaPerumahan;
          console.log('BUNGA_Perumahan:' + this.bungaPerumahan);
        }
      }
      // console.log('PENGAJUAN:SIMPANAN ==>' + nominalPengajuan + ' : ' + totalSimpanan);
      // console.log('BUNGA: ' + persentaseBunga);
      for (let x = 0; x < lamaAngsuran; x++) {
        const angsuranPokok = this.rounding(parseFloat(nominalPengajuan.toString()) / parseFloat(lamaAngsuran.toString()));
        const biayaBunga = this.rounding((parseFloat(nominalPengajuan.toString()) * (this.persentaseBunga / 100)));
        const jumlahTagihan = angsuranPokok + biayaBunga + this.biayaAdmin;
        if (currentMonth < 12) {
          currentMonth += 1;
        } else {
          currentMonth = 1;
          currentYear += 1;
        }
        const dataSimulasi = {
          tanggalJatuhTempo: new Date(currentYear + '-' + currentMonth + '-15'),
          angsuranPokok: angsuranPokok,
          biayaBunga: biayaBunga,
          biayaAdmin: this.biayaAdmin,
          jumlahTagihan:  jumlahTagihan,
          angsuranPokokAwal: angsuranPokok,
          biayaBungaAwal: biayaBunga,
          jumlahTagihanAwal:  jumlahTagihan
        };
        this.listSimulasi.push(dataSimulasi);
        // console.log(this.listSimulasi);
      }
      this.getTotalSimulation();
      this.formBuilder.get('totalAngsuranPerBulan').setValue(numeral(Number(Math.round(this.listSimulasi[this.listSimulasi.length - 1].jumlahTagihan))).format('0,0'));
    }
  }

  getTotalSimulation() {
    this.totalAngsuran = 0;
    this.totalBunga = 0;
    this.totalBiayaAdmin = 0;
    this.totalTagihan = 0;
    for (let i = 0; i < this.listSimulasi.length; i++) {
      this.totalAngsuran += Math.round(parseFloat(this.listSimulasi[i].angsuranPokokAwal.toString()));
      this.totalBunga +=  Math.round(parseFloat(this.listSimulasi[i].biayaBungaAwal.toString()));
      this.totalBiayaAdmin += Math.round(parseFloat(this.listSimulasi[i].biayaAdmin.toString()));
      this.totalTagihan += Math.round(parseFloat(this.listSimulasi[i].jumlahTagihanAwal.toString()));
    }
  }

  clearListSimulation() {
    this.listSimulasi = [];
  }

  submit() {
    this.loading = true;
    if (this.modalTitle !== 'Proses') {
      const param: any = this.selectedPengajuan;
      param.tanggalUpdate = new Date();
      param.lamaAngsuran = this.formBuilder.get('lamaAngsuran').value;
      param.nominalPengajuan = Math.round(this.formBuilder.get('nominalPengajuan').value.toString().replace(new RegExp(',', 'g'), ''));
      param.nominalAngsuran = Math.round(this.listSimulasi[this.listSimulasi.length - 1].jumlahTagihan);
      param.persenBunga = this.persentaseBunga;
      param.nominalBunga = this.listSimulasi[0].biayaBunga;
      param.totalBunga = this.totalBunga;
      param.kodeBarang = this.formBuilder.get('kodeBarang').value;
      param.namaBarang = this.formBuilder.get('namaBarang').value;
      param.tanggalJatuhTempo = this.listSimulasi[this.listSimulasi.length - 1].tanggalJatuhTempo;
      param.tanggalUpdate = new Date();
      if (this.statusPengajuanTitle === 'Approve') {
        param.statusPengajuan = 'APP';
        param.tanggalAppHRD = new Date();
        param.namaUserHRD = this.dataUser.nama;
        param.catatanHRD = this.formBuilder.get('catatan').value;
      } else if (this.statusPengajuanTitle === 'Tolak ADM') {
        param.statusPengajuan = 'CAN';
      } else if (this.statusPengajuanTitle === 'Tolak PENGAWAS') {
        param.statusPengajuan = 'CAN-PENGAWAS';
        param.tanggalAppPengawas = new Date();
        param.namaUserPengawas = this.dataUser.nama;
        param.catatanPengawas = this.formBuilder.get('catatan').value;
      } else if (this.statusPengajuanTitle === 'Tolak HRD') {
        param.statusPengajuan = 'CAN-HRD';
        param.tanggalAppHRD = new Date();
        param.namaUserHRD = this.dataUser.nama;
        param.catatanHRD = this.formBuilder.get('catatan').value;
      } else if (this.statusPengajuanTitle === 'Proses') {
        param.statusPengajuan = 'PROC';
      } else if (this.statusPengajuanTitle === 'Verifikasi') {
        param.statusPengajuan = 'VRF';
        param.tanggalAppPengawas = new Date();
        param.namaUserPengawas = this.dataUser.nama;
        param.catatanPengawas = this.formBuilder.get('catatan').value;
      }
      console.log(param);
      this.service.doPostPengajuan(param).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success(this.successMessage, 'Success!');
          this.doGetListPengajuan();
          // if (this.dataUser.role === 'AGT') {
          //     this.router.navigate(['/pinjaman/anggota']);
          // } else {
          //     this.router.navigate(['/pinjaman']);
          // }
        } else {
          this.doGetListPengajuan();
          this.toastr.error(this.errorMesssage, 'Ooops!');
        }
      }, (err) => {
        this.loading = false;
        this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
      });
    }
    if (this.modalTitle === 'Proses') {
      // UPDATE DATA PENGAJUAN
      const param: any = this.selectedPengajuan;
      param.tanggalUpdate = new Date();
      param.lamaAngsuran = this.formBuilder.get('lamaAngsuran').value;
      param.nominalPengajuan = Math.round(this.formBuilder.get('nominalPengajuan').value.toString().replace(new RegExp(',', 'g'), ''));
      param.nominalAngsuran = Math.round(this.listSimulasi[this.listSimulasi.length - 1].jumlahTagihan);
      param.persenBunga = this.persentaseBunga;
      param.nominalBunga = this.listSimulasi[0].biayaBunga;
      param.totalBunga = this.totalBunga;
      param.kodeBarang = this.formBuilder.get('kodeBarang').value;
      param.namaBarang = this.formBuilder.get('namaBarang').value;
      param.tanggalJatuhTempo = this.listSimulasi[this.listSimulasi.length - 1].tanggalJatuhTempo;
      param.statusPengajuan = 'PROC';
      param.ambilDariKas = this.formBuilder.get('ambilDariKas').value;
      param.ketKas = 'K';
      console.log('PENGAJUAN: ' + param);
      this.service.doPostPengajuan(param).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success(this.successMessage, 'Success!');
          this.doGetListPengajuan();
          // if (this.dataUser.role === 'AGT') {
          //     this.router.navigate(['/pinjaman/anggota']);
          // } else {
          //     this.router.navigate(['/pinjaman']);
          // }
        } else {
          this.doGetListPengajuan();
          this.toastr.error(this.errorMesssage, 'Ooops!');
        }
      }, (err) => {
        this.loading = false;
        this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
      });

      // POST PINJAMAN
      param.statusPinjaman = 'BELUM';
      console.log('PINJAMAN: ' + param);
      this.service.doPostPinjaman(param).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success(this.successMessage, 'Success!');
          this.doGetListPengajuan();
          // if (this.dataUser.role === 'AGT') {
          //     this.router.navigate(['/pinjaman/anggota']);
          // } else {
          //     this.router.navigate(['/pinjaman']);
          // }
        } else {
          this.toastr.error(this.errorMesssage, 'Ooops!');
        }
      }, (err) => {
        this.loading = false;
        this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
      });
    }
  }

  setKeteranganUntuk(val) {
    const bank = JSON.parse(val);
    this.formBuilder.get('ambilDariKas').setValue(bank.kodeBank);
  }

  setHargaBarang() {
    console.log(this.selectedItem);
    this.formBuilder.get('kodeBarang').setValue(this.selectedItem[0].kodeBarang);
    this.formBuilder.get('namaBarang').setValue(this.selectedItem[0].namaBarang);
    this.formBuilder.get('nominalPengajuan').setValue(numeral(Number(this.selectedItem[0].harga)).format('0,0'));
    this.getSimulation(this.formBuilder.get('lamaAngsuran').value);
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListPengajuan();
  }
}
