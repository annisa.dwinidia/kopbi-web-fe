import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpananComponent } from './simpanan.component';

describe('SimpananComponent', () => {
  let component: SimpananComponent;
  let fixture: ComponentFixture<SimpananComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpananComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpananComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
