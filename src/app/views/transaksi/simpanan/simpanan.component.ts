import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Pengajuan} from '../../../model/pengajuan';
import {AppService} from '../../../service/app.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import {User} from '../../../model/user';
import {AuthService} from '../../../service/auth.service';
import {Simpanan} from '../../../model/Simpanan';
import {Perusahaan} from '../../../model/Perusahaan';
import {Bank} from '../../../model/bank';
declare let $: any;
@Component({
  selector: 'app-simpanan',
  templateUrl: './simpanan.component.html',
  styleUrls: ['./simpanan.component.scss'],
  providers: [DatePipe]
})
export class SimpananComponent implements OnInit {

  formBuilder: FormGroup;
  dataUser: User;

  loading = false;

  addModal;

  selectedSimpanan = new Simpanan();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';

  listSimpanan:     Simpanan[] = [];
  listPerusahaan:     Perusahaan[] = [];
  listBank: Bank[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;
  tanggalMulai = '';
  tanggalAkhir = '';
  kodePerusahaan = '';
  selectedPerusahaan;
  jenisIuran = '';
  kodeSimpanan = '';
  currentMonth = new Date().getMonth() + 1;
  currentYear = new Date().getFullYear();

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    $('select').selectpicker();
    this.tanggalMulai = this.datePipe.transform(new Date(this.currentYear + '-' + this.currentMonth + '-01'), 'yyyy-MM-dd');
    this.tanggalAkhir = this.datePipe.transform(new Date(this.currentYear, this.currentMonth, 0), 'yyyy-MM-dd');
    this.formBuilder = this.fb.group({
      namaSimpanan: [''],
      kodeSimpanan: [''],
      nominalSimpanan: [''],
      jenisIuran: [''],
      ketIuran: [''],
      kodeMediaIuran: [''],
      mediaIuran: [''],
      bulan: [''],
      tahun: [''],
      keterangan: [''],
    });
    this.formBuilder.get('kodeSimpanan').valueChanges.subscribe(data => {
      $('select').selectpicker();
    });
    this.doGetListSimpanan();
    this.doGetListCompany();
    this.doGetListBank();
  }

  doGetListBank() {
    this.loading = true;
    const param = {
      search: ''
    };
    this.service.getListBank(param).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listBank = data;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Bank!', 'Ooops!');
    });
  }

  doOpenFormEdit() {
    console.log(this.selectedSimpanan);
    this.formBuilder.get('namaSimpanan').setValue(this.selectedSimpanan.namaSimpanan);
    this.formBuilder.get('kodeSimpanan').setValue(this.selectedSimpanan.kodeSimpanan);
    this.formBuilder.get('nominalSimpanan').setValue(numeral(Number(this.selectedSimpanan.nominalSimpanan)).format('0,0'));
    this.formBuilder.get('jenisIuran').setValue(this.selectedSimpanan.jenisIuran);
    this.formBuilder.get('ketIuran').setValue(this.selectedSimpanan.ketIuran);
    this.formBuilder.get('kodeMediaIuran').setValue(this.selectedSimpanan.kodeMediaIuran);
    this.formBuilder.get('mediaIuran').setValue(this.selectedSimpanan.mediaIuran);
    this.formBuilder.get('bulan').setValue(this.selectedSimpanan.bulan);
    this.formBuilder.get('tahun').setValue(this.selectedSimpanan.tahun);
    this.formBuilder.get('keterangan').setValue(this.selectedSimpanan.keterangan);
    $('select[name=kodeSimpanan]').val(this.selectedSimpanan.kodeSimpanan);
    $('select[name=bulan]').val(this.selectedSimpanan.bulan);
    $('select[name=tahun]').val(this.selectedSimpanan.tahun);
    $('select[name=kodeMediaIuran]').val(this.selectedSimpanan.kodeMediaIuran);
    $('select[name=jenisIuran]').val(this.selectedSimpanan.jenisIuran);
    $('.selectpicker').selectpicker('refresh');
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetListSimpanan() {
    this.loading = true;
    this.dataUser = JSON.parse(this.auth.getToken());
    const param: any = {
      tanggalMulai: this.tanggalMulai,
      tanggalAkhir: this.tanggalAkhir,
      // kodePerusahaan: !this.selectedPerusahaan ? '' : JSON.parse(this.selectedPerusahaan).kodePerusahaan,
      // search: this.filterText,
      kodeSimpanan: this.kodeSimpanan,
      jenisIuran: this.jenisIuran,
      mulai: ((this.currentPage - 1) * this.itemsPerPage) + 1,
      akhir: this.itemsPerPage,
      // nomorNik: '',
      nomorAnggota: ''
    };
    this.service.getListSimpanan(param).subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.totalItems = datas.totalRow;
      const data = datas.data;
      this.listSimpanan =   _.orderBy(data, function(dateObj) {
        return new Date(dateObj.tanggalSimpanan);
      }, 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Simpanan!', 'Ooops!');
    });
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  gotoNew(id: number) {
    // this.router.navigate(['/transaksi/pengajuan/tmb-pengajuan'], { queryParams: { id: id }, skipLocationChange: false });
    this.router.navigate(['/home/simpanan/new']);
  }

  gotoList(nik: number, name: string, kodePerusahaan: string, kodeAnggota: string) {
    this.router.navigate(['/home/simpanan/anggota'], { queryParams: { kodeAnggota: kodeAnggota, name: name, company: kodePerusahaan }, skipLocationChange: false });
  }

  setSelectedSimpanan(data: Simpanan, title) {
    this.selectedSimpanan = data;
    this.modalTitle = title;
  }

  submit() {
    const param: any = this.selectedSimpanan;
    param.tanggalUpdate = new Date();
    param.namaUser = this.dataUser.nama;
    this.service.doPostPengajuan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListSimpanan();
        // if (this.dataUser.role === 'AGT') {
        //     this.router.navigate(['/pinjaman/anggota']);
        // } else {
        //     this.router.navigate(['/pinjaman']);
        // }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });

  }

  updateSimpanan() {
    const param: Simpanan = this.selectedSimpanan;
    param.kodeSimpanan = this.formBuilder.get('kodeSimpanan').value;
    param.namaSimpanan = this.formBuilder.get('namaSimpanan').value;
    param.nominalSimpanan = Number(this.formBuilder.get('nominalSimpanan').value.toString().replace(new RegExp(',', 'g'), ''));
    param.jenisIuran = this.formBuilder.get('jenisIuran').value;
    param.ketIuran = this.formBuilder.get('ketIuran').value;
    param.kodeMediaIuran = this.formBuilder.get('kodeMediaIuran').value;
    param.mediaIuran = this.formBuilder.get('mediaIuran').value;
    param.bulan = this.formBuilder.get('bulan').value;
    param.tahun = this.formBuilder.get('tahun').value;
    param.keterangan = this.formBuilder.get('keterangan').value;
    param.tanggalUpdate = new Date();
    param.namaUser = this.dataUser.nama;
    // console.log(param);
    this.service.doPostSimpanan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        if (this.dataUser.role === 'AGT') {
          this.router.navigate(['/home/simpanan/anggota']);
        } else {
          this.router.navigate(['/home/simpanan']);
        }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  delete() {
    const param = this.selectedSimpanan;
    param.statusAktif = 'TIDAK AKTIF';
    this.service.doPostSimpanan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListSimpanan();
        // if (this.dataUser.role === 'AGT') {
        //     this.router.navigate(['/pinjaman/anggota']);
        // } else {
        //     this.router.navigate(['/pinjaman']);
        // }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  setNamaSimpanan() {
    const value = this.formBuilder.get('kodeSimpanan').value;
    if (value === 'SPP') {
      this.formBuilder.get('namaSimpanan').setValue('Simpanan Pokok');
    } else if (value === 'SPW') {
      this.formBuilder.get('namaSimpanan').setValue('Simpanan Wajib');
    } else if (value === 'SPS') {
      this.formBuilder.get('namaSimpanan').setValue('Simpanan Sukarela');
    }
  }

  setKeterangan() {
    const value = this.formBuilder.get('jenisIuran').value;
    // this.formBuilder.get('keterangan').setValue(value);
    if (value === 'Setoran') {
      this.formBuilder.get('ketIuran').setValue('D');
    } else if (value === 'Penarikan') {
      this.formBuilder.get('ketIuran').setValue('K');
    }

    const kodeBank = this.formBuilder.get('kodeMediaIuran').value;
    const bank = this.listBank.find(x => x.kodeBank === kodeBank);
    this.formBuilder.get('mediaIuran').setValue(bank.namaBank);
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListSimpanan();
  }
}
