import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Pengajuan} from '../../../model/pengajuan';
import {AppService} from '../../../service/app.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';
import {User} from "../../../model/user";
import {AuthService} from "../../../service/auth.service";
import {Pinjaman} from '../../../model/Pinjaman';
import {Perusahaan} from '../../../model/Perusahaan';
declare let $: any;
@Component({
  selector: 'app-pinjaman',
  templateUrl: './pinjaman.component.html',
  styleUrls: ['./pinjaman.component.scss'],
  providers: [DatePipe]
})
export class PinjamanComponent implements OnInit {

  formBuilder: FormGroup;
  dataUser: User;

  loading = false;

  addModal;

  selectedPinjaman = new Pinjaman();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';

  listPinjaman:     Pinjaman[] = [];
  listPerusahaan:     Perusahaan[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;
  tanggalMulai = '';
  tanggalAkhir = '';
  kodePerusahaan = '';
  selectedPerusahaan;
  statusPinjaman = '';
  currentMonth = new Date().getMonth() + 1;
  currentYear = new Date().getFullYear();

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    $('select').selectpicker();
    this.dataUser = JSON.parse(this.auth.getToken());
    if (this.dataUser.role === 'PENGAWAS') {
      this.tanggalMulai = this.datePipe.transform(new Date(this.currentYear + '-' + this.currentMonth + '-01'), 'yyyy-MM-dd');
      this.tanggalAkhir = this.datePipe.transform(new Date(this.currentYear, this.currentMonth, 0), 'yyyy-MM-dd');
    }
    if (this.dataUser.role === 'ADM') {
      this.statusPinjaman = 'BELUM';
      $('select[name=status]').selectpicker('val', 'BELUM');
    }
    this.doGetListPinjaman();
    this.doGetListCompany();
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetListPinjaman() {
    this.loading = true;
    let kodePerusahaan;
    if (this.dataUser.role === 'HRD') {
      kodePerusahaan = this.dataUser.kodePerusahaan;
    } else {
      kodePerusahaan = !this.selectedPerusahaan ? '' : JSON.parse(this.selectedPerusahaan).kodePerusahaan;
    }
    const param: any = {
      tanggalMulai: this.tanggalMulai,
      tanggalAkhir: this.tanggalAkhir,
      kodePerusahaan: kodePerusahaan,
      search: this.filterText,
      statusPinjaman: this.statusPinjaman,
      mulai: ((this.currentPage - 1) * this.itemsPerPage) + 1,
      akhir: this.itemsPerPage//,
      // nomorNik: '',
      //nomorAnggota: ''
    };
    this.service.getListPinjaman(param).subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.totalItems = datas.totalRow;
      const data = datas.data;
      // const list = _.filter(data, ['statusPinjaman', 'Lunas']);
      this.listPinjaman =   _.orderBy(data, function(dateObj) {
        return new Date(dateObj.tanggalPinjaman);
      }, 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Pinjaman!', 'Ooops!');
    });
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  gotoNew(id: number) {
    // this.router.navigate(['/transaksi/pengajuan/tmb-pengajuan'], { queryParams: { id: id }, skipLocationChange: false });
    this.router.navigate(['/home/pinjaman/new']);
  }

  gotoList(nik: number, name: string, kodePerusahaan: string, kodeAnggota: string) {
    this.router.navigate(['/home/pinjaman/anggota'], { queryParams: { kodeAnggota: kodeAnggota, name: name}, skipLocationChange: false });
  }

  setSelectedPinjaman(data: Pinjaman, title) {
    this.selectedPinjaman = data;
    this.modalTitle = title;
  }

  submit() {
    this.loading = true;
    const param: any = this.selectedPinjaman;
    param.tanggalUpdate = new Date();
    this.service.doPostPinjaman(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListPinjaman();
        // if (this.dataUser.role === 'AGT') {
        //     this.router.navigate(['/pinjaman/anggota']);
        // } else {
        //     this.router.navigate(['/pinjaman']);
        // }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  gotoListAngsuran(nomorPinjaman, nama, data) {
    const dataSend = {
      tanggalPengajuan: data.tanggalPengajuan,
      tanggalPinjaman: data.tanggalPinjaman,
      keterangan: data.keterangan,
      tipePengajuan: data.tipePengajuan,
      nominalPinjaman: data.nominalPinjaman,
      lamaAngsuran: data.lamaAngsuran
    };
    this.router.navigate(['/home/pinjaman/angsuran'], { queryParams: { nomorPinjaman: nomorPinjaman, name: nama, data: JSON.stringify(data) }, skipLocationChange: false });
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListPinjaman();
  }
}
