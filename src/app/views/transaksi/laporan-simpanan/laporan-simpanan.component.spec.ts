import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanSimpananComponent } from './laporan-simpanan.component';

describe('LaporanSimpananComponent', () => {
  let component: LaporanSimpananComponent;
  let fixture: ComponentFixture<LaporanSimpananComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanSimpananComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanSimpananComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
