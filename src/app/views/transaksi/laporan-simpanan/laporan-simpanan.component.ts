import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Simpanan} from '../../../model/Simpanan';
import {AppService} from '../../../service/app.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';
import {User} from "../../../model/user";
import {AuthService} from "../../../service/auth.service";
import {Perusahaan} from '../../../model/Perusahaan';
declare let $: any;
@Component({
  selector: 'app-laporan-simpanan',
  templateUrl: './laporan-simpanan.component.html',
  styleUrls: ['./laporan-simpanan.component.scss'],
  providers: [DatePipe]
})
export class LaporanSimpananComponent implements OnInit {

  formBuilder: FormGroup;

  loading = false;

  sub: any;
  nik;
  name;
  company;
  dataUser: User;
  addModal;

  selectedSimpanan = new Simpanan();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';

  listSimpanan:     any[] = [];
  listPerusahaan: Perusahaan[] = [];
  listAnggota: any[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;
  tanggalMulai = '';
  tanggalAkhir = '';
  kodePerusahaan = '';
  selectedPerusahaan;
  selectedAnggota;
  selectedName = '';
  kodeSimpanan = '';
  jenisIuran = '';
  currentMonth = new Date().getMonth() + 1;
  currentYear = new Date().getFullYear();
  saldoSimpanan;

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private route: ActivatedRoute,
              private auth: AuthService) { }

  ngOnInit() {
    $('select').selectpicker();
    this.dataUser = JSON.parse(this.auth.getToken());
    this.sub = this.route.queryParams.subscribe(params => {
      this.nik = !params['nik'] ? this.dataUser.nomorNik : params['nik'];
      this.name = !params['name'] ? this.dataUser.nama : params['name'];
      this.company = !params['company'] ? this.dataUser.kodePerusahaan : params['company'];
    });
    this.tanggalMulai = this.datePipe.transform(new Date(this.currentYear + '-' + this.currentMonth + '-01'), 'yyyy-MM-dd');
    this.tanggalAkhir = this.datePipe.transform(new Date(this.currentYear, this.currentMonth, 0), 'yyyy-MM-dd');
    this.doGetListCompany();
    this.doGetListSimpananNew();
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetListMember() {
    const param = {
      kodePerusahaan: !this.selectedPerusahaan ? '' : JSON.parse(this.selectedPerusahaan).kodePerusahaan,
      search: this.selectedName,
      mulai: 1,
      akhir: 50,
      status: ''
    };
    this.service.getListAllAnggota(param).subscribe((res) => {
      const data = JSON.parse(res.data);
      this.listAnggota = _.sortBy(data.data, ['id']);
    }, (err) => {
      // this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Anggota!', 'Ooops!');
    });
  }

  onMemberChanged(val: string) {
    const name = val.split(' - ');
    console.log(name[1]);
    if (this.listAnggota.length > 0) {
      const selectedMember = this.getSelectedMemberByNik(name[1]);
      this.selectedAnggota = selectedMember;
    }
  }

  getSelectedMemberByNik(selectedNik: string) {
    return this.listAnggota.find(member => member.nomorNik === selectedNik);
  }

  clearAnggota() {
    this.selectedAnggota = null;
    this.saldoSimpanan = null;
    this.selectedName = '';
  }

  doGetListSimpanan() {
    if (this.selectedName === '') {
      this.doGetListSimpananNew();
    } else {
      this.doGetListSimpananOld();
    }
  }

  doGetListSimpananOld() {
    this.loading = true;
    this.saldoSimpanan = null;
    const nomorNik = this.selectedAnggota.nomorAnggota;
    this.service.getSaldoSimpanan(nomorNik, {}).subscribe((res) => {
      if (res.data) {
        const data = JSON.parse(res.data);
        this.saldoSimpanan = data;
        this.listSimpanan = [];
        const dataSimpanan = {
          nomorAnggota: this.selectedAnggota.nomorAnggota,
          nama: this.selectedAnggota.nama,
          namaPerusahaan: this.selectedAnggota.namaPerusahaan,
          lokasiPenempatan: this.selectedAnggota.lokasiPenempatan,
          statusAnggota: this.selectedAnggota.statusAnggota,
          totalSimpananPokok: this.saldoSimpanan.totalSimpananPokok,
          totalSimpananWajib: this.saldoSimpanan.totalSimpananWajib,
          totalSimpananSukarela: this.saldoSimpanan.totalSimpananSukarela,
          totalSimpanan: this.saldoSimpanan.totalSimpanan,
        };
        this.listSimpanan.push(dataSimpanan);
      }
      this.totalItems = 1;
      this.loading = false;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Saldo Simpanan!', 'Ooops!');
    });
  }

  doGetListSimpananNew() {
    this.loading = true;
    const param = {
      mulai: ((this.currentPage - 1) * this.itemsPerPage) + 1,
      akhir: this.itemsPerPage,
      kodePerusahaan: !this.selectedPerusahaan ? '' : JSON.parse(this.selectedPerusahaan).kodePerusahaan,
      search: ''
    };
    this.service.getLaporanSimpanan(param).subscribe((res) => {
      this.loading = false;
      if (res.data) {
        const datas = JSON.parse(res.data);
        this.totalItems = datas.totalRow;
        this.listSimpanan = datas.data;
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Saldo Simpanan!', 'Ooops!');
    });
  }

  setSelectedSimpanan(data: Simpanan) {
    this.selectedSimpanan = data;
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    if (this.selectedName === '') {
      this.doGetListSimpananNew();
    }
  }

  backClicked() {
    this.router.navigate(['/home/simpanan']);
  }
}
