import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpananAnggotaComponent } from './simpanan-anggota.component';

describe('SimpananAnggotaComponent', () => {
  let component: SimpananAnggotaComponent;
  let fixture: ComponentFixture<SimpananAnggotaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpananAnggotaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpananAnggotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
