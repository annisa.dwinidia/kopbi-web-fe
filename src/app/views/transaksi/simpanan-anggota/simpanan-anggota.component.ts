import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Simpanan} from '../../../model/Simpanan';
import {AppService} from '../../../service/app.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';
import {User} from "../../../model/user";
import {AuthService} from "../../../service/auth.service";
import {Perusahaan} from '../../../model/Perusahaan';
declare let $: any;
@Component({
  selector: 'app-simpanan-anggota',
  templateUrl: './simpanan-anggota.component.html',
  styleUrls: ['./simpanan-anggota.component.scss'],
  providers: [DatePipe]
})
export class SimpananAnggotaComponent implements OnInit {

  formBuilder: FormGroup;

  loading = false;

  sub: any;
  nik;
  name;
  company;
  dataUser: User;
  addModal;
  kodeAnggota;

  selectedSimpanan = new Simpanan();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';

  listSimpanan:     Simpanan[] = [];
  listPerusahaan: Perusahaan[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;
  tanggalMulai = '';
  tanggalAkhir = '';
  kodePerusahaan = '';
  selectedPerusahaan;
  kodeSimpanan = '';
  jenisIuran = '';
  currentMonth = new Date().getMonth() + 1;
  currentYear = new Date().getFullYear();
  saldoSimpanan;

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private route: ActivatedRoute,
              private auth: AuthService) { }

  ngOnInit() {
    $('select').selectpicker();
    this.dataUser = JSON.parse(this.auth.getToken());
    this.sub = this.route.queryParams.subscribe(params => {
      this.nik = !params['nik'] ? this.dataUser.nomorNik : params['nik'];
      this.name = !params['name'] ? this.dataUser.nama : params['name'];
      this.company = !params['company'] ? this.dataUser.kodePerusahaan : params['company'];
      this.kodeAnggota = !params['kodeAnggota'] ? this.dataUser.nomorAnggota : params['kodeAnggota'];
    });
    this.tanggalMulai = this.datePipe.transform(new Date(this.currentYear + '-' + this.currentMonth + '-01'), 'yyyy-MM-dd');
    this.tanggalAkhir = this.datePipe.transform(new Date(this.currentYear, this.currentMonth, 0), 'yyyy-MM-dd');
    this.doGetListCompany();
    this.doGetListSimpanan();
    this.doGetSaldoSimpanan();
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetSaldoSimpanan() {
    this.loading = true;
    this.saldoSimpanan = null;
    this.service.getSaldoSimpanan(this.kodeAnggota, {}).subscribe((res) => {
      this.loading = false;
      if (res.data) {
        const datas = JSON.parse(res.data);
        this.saldoSimpanan = datas;
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Saldo Simpanan!', 'Ooops!');
    });
  }

  doGetListSimpanan() {
    this.loading = true;
    // const param: any = {
    //   mulai: ((this.currentPage - 1) * this.itemsPerPage) + 1,
    //   akhir: this.itemsPerPage,
    //   tanggalMulai: this.tanggalMulai,
    //   tanggalAkhir: this.tanggalAkhir,
    //   // kodePerusahaan: !this.selectedPerusahaan ? '' : JSON.parse(this.selectedPerusahaan).kodePerusahaan,
    //   // search: this.filterText,
    //   // kodeSimpanan: this.kodeSimpanan,
    //   // jenisIuran: this.jenisIuran
    // };
    const param: any = {
      tanggalMulai: this.tanggalMulai,
      tanggalAkhir: this.tanggalAkhir,
      kodePerusahaan: !this.selectedPerusahaan ? '' : JSON.parse(this.selectedPerusahaan).kodePerusahaan,
      // search: this.nik,
      kodeSimpanan: this.kodeSimpanan,
      jenisIuran: this.jenisIuran,
      mulai: ((this.currentPage - 1) * this.itemsPerPage) + 1,
      akhir: this.itemsPerPage,
      // nomorNik: this.nik,
      nama: this.name,
      nomorAnggota: this.kodeAnggota
    };
    this.service.getListSimpanan(param).subscribe((res) => {
      if (res) {
        // console.log(res.length);
        const datas = JSON.parse(res.data);
        this.totalItems = datas.totalRow;
        const data = datas.data;
        this.listSimpanan = _.orderBy(data, ['tanggalSimpanan'], 'desc');
        // this.listSimpanan = data;
        this.loading = false;
      } else {
        this.listSimpanan = [];
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Simpanan!', 'Ooops!');
    });
  }

  setSelectedSimpanan(data: Simpanan) {
    this.selectedSimpanan = data;
  }

  submit() {
    const param: Simpanan = this.selectedSimpanan;
    param.tanggalUpdate = new Date();
    this.service.doPostSimpanan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListSimpanan();
        if (this.dataUser.role === 'AGT') {
          this.router.navigate(['/home/simpanan/anggota']);
        } else {
          this.router.navigate(['/home/simpanan']);
        }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  gotoNew() {
    // this.router.navigate(['/transaksi/pengajuan/tmb-pengajuan'], { queryParams: { id: id }, skipLocationChange: false });
    if (this.dataUser.role === 'AGT') {
      this.router.navigate(['/home/simpanan/anggota/new']);
    } else {
      this.router.navigate(['/home/simpanan/new']);
    }
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListSimpanan();
  }

  backClicked() {
    this.router.navigate(['/home/simpanan']);
  }
}
