import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PengajuanKonsumerComponent } from './pengajuan-konsumer.component';

describe('PengajuanKonsumerComponent', () => {
  let component: PengajuanKonsumerComponent;
  let fixture: ComponentFixture<PengajuanKonsumerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengajuanKonsumerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PengajuanKonsumerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
