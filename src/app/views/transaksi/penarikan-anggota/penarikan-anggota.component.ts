import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Pengajuan} from '../../../model/pengajuan';
import {AppService} from '../../../service/app.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';
import {User} from "../../../model/user";
import {AuthService} from "../../../service/auth.service";
import {Perusahaan} from '../../../model/Perusahaan';
declare let $: any;
@Component({
  selector: 'app-penarikan-anggota',
  templateUrl: './penarikan-anggota.component.html',
  styleUrls: ['./penarikan-anggota.component.scss'],
  providers: [DatePipe]
})
export class PenarikanAnggotaComponent implements OnInit {

  formBuilder: FormGroup;

  loading = false;

  sub: any;
  nik;
  name;
  company;
  kodeAnggota;
  dataUser: User;
  addModal;

  selectedPengajuan = new Pengajuan();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';

  listPengajuan:     Pengajuan[] = [];
  listPerusahaan: Perusahaan[] = [];

  totalSimpananWajib = 0;
  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;
  tanggalMulai = '';
  tanggalAkhir = '';
  kodePerusahaan = '';
  selectedPerusahaan;
  statusPengajuan = '';
  saldoSimpanan;
  currentMonth = new Date().getMonth() + 1;
  currentYear = new Date().getFullYear();

  isDesc = true;
  column: string;
  direction: number;

  fileUploadDokumen: File;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private route: ActivatedRoute,
              private auth: AuthService) { }

  ngOnInit() {
    $('select').selectpicker();
    this.dataUser = JSON.parse(this.auth.getToken());
    this.sub = this.route.queryParams.subscribe(params => {
      this.nik = !params['nik'] ? this.dataUser.nomorNik : params['nik'];
      this.name = !params['name'] ? this.dataUser.nama : params['name'];
      this.company = !params['company'] ? this.dataUser.kodePerusahaan : params['company'];
      this.kodeAnggota = !params['kodeAnggota'] ? this.dataUser.nomorAnggota : params['kodeAnggota'];
    });
    // this.tanggalMulai = this.datePipe.transform(new Date(this.currentYear + '-' + this.currentMonth + '-01'), 'yyyy-MM-dd');
    // this.tanggalAkhir = this.datePipe.transform(new Date(this.currentYear, this.currentMonth, 0), 'yyyy-MM-dd');
    if (this.dataUser.role === 'HRD') {
      this.statusPengajuan = 'NEW';
      $('select[name=status]').selectpicker('val', 'NEW');
    } else if (this.dataUser.role === 'PENGAWAS') {
      this.statusPengajuan = 'APP';
      $('select[name=status]').selectpicker('val', 'APP');
    } else if (this.dataUser.role === 'ADM') {
      this.statusPengajuan = 'VRF';
      $('select[name=status]').selectpicker('val', 'VRF');
    }
    this.doGetSaldoSimpanan();
    this.doGetListPinjaman();
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetSaldoSimpanan() {
    // this.loading = true;
    this.saldoSimpanan = null;
    let nomorNik;
    if (this.nik != null) {
      nomorNik = this.kodeAnggota;
    } else {
      nomorNik = this.dataUser.nomorAnggota;
    }
    this.service.getSaldoSimpanan(nomorNik, {}).subscribe((res) => {
      // this.loading = false;
      if (res.data) {
        const data = JSON.parse(res.data);
        this.saldoSimpanan = data;
      } else {
        this.saldoSimpanan = 0;
      }
    }, (err) => {
      // this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Saldo Simpanan!', 'Ooops!');
    });
  }

  doGetListPinjaman() {
    this.loading = true;
    const param: any = {
      mulai: ((this.currentPage - 1) * this.itemsPerPage) + 1,
      akhir: this.currentPage * this.itemsPerPage,
      tanggalMulai: this.tanggalMulai,
      tanggalAkhir: this.tanggalAkhir,
      kodePerusahaan: this.company,
      // search: this.nik,
      // tipePengajuan: '',
      statusPengajuan: this.statusPengajuan,
      nama: this.name,
      nomorNik : this.nik,
      kategoriPengajuan: 'PENARIKAN',
      // nomorAnggota: this.kodeAnggota
    };
    this.service.getListPengajuan(param).subscribe((res) => {
      if (res) {
        // this.totalItems = res.length;
        // const data = res;
        const datas = JSON.parse(res.data);
        this.totalItems = datas.totalRow;
        const data = datas.data;
        this.listPengajuan = _.orderBy(data, ['tanggalPengajuan'], 'desc');
        this.loading = false;
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Pengajuan!', 'Ooops!');
    });
  }

  setSelectedPengajuan(data: Pengajuan) {
    this.selectedPengajuan = data;
  }

  submit() {
    const param: any = this.selectedPengajuan;
    param.tanggalUpdate = new Date();
    param.statusPengajuan = 'CAN';
    this.service.doPostPengajuan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListPinjaman();
        // if (this.dataUser.role === 'AGT') {
        //     this.router.navigate(['/pinjaman/anggota']);
        // } else {
        //     this.router.navigate(['/pinjaman']);
        // }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  doSubmitDokumen() {
    const formDataDokumen: FormData = new FormData();
    formDataDokumen.append('file', this.fileUploadDokumen);
    // formData.append("fotoKtp", this.uploadedFileKtp);
    // formData.append("param", param);
    this.service.doPostUploadFile(formDataDokumen, 'pengajuan', this.dataUser.nomorAnggota).subscribe((res) => {
      this.loading = false;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam upload Dokumen!', 'Ooops!');
    });
  }

  onFileChangedDokumen(event){
    this.fileUploadDokumen = event.item(0);
  }

  gotoNew(id: number) {
    const totalSimpanan = !this.saldoSimpanan.totalSimpanan ? 0 : parseFloat(this.saldoSimpanan.totalSimpanan.toString());
    // console.log(totalSimpanan);
    // this.router.navigate(['/penarikan/anggota/new']);
    if (totalSimpanan > 0) {
      if (this.dataUser.role === 'AGT') {
        this.router.navigate(['/home/pengajuan/penarikan/anggota/new']);
      } else {
        this.router.navigate(['/home/pengajuan/penarikan/new']);
      }
    } else {
      this.toastr.error(`Anggota belum bisa melakukan Penarikan. Total Simpanan 0!`, 'Maaf!');
    }
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListPinjaman();
  }

  backClicked() {
    this.router.navigate(['/home/pengajuan/penarikan']);
  }
}
