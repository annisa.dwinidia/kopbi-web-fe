import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenarikanAnggotaComponent } from './penarikan-anggota.component';

describe('PenarikanAnggotaComponent', () => {
  let component: PenarikanAnggotaComponent;
  let fixture: ComponentFixture<PenarikanAnggotaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenarikanAnggotaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenarikanAnggotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
