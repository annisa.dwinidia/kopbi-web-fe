import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagihanPinjamanComponent } from './tagihan-pinjaman.component';

describe('TagihanPinjamanComponent', () => {
  let component: TagihanPinjamanComponent;
  let fixture: ComponentFixture<TagihanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagihanPinjamanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagihanPinjamanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
