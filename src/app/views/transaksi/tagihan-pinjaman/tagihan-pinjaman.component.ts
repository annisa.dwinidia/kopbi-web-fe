import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Pengajuan} from '../../../model/pengajuan';
import {AppService} from '../../../service/app.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';
import {User} from "../../../model/user";
import {AuthService} from "../../../service/auth.service";
import {Simpanan} from '../../../model/Simpanan';
import {Perusahaan} from '../../../model/Perusahaan';
declare let $: any;
@Component({
  selector: 'app-tagihan-pinjaman',
  templateUrl: './tagihan-pinjaman.component.html',
  styleUrls: ['./tagihan-pinjaman.component.scss'],
  providers: [DatePipe]
})
export class TagihanPinjamanComponent implements OnInit {

  formBuilder: FormGroup;
  dataUser: User;

  loading = false;

  addModal;

  selectedTagihan = new Simpanan();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';

  listTagihan:     any[] = [];
  listPerusahaan:     Perusahaan[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;
  tanggalMulai = '';
  tanggalAkhir = '';
  kodePerusahaan = '';
  selectedPerusahaan;
  currentMonth = new Date().getMonth() + 1;
  currentYear = new Date().getFullYear();

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    $('select').selectpicker();
    this.dataUser = JSON.parse(this.auth.getToken());
    // this.tanggalMulai = this.datePipe.transform(new Date(this.currentYear + '-' + this.currentMonth + '-01'), 'yyyy-MM-dd');
    // this.tanggalAkhir = this.datePipe.transform(new Date(this.currentYear, this.currentMonth, 0), 'yyyy-MM-dd');
    // this.doGetListTagihan();
    this.doGetListCompany();
    this.doGetListTagihan();
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetListTagihan() {
    this.loading = true;
    let selectedPerusahaan;
    if (this.dataUser.role === 'HRD') {
      selectedPerusahaan = this.dataUser.kodePerusahaan;
    } else {
      selectedPerusahaan = !this.selectedPerusahaan ? '' : JSON.parse(this.selectedPerusahaan).kodePerusahaan;
    }
    const param: any = {
      tanggalMulai: this.tanggalMulai,
      tanggalAkhir: this.tanggalAkhir,
      kodePerusahaan: selectedPerusahaan,
      // search: this.filterText,
      statusPinjaman: 'BELUM',
      mulai: ((this.currentPage - 1) * this.itemsPerPage) + 1,
      akhir: this.itemsPerPage,
      // nomorNik: ''
      nomorAnggota: ''
    };
    this.service.getListPinjaman(param).subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.totalItems = datas.totalRow;
      const data = datas.data;
      this.listTagihan =   _.orderBy(data, function(dateObj) {
        return new Date(dateObj.tanggalSimpanan);
      }, 'desc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Tagihan!', 'Ooops!');
    });
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  gotoNew(id: number) {
    // this.router.navigate(['/transaksi/pengajuan/tmb-pengajuan'], { queryParams: { id: id }, skipLocationChange: false });
    this.router.navigate(['/home/tagihan-pinjaman']);
  }

  gotoList(nik: number, name: string, kodePerusahaan: string, kodeAnggota: string) {
    this.router.navigate(['/home/tagihan/pinjaman/anggota'], { queryParams: { kodeAnggota: kodeAnggota, name: name }, skipLocationChange: false });
  }

  setSelectedTagihan(data: Simpanan, title) {
    this.selectedTagihan = data;
    this.modalTitle = title;
  }

  submit() {
    const param: any = this.selectedTagihan;
    param.tanggalUpdate = new Date();
    this.service.doPostPengajuan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListTagihan();
        // if (this.dataUser.role === 'AGT') {
        //     this.router.navigate(['/pinjaman/anggota']);
        // } else {
        //     this.router.navigate(['/pinjaman']);
        // }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });

  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListTagihan();
  }
}
