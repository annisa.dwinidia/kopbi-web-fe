import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import {Pengajuan} from '../../../model/pengajuan';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppService} from '../../../service/app.service';
import {DatePipe} from '@angular/common';
import {User} from '../../../model/user';
import {AuthService} from '../../../service/auth.service';
import {Perusahaan} from "../../../model/Perusahaan";
import {Anggota} from "../../../model/anggota";
import {Barang} from "../../../model/Barang";
import {configKOPBI} from '../../../config/application-config';
import {Simpanan} from '../../../model/Simpanan';
declare let $: any;

@Component({
  selector: 'app-tmb-angsuran',
  templateUrl: './tmb-angsuran.component.html',
  styleUrls: ['./tmb-angsuran.component.scss'],
  providers: [DatePipe]
})
export class TmbAngsuranComponent implements OnInit {

  formBuilder: FormGroup;
  dataUser: User;

  loading = false;
  addModal;
  selectedPengajuan = new Pengajuan();
  selectedPerusahaan;
  selectedAnggota;
  selectedItem;
  selectedLamaAngsuran;
  selectedJenisAngsuran;

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';
  tipePengajuan     = '';

  listPengajuan:     Pengajuan[] = [];
  listPerusahaan: Perusahaan[] = [];
  listAnggota: Anggota[] = [];
  listLamaAngsuran: any;
  listSimulasi: any[] = [];
  listItem: Barang[] = [];
  listJenisAngsuran: any[] = [];

  configSelectPerusahaan = null;
  configSelectAnggota = null;
  totalAngsuran = 0;
  totalBunga = 0;
  totalBiayaAdmin = 0;
  totalTagihan = 0;

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;

  now = new Date();
  currentMonth;
  currentYear;
  selectedMonth;
  selectedYear;

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    $('select').selectpicker();
    this.dataUser = JSON.parse(this.auth.getToken());
    this.currentYear = this.now.getFullYear();
    this.currentMonth = this.now.getMonth() + 1;
    this.configSelectPerusahaan = {
      displayKey: 'namaPerusahaan',
      search: true,
      height: 'auto',
      placeholder: 'Pilih Perusahaan',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Perusahaan..',
      // searchOnKey: 'departmentCode'
    };
    this.configSelectAnggota = {
      displayKey: 'nama',
      search: true,
      height: 'auto',
      placeholder: 'Pilih Anggota',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Anggota..',
      // searchOnKey: 'departmentCode'
    };
    this.listLamaAngsuran = Array(24).fill([], 0, 24).map((x, i) => i + 1);
    this.formBuilder = this.fb.group({
      kodeAngsuran: [''],
      nomorPinjaman: [''],
      tanggalJatuhTempo: [''],
      angsuranKe: [''],
      nominalAngsuran: [''],
      persenBunga: [''],
      nominalBunga: [''],
      totalBunga: [''],
      biayaAdmin: [''],
      totalBayar: [''],
      status: ['']
    });
    this.formBuilder.get('bulan').valueChanges.subscribe(data => {
      this.selectedMonth = data;
    });
    this.formBuilder.get('tahun').valueChanges.subscribe(data => {
      this.selectedYear = data;
    });
    this.doGetListCompany();
  }

  doGetListMember() {
    this.loading = true;
    const perusahaan = !this.selectedPerusahaan ? '' : this.selectedPerusahaan[0].kodePerusahaan;
    this.service.getListAnggotaPerusahaan(perusahaan, 1, 100).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listAnggota = _.sortBy(data, ['id']);
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Anggota!', 'Ooops!');
    });
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doSubmitSimpanan() {
    if (this.dataUser.role === 'AGT') {
      this.doSubmitSimpananAnggota();
    } else {
      this.doSubmitSimpananAdmin();
    }
  }

  doSubmitSimpananAnggota() {
    const lamaAngsuran = !this.selectedLamaAngsuran ? '' : this.selectedLamaAngsuran.split(' ');
    const param: Simpanan = new Simpanan();
    param.tanggalSimpanan = new Date();
    param.nomorKtp =  this.dataUser.nomorKtp;
    param.nomorAnggota = this.dataUser.nomorAnggota;
    param.nama = this.dataUser.nama;
    param.nomorNik = this.dataUser.nomorNik;
    param.nomorHp = this.dataUser.nomorHp;
    param.kodePerusahaan = this.dataUser.kodePerusahaan;
    param.namaPerusahaan = this.dataUser.namaPerusahaan;
    param.alamatPerusahaan = this.dataUser.alamatPerusahaan;
    param.emailPerusahaan = this.dataUser.emailPerusahaan;
    // param.kodeSimpanan = '';
    param.namaSimpanan = this.formBuilder.get('namaSimpanan').value;
    param.nominalSimpanan = Number(this.formBuilder.get('nominalSimpanan').value.toString().replace(new RegExp(',', 'g'), ''));
    param.jenisIuran = this.formBuilder.get('jenisIuran').value;
    param.ketIuran = this.formBuilder.get('ketIuran').value;
    param.mediaIuran = this.formBuilder.get('mediaIuran').value;
    param.bulan = this.selectedMonth;
    param.tahun = this.selectedYear;
    param.keterangan = this.formBuilder.get('keterangan').value;
    param.kodeUser = this.dataUser.kodeAnggota;
    param.namaUser = this.dataUser.nama;
    param.tanggalUpdate = new Date();
    this.service.doPostSimpanan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        if (this.dataUser.role === 'AGT') {
          this.router.navigate(['/home/simpanan/anggota']);
        } else {
          this.router.navigate(['/home/simpanan']);
        }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  doSubmitSimpananAdmin() {
    const param: any = {
      kodeUser: this.selectedAnggota[0].kodeAnggota,
      namaUser: this.selectedAnggota[0].nama,
      tanggalUpdate: new Date(),
      // kodePengajuan: '',
      kodeTipePengajuan: this.formBuilder.get('kodeTipePengajuan').value,
      tipePengajuan: this.formBuilder.get('tipePengajuan').value,
      nomorKtp: this.selectedAnggota[0].nomorKtp,
      nomorAnggota: this.selectedAnggota[0].nomorAnggota,
      nama: this.selectedAnggota[0].nama,
      nomorNik: this.selectedAnggota[0].nomorNik,
      nomorHp: this.selectedAnggota[0].nomorHp,
      kodePerusahaan: this.selectedAnggota[0].kodePerusahaan,
      namaPerusahaan: this.selectedAnggota[0].namaPerusahaan,
      alamatPerusahaan: this.selectedAnggota[0].alamatPerusahaan,
      emailPerusahaan: this.selectedAnggota[0].emailPerusahaan,
      tanggalPengajuan: new Date(),
      lamaAngsuran: this.formBuilder.get('lamaAngsuran').value,
      nominalAngsuran: Number(this.formBuilder.get('nominalAngsuran').value.toString().replace(new RegExp(',', 'g'), '')),
      persenBunga: configKOPBI.persentaseBunga,
      nominalBunga: this.listSimulasi[0].biayaBunga,
      totalBunga: this.totalBunga,
      biayaAdmin: configKOPBI.biayaAdmin,
      kodeBarang: '',
      namaBarang: this.formBuilder.get('namaBarang').value,
      ambilDariKas: '',
      ketKas: '',
      keterangan: this.formBuilder.get('keterangan').value,
      statusPengajuan: 'APP',
      tanggalPerubahan: '',
      tanggalTempo: '',
      tanggalJatuhTempo: this.listSimulasi[this.listSimulasi.length - 1].tanggalJatuhTempo,
      nomorPinjaman: ''
    };
    this.service.doPostSimpanan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        if (this.dataUser.role === 'AGT') {
          this.router.navigate(['/home/pinjaman/anggota']);
        } else {
          this.router.navigate(['/home/pinjaman']);
        }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  setNamaSimpanan(value) {
    console.log(value);
    this.formBuilder.get('namaSimpanan').setValue(value);
    this.formBuilder.get('ketIuran').setValue(value);
  }
}
