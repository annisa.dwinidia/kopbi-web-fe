import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PengajuanAnggotaComponent } from './pengajuan-anggota.component';

describe('PengajuanAnggotaComponent', () => {
  let component: PengajuanAnggotaComponent;
  let fixture: ComponentFixture<PengajuanAnggotaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengajuanAnggotaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PengajuanAnggotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
