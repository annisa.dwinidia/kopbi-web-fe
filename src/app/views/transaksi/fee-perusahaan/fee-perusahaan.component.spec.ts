import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeePerusahaanComponent } from './fee-perusahaan.component';

describe('FeePerusahaanComponent', () => {
  let component: FeePerusahaanComponent;
  let fixture: ComponentFixture<FeePerusahaanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeePerusahaanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeePerusahaanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
