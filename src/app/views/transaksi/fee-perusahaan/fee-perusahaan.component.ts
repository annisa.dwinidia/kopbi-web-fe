import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Pengajuan} from '../../../model/pengajuan';
import {AppService} from '../../../service/app.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';
import {User} from "../../../model/user";
import {AuthService} from "../../../service/auth.service";
import {Simpanan} from '../../../model/Simpanan';
import {Perusahaan} from '../../../model/Perusahaan';
declare let $: any;
@Component({
  selector: 'app-fee-perusahaan',
  templateUrl: './fee-perusahaan.component.html',
  styleUrls: ['./fee-perusahaan.component.scss'],
  providers: [DatePipe]
})
export class FeePerusahaanComponent implements OnInit {

  formBuilder: FormGroup;
  dataUser: User;

  loading = false;

  addModal;

  selectedFee = new Simpanan();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';

  listFee:     Simpanan[] = [];
  listPerusahaan:     Perusahaan[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;
  tanggalMulai = '';
  tanggalAkhir = '';
  kodePerusahaan = '';
  selectedPerusahaan;
  currentMonth = new Date().getMonth() + 1;
  currentYear = new Date().getFullYear();

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    $('select').selectpicker();
    this.dataUser = JSON.parse(this.auth.getToken());
    this.formBuilder = this.fb.group({
      namaPemohon: '',
      nominal: '',
      tujuan: ''
    });
    // this.tanggalMulai = this.datePipe.transform(new Date(this.currentYear + '-' + this.currentMonth + '-01'), 'yyyy-MM-dd');
    // this.tanggalAkhir = this.datePipe.transform(new Date(this.currentYear, this.currentMonth, 0), 'yyyy-MM-dd');
    // this.doGetListFee();
    this.doGetListCompany();
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = res;
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetListFee() {
    // this.loading = true;
    // this.dataUser = JSON.parse(this.auth.getToken());
    // const param: any = {
    //   tanggalMulai: this.tanggalMulai,
    //   tanggalAkhir: this.tanggalAkhir,
    //   kodePerusahaan: this.kodePerusahaan,
    //   search: this.filterText,
    //   kodeTagihan: '',
    //   jenisIuran: '',
    //   mulai: ((this.currentPage - 1) * this.itemsPerPage) + 1,
    //   akhir: this.itemsPerPage
    // };
    // this.service.getListSimpanan(param).subscribe((res) => {
    //   this.loading = false;
    //   const datas = JSON.parse(res.data);
    //   this.totalItems = datas.totalRow;
    //   const data = datas.data;
    //   this.listFee =   _.orderBy(data, function(dateObj) {
    //     return new Date(dateObj.tanggalSimpanan);
    //   }, 'desc');
    // }, (err) => {
    //   this.loading = false;
    //   this.toastr.error('Terjadi kesalahan dalam pengambilan data Fee!', 'Ooops!');
    // });
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  gotoNew(id: number) {
    // this.router.navigate(['/transaksi/pengajuan/tmb-pengajuan'], { queryParams: { id: id }, skipLocationChange: false });
    this.router.navigate(['/home/fee-perusahaan']);
  }

  gotoList(nik: number, name: string) {
    this.router.navigate(['/home/fee-perusahaan'], { queryParams: { nik: nik, name: name }, skipLocationChange: false });
  }

  setSelectedFee(data: Simpanan, title) {
    this.selectedFee = data;
    this.modalTitle = title;
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListFee();
  }

  submit() {
    // const param: any = this.selectedFee;
    // param.tanggalUpdate = new Date();
    // this.service.doPostPengajuan(param).subscribe((res) => {
    //   this.loading = false;
    //   if (res.success) {
    //     this.toastr.success(this.successMessage, 'Success!');
    //     this.doGetListFee();
    //     // if (this.dataUser.role === 'AGT') {
    //     //     this.router.navigate(['/pinjaman/anggota']);
    //     // } else {
    //     //     this.router.navigate(['/pinjaman']);
    //     // }
    //   } else {
    //     this.toastr.error(this.errorMesssage, 'Ooops!');
    //   }
    // }, (err) => {
    //   this.loading = false;
    //   this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    // });

  }
}
