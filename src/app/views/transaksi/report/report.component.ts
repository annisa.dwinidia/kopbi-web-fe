import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Pengajuan} from '../../../model/pengajuan';
import {AppService} from '../../../service/app.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import {User} from '../../../model/user';
import {AuthService} from '../../../service/auth.service';
import {Simpanan} from '../../../model/Simpanan';
import {ArusKas} from '../../../model/ArusKas';
import {Bank} from '../../../model/bank';
declare let $: any;

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
  providers: [DatePipe]
})
export class ReportComponent implements OnInit {

  formBuilder: FormGroup;
  dataUser: User;

  loading = false;

  addModal;

  selectedKas = new ArusKas();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';

  listKas:     ArusKas[] = [];
  listBank: Bank[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;
  tanggalMulai = '';
  tanggalAkhir = '';
  currentMonth = new Date().getMonth() + 1;
  currentYear = new Date().getFullYear();
  dariKas = '';
  untukKas = '';

  isDesc = true;
  column: string;
  direction: number;
  tanggalTransaksi = new Date();

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    // $('select').selectpicker();
    // this.tanggalMulai = this.datePipe.transform(new Date(this.currentYear + '-' + this.currentMonth + '-01'), 'yyyy-MM-dd');
    // this.tanggalAkhir = this.datePipe.transform(new Date(this.currentYear, this.currentMonth, 0), 'yyyy-MM-dd');
    this.formBuilder = this.fb.group({
      tanggalTransaksi: '',
      nominal: '',
      keterangan: '',
      dariKas: '',
      untukKas: '',
      ketKas: '',
      jenisTransaksi: '',
      akun: ''
    });
    this.doGetListKas();
    this.doGetListBank();
  }

  doGetListBank() {
    this.loading = true;
    const param = {
      search: ''
    };
    this.service.getListBank(param).subscribe((res) => {
      this.loading = false;
      // console.log(res);
      const data = JSON.parse(res.data);
      this.listBank = data;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Bank!', 'Ooops!');
    });
  }

  doGetListKas() {
    this.loading = true;
    this.dataUser = JSON.parse(this.auth.getToken());
    const param: any = {
      mulai: ((this.currentPage - 1) * this.itemsPerPage) + 1,
      akhir: this.itemsPerPage,
      tanggalMulai: this.tanggalMulai,
      tanggalAkhir: this.tanggalAkhir,
      kodeDariKas: this.dariKas,
      kodeUntukKas: this.untukKas,
      jenisTransaksi: ''
    };
    this.service.getListReport(param).subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.totalItems = datas.totalRow; /*res.recordsTotal;*/
      const data = datas.data;
      this.listKas =   _.orderBy(data, 'tanggalTransaksi', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Laporan Kas!', 'Ooops!');
    });
  }

  doOpenFormAdd() {
    this.modalTitle = 'Tambah';
    this.formBuilder.reset();
    this.formBuilder.get('akun').setValue('');
    this.formBuilder.get('dariKas').setValue('');
    this.formBuilder.get('untukKas').setValue('');
  }

  doOpenFormEdit() {
    this.modalTitle = 'Ubah';
    this.formBuilder.reset();
    this.formBuilder.get('nominal').setValue(numeral(Number(this.selectedKas.nominal)).format('0,0'));
    this.formBuilder.get('keterangan').setValue(this.selectedKas.keterangan);
    this.formBuilder.get('akun').setValue(this.selectedKas.akun);
    this.formBuilder.get('dariKas').setValue(this.selectedKas.dariKas);
    this.formBuilder.get('untukKas').setValue(this.selectedKas.untukKas);
    this.formBuilder.get('ketKas').setValue(this.selectedKas.ketKas);
    this.formBuilder.get('tanggalTransaksi').setValue(this.selectedKas.tanggalTransaksi);
    // this.tanggalTransaksi = new Date(this.selectedKas.tanggalTransaksi);
    console.log(this.tanggalTransaksi);
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  gotoNew(id: number) {
    // this.router.navigate(['/transaksi/pengajuan/tmb-pengajuan'], { queryParams: { id: id }, skipLocationChange: false });
    this.router.navigate(['/home/kas/new']);
  }

  setSelectedKas(data: ArusKas, title) {
    this.selectedKas = data;
    this.modalTitle = title;
  }

  submit() {
    if (this.modalTitle === 'Tambah') {
      this.insert();
    } else if (this.modalTitle === 'Ubah') {
      this.update();
    }
  }

  insert() {
    const param = {
      tanggalTransaksi: this.formBuilder.get('tanggalTransaksi').value,
      nominal: this.formBuilder.get('nominal').value.toString().replace(new RegExp(',', 'g'), ''),
      keterangan: this.formBuilder.get('keterangan').value,
      akun: this.formBuilder.get('akun').value,
      dariKas: this.formBuilder.get('dariKas').value,
      untukKas: this.formBuilder.get('untukKas').value,
      jenisTransaksi: this.formBuilder.get('akun').value,
      ketJenisTransaksi: '',
      ketKas: this.formBuilder.get('ketKas').value,
      tanggalUpdate: new Date(),
      userUpdate: this.dataUser.nama,
      statusAktif: 'AKTIF'
    };
    this.service.doPostKas(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListKas();
        // if (this.dataUser.role === 'AGT') {
        //     this.router.navigate(['/pinjaman/anggota']);
        // } else {
        //     this.router.navigate(['/pinjaman']);
        // }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });

  }

  update() {
    const param = {
      id: this.selectedKas.id,
      tanggalTransaksi: this.formBuilder.get('tanggalTransaksi').value,
      nominal: this.formBuilder.get('nominal').value.toString().replace(new RegExp(',', 'g'), ''),
      keterangan: this.formBuilder.get('keterangan').value,
      akun: this.formBuilder.get('akun').value,
      dariKas: this.formBuilder.get('dariKas').value,
      untukKas: this.formBuilder.get('untukKas').value,
      jenisTransaksi: this.formBuilder.get('akun').value,
      ketJenisTransaksi: '',
      ketKas: this.formBuilder.get('ketKas').value,
      tanggalUpdate: new Date(),
      userUpdate: this.dataUser.nama,
      statusAktif: 'AKTIF'
    };
    this.service.doPostKas(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListKas();
        // if (this.dataUser.role === 'AGT') {
        //     this.router.navigate(['/pinjaman/anggota']);
        // } else {
        //     this.router.navigate(['/pinjaman']);
        // }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  delete() {
    const param = this.selectedKas;
    param.statusAktif = 'TIDAK AKTIF';
    this.service.doPostKas(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListKas();
        // if (this.dataUser.role === 'AGT') {
        //     this.router.navigate(['/pinjaman/anggota']);
        // } else {
        //     this.router.navigate(['/pinjaman']);
        // }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  setKeterangan() {
    const value = this.formBuilder.get('akun').value;
    if (value === 'Pemasukan') {
      this.formBuilder.get('ketKas').setValue('D');
    } else if (value === 'Pengeluaran') {
      this.formBuilder.get('ketKas').setValue('K');
    } else {
      this.formBuilder.get('ketKas').setValue('T');
    }
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListKas();
  }
}
