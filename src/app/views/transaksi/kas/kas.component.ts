import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Pengajuan} from '../../../model/pengajuan';
import {AppService} from '../../../service/app.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import {User} from '../../../model/user';
import {AuthService} from '../../../service/auth.service';
import {Simpanan} from '../../../model/Simpanan';
import {ArusKas} from '../../../model/ArusKas';
import {Bank} from '../../../model/bank';
declare let $: any;

@Component({
  selector: 'app-kas',
  templateUrl: './kas.component.html',
  styleUrls: ['./kas.component.scss'],
  providers: [DatePipe]
})
export class KasComponent implements OnInit {

  formBuilder: FormGroup;
  dataUser: User;

  loading = false;

  addModal;

  selectedKas = new ArusKas();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';

  listKas:     ArusKas[] = [];
  listBank: any[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;
  tanggalMulai = '';
  tanggalAkhir = '';
  currentMonth = new Date().getMonth() + 1;
  currentYear = new Date().getFullYear();
  dariKas = '';
  untukKas = '';

  isDesc = true;
  column: string;
  direction: number;
  tanggalTransaksi = new Date();

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    console.log(this.tanggalTransaksi);
    // $('select').selectpicker();
    this.tanggalMulai = this.datePipe.transform(new Date(this.currentYear + '-' + this.currentMonth + '-01'), 'yyyy-MM-dd');
    this.tanggalAkhir = this.datePipe.transform(new Date(this.currentYear, this.currentMonth, 0), 'yyyy-MM-dd');
    this.formBuilder = this.fb.group({
      tanggalTransaksi: ['', Validators.required],
      nominal: ['', Validators.required],
      keterangan: ['', Validators.required],
      kodeDariKas: '',
      dariKas: '',
      kodeUntukKas: '',
      untukKas: '',
      ketKas: '',
      jenisTransaksi: '',
      akun: ['', Validators.required],
      akunDari: '',
      akunUntuk: ''
    });
    this.doGetListKas();
    this.doGetListBank();
  }

  doGetListBank() {
    this.loading = true;
    const param = {
      search: ''
    };
    this.service.getListBank(param).subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.listBank = datas;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Bank!', 'Ooops!');
    });
  }

  doGetListKas() {
    this.loading = true;
    this.dataUser = JSON.parse(this.auth.getToken());
    const param: any = {
      mulai: ((this.currentPage - 1) * this.itemsPerPage) + 1,
      akhir: this.itemsPerPage,
      tanggalMulai: this.tanggalMulai,
      tanggalAkhir: this.tanggalAkhir,
      dariKas: this.dariKas,
      untukKas: this.untukKas,
      jenisTransaksi: ''
    };
    this.service.getListKas(param).subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.totalItems = datas.totalRow;
      // this.totalFiltered = 10; /*res.recordsFiltered;*/
      const data = datas.data;
      this.listKas =   _.orderBy(data, function(dateObj) {
        return new Date(dateObj.tanggalTransaksi);
      }, 'desc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Arus Kas!', 'Ooops!');
    });
  }

  doOpenFormAdd() {
    this.modalTitle = 'Tambah';
    this.formBuilder.reset();
    this.formBuilder.get('akun').setValue('');
    this.formBuilder.get('kodeDariKas').setValue('');
    this.formBuilder.get('kodeUntukKas').setValue('');
  }

  doOpenFormEdit() {
    this.modalTitle = 'Ubah';
    this.formBuilder.reset();
    this.formBuilder.get('nominal').setValue(numeral(Number(this.selectedKas.nominal)).format('0,0'));
    this.formBuilder.get('keterangan').setValue(this.selectedKas.keterangan);
    this.formBuilder.get('akun').setValue(this.selectedKas.akun);
    this.formBuilder.get('kodeDariKas').setValue(this.selectedKas.kodeDariKas);
    this.formBuilder.get('dariKas').setValue(this.selectedKas.dariKas);
    this.formBuilder.get('kodeUntukKas').setValue(this.selectedKas.kodeUntukKas);
    this.formBuilder.get('untukKas').setValue(this.selectedKas.untukKas);
    this.formBuilder.get('ketKas').setValue(this.selectedKas.ketKas);
    this.formBuilder.get('tanggalTransaksi').setValue(this.selectedKas.tanggalTransaksi);
    // this.tanggalTransaksi = new Date(this.selectedKas.tanggalTransaksi);
    // console.log(this.tanggalTransaksi);
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  gotoNew(id: number) {
    // this.router.navigate(['/transaksi/pengajuan/tmb-pengajuan'], { queryParams: { id: id }, skipLocationChange: false });
    this.router.navigate(['/home/kas/new']);
  }

  setSelectedKas(data: ArusKas, title) {
    this.selectedKas = data;
    this.modalTitle = title;
  }

  submit() {
    if (this.modalTitle === 'Tambah') {
      this.insert();
    } else if (this.modalTitle === 'Ubah') {
      this.update();
    }
  }

  insert() {
    const param = {
      tanggalTransaksi: this.formBuilder.get('tanggalTransaksi').value,
      nominal: this.formBuilder.get('nominal').value.toString().replace(new RegExp(',', 'g'), ''),
      keterangan: this.formBuilder.get('keterangan').value,
      akun: this.formBuilder.get('akun').value,
      kodeDariKas: this.formBuilder.get('kodeDariKas').value,
      dariKas: this.formBuilder.get('dariKas').value,
      kodeUntukKas: this.formBuilder.get('kodeUntukKas').value,
      untukKas: this.formBuilder.get('untukKas').value,
      jenisTransaksi: this.formBuilder.get('akun').value,
      ketJenisTransaksi: '',
      ketKas: this.formBuilder.get('ketKas').value,
      tanggalUpdate: new Date(),
      userUpdate: this.dataUser.nama,
      statusAktif: 'AKTIF'
    };
    this.service.doPostKas(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListKas();
        // if (this.dataUser.role === 'AGT') {
        //     this.router.navigate(['/pinjaman/anggota']);
        // } else {
        //     this.router.navigate(['/pinjaman']);
        // }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });

  }

  update() {
    const param = {
      id: this.selectedKas.id,
      tanggalTransaksi: this.formBuilder.get('tanggalTransaksi').value,
      nominal: this.formBuilder.get('nominal').value.toString().replace(new RegExp(',', 'g'), ''),
      keterangan: this.formBuilder.get('keterangan').value,
      akun: this.formBuilder.get('akun').value,
      kodeDariKas: this.formBuilder.get('kodeDariKas').value,
      dariKas: this.formBuilder.get('dariKas').value,
      kodeUntukKas: this.formBuilder.get('kodeUntukKas').value,
      untukKas: this.formBuilder.get('untukKas').value,
      jenisTransaksi: this.formBuilder.get('akun').value,
      ketJenisTransaksi: '',
      ketKas: this.formBuilder.get('ketKas').value,
      tanggalUpdate: new Date(),
      userUpdate: this.dataUser.nama,
      statusAktif: 'AKTIF'
    };
    this.service.doPostKas(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListKas();
        // if (this.dataUser.role === 'AGT') {
        //     this.router.navigate(['/pinjaman/anggota']);
        // } else {
        //     this.router.navigate(['/pinjaman']);
        // }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  delete() {
    const param = this.selectedKas;
    param.statusAktif = 'TIDAK AKTIF';
    this.service.doPostKas(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListKas();
        // if (this.dataUser.role === 'AGT') {
        //     this.router.navigate(['/pinjaman/anggota']);
        // } else {
        //     this.router.navigate(['/pinjaman']);
        // }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  setKeterangan() {
    const value = this.formBuilder.get('akun').value;
    if (value === 'Pemasukan') {
      this.formBuilder.get('ketKas').setValue('D');
    } else if (value === 'Pengeluaran') {
      this.formBuilder.get('ketKas').setValue('K');
    } else {
      this.formBuilder.get('ketKas').setValue('T');
    }
  }

  setKeteranganDari() {
    const kodeBank = this.formBuilder.get('kodeDariKas').value;
    const bank = this.listBank.find(x => x.kodeBank === kodeBank);
    this.formBuilder.get('dariKas').setValue(bank.namaBank);
  }

  setKeteranganUntuk() {
    const kodeBank = this.formBuilder.get('kodeUntukKas').value;
    const bank = this.listBank.find(x => x.kodeBank === kodeBank);
    this.formBuilder.get('untukKas').setValue(bank.namaBank);
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListKas();
  }
}
