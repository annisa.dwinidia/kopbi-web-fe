import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { AppService } from '../../../service/app.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import {User} from '../../../model/user';
import {Perusahaan} from '../../../model/Perusahaan';
import {AuthService} from '../../../service/auth.service';
import {Konfederasi} from '../../../model/Konfederasi';
import {Bank} from '../../../model/bank';
import {DatePipe} from '@angular/common';
import {config} from '../../../config/application-config';
import {AESEncryptDecryptService} from '../../../service/aesencrypt-decrypt-service.service';
import * as numeral from 'numeral';
declare let $: any;
@Component({
  selector: 'app-registrasi',
  templateUrl: './registrasi.component.html',
  styleUrls: ['./registrasi.component.scss'],
  providers: [DatePipe]
})
export class RegistrasiComponent implements OnInit {
  @ViewChild('fileFoto')
  fileFoto: ElementRef;

  @ViewChild('fileAnggota')
  fileAnggota: ElementRef;

  formBuilder: FormGroup;

  loading = false;
  dataUser: User;

  linkImage = config.KOPBI_IMAGES_BASE_URL;

  addModal;
  selectedAnggota = new User();
  selectedPerusahaan;

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  filterTextNIK     = '';

  listAnggota:     User[] = [];
  searchAnggota;
  listPerusahaan:   Perusahaan[] = [];
  listKonfederasi:   Konfederasi[] = [];
  listBank:   Bank[] = [];
  filterPerusahaan;
  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;

  configSelectPerusahaan = null;
  configSelectKonfederasi = null;
  configSelectBank = null;

  isDesc = true;
  column: string;
  direction: number;

  public imagePathAnggota;
  imgURLAnggota: any;
  public messageAnggota: string;

  uploadedFileKtp: File;
  uploadedFileAnggota: File;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private router: Router, private auth: AuthService, private datePipe: DatePipe, private encryption: AESEncryptDecryptService)
  { }

  ngOnInit() {
    this.dataUser = JSON.parse(this.auth.getToken());
    this.doGetListMember();
    this.doGetListCompany();
    this.doGetListKonfederasi();
    // this.doGetListBank();
    this.configSelectPerusahaan = {
      displayKey: 'namaPerusahaan',
      search: true,
      height: '200px',
      placeholder: 'Pilih Perusahaan',
      customComparator: () => {},
      limitTo: this.listPerusahaan.length,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Perusahaan..',
      // searchOnKey: 'departmentCode'
    };
    this.configSelectKonfederasi = {
      displayKey: 'namaKonfederasi',
      search: true,
      height: '200px',
      placeholder: 'Pilih Konfederasi',
      customComparator: () => {},
      limitTo: this.listKonfederasi.length,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Konfederasi..',
      // searchOnKey: 'departmentCode'
    };
    this.configSelectBank = {
      displayKey: 'namaBank',
      search: true,
      height: '200px',
      placeholder: 'Pilih Bank',
      customComparator: () => {},
      limitTo: this.listBank.length,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Bank..',
      // searchOnKey: 'departmentCode'
    };
    this.formBuilder = this.fb.group({
      kodeAnggota: [''],
      nomorAnggota:  [''],
      nama:  ['', Validators.required],
      nomorKtp:  ['', Validators.required],
      nomorNik:  ['', Validators.required],
      jenisKelamin:  ['', Validators.required],
      tempatLahir:  ['', Validators.required],
      tanggalLahir:  ['', Validators.required],
      status:  ['', Validators.required],
      pekerjaan:  ['', Validators.required],
      alamat:  ['', Validators.required],
      nomorHp:  ['', Validators.required],
      kodePerusahaan:  ['', Validators.required],
      namaPerusahaan:  ['', Validators.required],
      alamatPerusahaan:  [''],
      emailPerusahaan:  [''],
      lokasiPenempatan: ['', Validators.required],
      kodeJabatan:  ['', Validators.required],
      namaJabatan:  [''],
      kodeBank:  [''],
      namaBank:  ['', Validators.required],
      cabangBank:  ['', Validators.required],
      nomorRekening:  ['', Validators.required],
      tanggalRegistrasi: [''],
      password: [''],
      statusAnggota: ['', Validators.required],
      role: ['', Validators.required],
      namaKonfederasi: ['', Validators.required],
      kodeKonfederasi: ['', Validators.required],

      pendapatan: ['', Validators.required],
      simpananWajib: [''],
      simpananSukarela: [''],
      jabatanKeanggotaan: [''],
      namaSaudaraDekat: ['', Validators.required],
      hubunganSaudara: ['', Validators.required],
      alamatSaudara: ['', Validators.required],
      nomorHpSaudara: ['', Validators.required],
      emailPribadi: ['', Validators.required],

      selectedKonfederasi: '',
      selectedPerusahaan: ''
    });
  }

  doSearching() {
    this.listAnggota = [];
    if (this.filterTextNIK !== '') {
      this.doGetListMemberByKTP();
    } else {
      this.doGetListMember();
    }
  }

  doGetListMember() {
    this.loading = true;
    // console.log(this.filterPerusahaan);
    const param = {
      kodePerusahaan: 'PRBD',
      search: this.filterText,
      mulai: (this.currentPage - 1) * this.itemsPerPage + 1,
      akhir: this.itemsPerPage,
      status: 'REG'
    };
    this.service.getListRegistrasi(param).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.totalItems = data.totalRow;
      this.listAnggota =  _.orderBy(data.data, function(dateObj) {
        return new Date(dateObj.tanggalRegistrasi);
      }, 'desc');
      // console.log(data);
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Anggota!', 'Ooops!');
    });
  }

  doGetListMemberByKTP() {
    this.loading = true;
    const param = this.filterTextNIK;
    this.service.getAnggotaByKTP(param).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      if (res.success) {
        this.totalItems = 1;
        this.listAnggota = [data];
      }
      // console.log(data);
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Anggota!', 'Ooops!');
    });
  }

  doSearchMemberByKTP() {
    this.loading = true;
    const param = this.formBuilder.get('nomorKtp').value;
    this.formBuilder.reset();
    this.searchAnggota = '';
    this.formBuilder.get('nomorKtp').setValue(param);
    this.service.getAnggotaByKTP(param).subscribe((res) => {
      this.loading = false;
      if (res.data) {
        const data = JSON.parse(res.data);
        this.searchAnggota =  data;
        const selectedAnggota = this.searchAnggota;
        this.formBuilder.get('kodeAnggota').setValue(selectedAnggota.kodeAnggota);
        this.formBuilder.get('nomorAnggota').setValue(selectedAnggota.nomorAnggota);
        this.formBuilder.get('nama').setValue(selectedAnggota.nama);
        this.formBuilder.get('nomorKtp').setValue(selectedAnggota.nomorKtp);
        this.formBuilder.get('nomorNik').setValue(selectedAnggota.nomorNik);
        this.formBuilder.get('jenisKelamin').setValue(selectedAnggota.jenisKelamin);
        this.formBuilder.get('tempatLahir').setValue(selectedAnggota.tempatLahir);
        this.formBuilder.get('tanggalLahir').setValue(selectedAnggota.tanggalLahir);
        this.formBuilder.get('status').setValue(selectedAnggota.status);
        this.formBuilder.get('pekerjaan').setValue(selectedAnggota.pekerjaan);
        this.formBuilder.get('alamat').setValue(selectedAnggota.alamat);
        this.formBuilder.get('nomorHp').setValue(selectedAnggota.nomorHp);
        this.formBuilder.get('kodePerusahaan').setValue(selectedAnggota.kodePerusahaan);
        this.formBuilder.get('namaPerusahaan').setValue(selectedAnggota.namaPerusahaan);
        this.formBuilder.get('alamatPerusahaan').setValue(selectedAnggota.alamatPerusahaan);
        this.formBuilder.get('emailPerusahaan').setValue(selectedAnggota.emailPerusahaan);
        this.formBuilder.get('lokasiPenempatan').setValue(selectedAnggota.lokasiPenempatan);
        this.formBuilder.get('namaJabatan').setValue(selectedAnggota.namaJabatan);
        this.formBuilder.get('kodeJabatan').setValue(selectedAnggota.kodeJabatan);
        // this.formBuilder.get('kodeBank').setValue(this.selectedAnggota.kodeBank);
        // this.formBuilder.get('namaBank').setValue(this.selectedAnggota.namaBank);
        this.formBuilder.get('namaBank').setValue(selectedAnggota.namaBank);
        this.formBuilder.get('cabangBank').setValue(selectedAnggota.cabangBank);
        this.formBuilder.get('nomorRekening').setValue(selectedAnggota.nomorRekening);
        this.formBuilder.get('tanggalRegistrasi').setValue(this.datePipe.transform(selectedAnggota.tanggalRegistrasi, 'yyyy-MM-dd'));
        this.formBuilder.get('password').setValue(selectedAnggota.password);
        this.formBuilder.get('statusAnggota').setValue(selectedAnggota.statusAnggota);
        this.formBuilder.get('role').setValue(selectedAnggota.role);
        this.formBuilder.get('namaKonfederasi').setValue(selectedAnggota.namaKonfederasi);
        this.formBuilder.get('kodeKonfederasi').setValue(selectedAnggota.kodeKonfederasi);
        this.formBuilder.get('selectedKonfederasi').setValue(selectedAnggota.namaKonfederasi);
        const perusahaan: any = {
          id: '',
          kodePerusahaan: selectedAnggota.kodePerusahaan,
          namaPerusahaan: selectedAnggota.namaPerusahaan,
          alamatPerusahaan: selectedAnggota.alamatPerusahaan,
          emailPerusahaan: selectedAnggota.emailPerusahaan,
        };
        this.formBuilder.get('selectedPerusahaan').setValue(perusahaan);
        this.formBuilder.get('namaSaudaraDekat').setValue(selectedAnggota.namaSaudaraDekat);
        this.formBuilder.get('hubunganSaudara').setValue(selectedAnggota.hubunganSaudara);
        this.formBuilder.get('alamatSaudara').setValue(selectedAnggota.alamatSaudara);
        this.formBuilder.get('nomorHpSaudara').setValue(selectedAnggota.nomorHpSaudara);
        this.formBuilder.get('emailPribadi').setValue(selectedAnggota.emailPribadi);
        /*Edit Henda Prasetya 08/12/2019*/
        this.formBuilder.get('pendapatan').setValue(selectedAnggota.pendapatan);
        this.formBuilder.get('simpananWajib').setValue(selectedAnggota.simpananWajib);
        this.formBuilder.get('simpananSukarela').setValue(selectedAnggota.simpananSukarela);
        this.formBuilder.get('jabatanKeanggotaan').setValue(selectedAnggota.jabatanKeanggotaan);
      } else {
        this.toastr.error('Silahkan Lengkapi Data!', 'Data tidak ditemukan!');
      }
      // console.log(data);
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Anggota!', 'Ooops!');
    });
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetListKonfederasi() {
    this.loading = true;
    const param = {
      search: ''
    };
    this.service.getListKonfederasi(param).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listKonfederasi = data;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetListBank() {
    this.loading = true;
    const param = {
      search: ''
    };
    this.service.getListBank(param).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listBank = data;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }


  doOpenFormAdd() {
    this.listPerusahaan = [...this.listPerusahaan];
    this.listKonfederasi = [...this.listKonfederasi];
    // this.fileFoto.nativeElement.value = '';
    // this.fileAnggota.nativeElement.value = '';
    this.modalTitle = 'Tambah';
    this.formBuilder.reset();
    this.formBuilder.get('jenisKelamin').setValue('-');
    // this.formBuilder.get('kodeBank').setValue('-');
    this.formBuilder.get('role').setValue('-');
    this.formBuilder.get('status').setValue('-');
    this.formBuilder.get('namaKonfederasi').setValue('-');
    this.formBuilder.get('statusAnggota').setValue('-');
    this.formBuilder.get('kodeJabatan').setValue('-');
  }

  doOpenFormEdit() {
    this.listPerusahaan = [...this.listPerusahaan];
    this.listKonfederasi = [...this.listKonfederasi];
    // this.fileFoto.nativeElement.value = '';
    // this.fileAnggota.nativeElement.value = '';
    this.modalTitle = 'Edit';
    this.formBuilder.get('kodeAnggota').setValue(this.selectedAnggota.kodeAnggota);
    this.formBuilder.get('nomorAnggota').setValue(this.selectedAnggota.nomorAnggota);
    this.formBuilder.get('nama').setValue(this.selectedAnggota.nama);
    this.formBuilder.get('nomorKtp').setValue(this.selectedAnggota.nomorKtp);
    this.formBuilder.get('nomorNik').setValue(this.selectedAnggota.nomorNik);
    this.formBuilder.get('jenisKelamin').setValue(this.selectedAnggota.jenisKelamin);
    this.formBuilder.get('tempatLahir').setValue(this.selectedAnggota.tempatLahir);
    this.formBuilder.get('tanggalLahir').setValue(this.selectedAnggota.tanggalLahir);
    this.formBuilder.get('status').setValue(this.selectedAnggota.status);
    this.formBuilder.get('pekerjaan').setValue(this.selectedAnggota.pekerjaan);
    this.formBuilder.get('alamat').setValue(this.selectedAnggota.alamat);
    this.formBuilder.get('nomorHp').setValue(this.selectedAnggota.nomorHp);
    this.formBuilder.get('kodePerusahaan').setValue(this.selectedAnggota.kodePerusahaan);
    this.formBuilder.get('namaPerusahaan').setValue(this.selectedAnggota.namaPerusahaan);
    this.formBuilder.get('alamatPerusahaan').setValue(this.selectedAnggota.alamatPerusahaan);
    this.formBuilder.get('emailPerusahaan').setValue(this.selectedAnggota.emailPerusahaan);
    this.formBuilder.get('lokasiPenempatan').setValue(this.selectedAnggota.lokasiPenempatan);
    this.formBuilder.get('namaJabatan').setValue(this.selectedAnggota.namaJabatan);
    this.formBuilder.get('kodeJabatan').setValue(this.selectedAnggota.kodeJabatan);
    // this.formBuilder.get('kodeBank').setValue(this.selectedAnggota.kodeBank);
    // this.formBuilder.get('namaBank').setValue(this.selectedAnggota.namaBank);
    this.formBuilder.get('namaBank').setValue(this.selectedAnggota.namaBank);
    this.formBuilder.get('cabangBank').setValue(this.selectedAnggota.cabangBank);
    this.formBuilder.get('nomorRekening').setValue(this.selectedAnggota.nomorRekening);
    this.formBuilder.get('tanggalRegistrasi').setValue(this.datePipe.transform(this.selectedAnggota.tanggalRegistrasi, 'yyyy-MM-dd'));
    this.formBuilder.get('password').setValue(this.selectedAnggota.password);
    this.formBuilder.get('statusAnggota').setValue(this.selectedAnggota.statusAnggota);
    this.formBuilder.get('role').setValue(this.selectedAnggota.role);
    this.formBuilder.get('namaKonfederasi').setValue(this.selectedAnggota.namaKonfederasi);
    this.formBuilder.get('kodeKonfederasi').setValue(this.selectedAnggota.kodeKonfederasi);
    this.formBuilder.get('selectedKonfederasi').setValue(this.selectedAnggota.namaKonfederasi);
    const perusahaan: any = {
      id: '',
      kodePerusahaan: this.selectedAnggota.kodePerusahaan,
      namaPerusahaan: this.selectedAnggota.namaPerusahaan,
      alamatPerusahaan: this.selectedAnggota.alamatPerusahaan,
      emailPerusahaan: this.selectedAnggota.emailPerusahaan,
    };
    this.formBuilder.get('selectedPerusahaan').setValue(perusahaan);
    this.formBuilder.get('namaSaudaraDekat').setValue(this.selectedAnggota.namaSaudaraDekat);
    this.formBuilder.get('hubunganSaudara').setValue(this.selectedAnggota.hubunganSaudara);
    this.formBuilder.get('alamatSaudara').setValue(this.selectedAnggota.alamatSaudara);
    this.formBuilder.get('nomorHpSaudara').setValue(this.selectedAnggota.nomorHpSaudara);
    this.formBuilder.get('emailPribadi').setValue(this.selectedAnggota.emailPribadi);
    /*Edit Henda Prasetya 08/12/2019*/
    this.formBuilder.get('pendapatan').setValue(this.selectedAnggota.pendapatan);
    this.formBuilder.get('simpananWajib').setValue(numeral(Number(this.selectedAnggota.simpananWajib)).format('0,0'));
    this.formBuilder.get('simpananSukarela').setValue(numeral(Number(this.selectedAnggota.simpananSukarela)).format('0,0'));
    this.formBuilder.get('jabatanKeanggotaan').setValue(this.selectedAnggota.jabatanKeanggotaan);
  }

  submit() {
    this.loading = true;
    let param: any = {};
    if (this.modalTitle === 'Tambah') {
      param = {
        // kodeAnggota: '',
        nomorAnggota:  !this.searchAnggota ? '' : this.searchAnggota.nomorAnggota,
        nama:  this.formBuilder.get('nama').value,
        nomorKtp: this.formBuilder.get('nomorKtp').value,
        nomorNik:  this.formBuilder.get('nomorNik').value,
        jenisKelamin:  this.formBuilder.get('jenisKelamin').value,
        tempatLahir:  this.formBuilder.get('tempatLahir').value,
        tanggalLahir:  this.formBuilder.get('tanggalLahir').value,
        status:  this.formBuilder.get('status').value,
        pekerjaan:  this.formBuilder.get('pekerjaan').value,
        alamat:  this.formBuilder.get('alamat').value,
        nomorHp:  this.formBuilder.get('nomorHp').value,
        kodePerusahaan:  this.formBuilder.get('kodePerusahaan').value,
        namaPerusahaan:  this.formBuilder.get('namaPerusahaan').value,
        alamatPerusahaan: this.formBuilder.get('alamatPerusahaan').value,
        emailPerusahaan:  this.formBuilder.get('emailPerusahaan').value,
        lokasiPenempatan: this.formBuilder.get('lokasiPenempatan').value,
        kodeJabatan:  this.formBuilder.get('kodeJabatan').value,
        namaJabatan:  this.formBuilder.get('namaJabatan').value,
        // kodeBank:  this.formBuilder.get('kodeBank').value,
        // namaBank:  this.formBuilder.get('namaBank').value,
        kodeBank:  '',
        namaBank:  this.formBuilder.get('namaBank').value,
        cabangBank:  this.formBuilder.get('cabangBank').value,
        nomorRekening:  this.formBuilder.get('nomorRekening').value,
        tanggalRegistrasi: this.formBuilder.get('tanggalRegistrasi').value,
        password: '',
        statusAnggota: this.formBuilder.get('statusAnggota').value,
        role: this.formBuilder.get('role').value,
        namaKonfederasi: this.formBuilder.get('namaKonfederasi').value,
        kodeKonfederasi: this.formBuilder.get('kodeKonfederasi').value,
        pendapatan: !this.formBuilder.get('pendapatan').value ? '' : this.formBuilder.get('pendapatan').value.toString().replace(new RegExp(',', 'g'), ''),
        simpananWajib: !this.formBuilder.get('simpananWajib').value ? '' : this.formBuilder.get('simpananWajib').value.toString().replace(new RegExp(',', 'g'), ''),
        simpananSukarela: !this.formBuilder.get('simpananSukarela').value ? '' : this.formBuilder.get('simpananSukarela').value.toString().replace(new RegExp(',', 'g'), ''),
        jabatanKeanggotaan: this.formBuilder.get('jabatanKeanggotaan').value,
        namaSaudaraDekat: this.formBuilder.get('namaSaudaraDekat').value,
        hubunganSaudara: this.formBuilder.get('hubunganSaudara').value,
        alamatSaudara: this.formBuilder.get('alamatSaudara').value,
        nomorHpSaudara: this.formBuilder.get('nomorHpSaudara').value,
        emailPribadi: this.formBuilder.get('emailPribadi').value
      };
      if (this.searchAnggota) {
        param.kodeAnggota = this.searchAnggota.kodeAnggota;
      }
    } else if (this.modalTitle === 'Edit') {
      param = {
        kodeAnggota: this.selectedAnggota.kodeAnggota,
        nomorAnggota:  this.selectedAnggota.nomorAnggota,
        nama:  this.formBuilder.get('nama').value,
        nomorKtp: this.formBuilder.get('nomorKtp').value,
        nomorNik:  this.formBuilder.get('nomorNik').value,
        jenisKelamin:  this.formBuilder.get('jenisKelamin').value,
        tempatLahir:  this.formBuilder.get('tempatLahir').value,
        tanggalLahir:  this.formBuilder.get('tanggalLahir').value,
        status:  this.formBuilder.get('status').value,
        pekerjaan:  this.formBuilder.get('pekerjaan').value,
        alamat:  this.formBuilder.get('alamat').value,
        nomorHp:  this.formBuilder.get('nomorHp').value,
        kodePerusahaan:  this.formBuilder.get('kodePerusahaan').value,
        namaPerusahaan:  this.formBuilder.get('namaPerusahaan').value,
        alamatPerusahaan: this.formBuilder.get('alamatPerusahaan').value,
        emailPerusahaan:  this.formBuilder.get('emailPerusahaan').value,
        lokasiPenempatan: this.formBuilder.get('lokasiPenempatan').value,
        kodeJabatan:  this.formBuilder.get('kodeJabatan').value,
        namaJabatan:  this.formBuilder.get('namaJabatan').value,
        // kodeBank:  this.formBuilder.get('kodeBank').value,
        // namaBank:  this.formBuilder.get('namaBank').value,
        kodeBank:  '',
        namaBank:  this.formBuilder.get('namaBank').value,
        cabangBank:  this.formBuilder.get('cabangBank').value,
        nomorRekening:  this.formBuilder.get('nomorRekening').value,
        tanggalRegistrasi: this.formBuilder.get('tanggalRegistrasi').value,
        password: this.selectedAnggota.password,
        statusAnggota: this.formBuilder.get('statusAnggota').value,
        role: this.formBuilder.get('role').value,
        namaKonfederasi: this.formBuilder.get('namaKonfederasi').value,
        kodeKonfederasi: this.formBuilder.get('kodeKonfederasi').value,
        pendapatan: !this.formBuilder.get('pendapatan').value ? '' : this.formBuilder.get('pendapatan').value.toString().replace(new RegExp(',', 'g'), ''),
        simpananWajib: !this.formBuilder.get('simpananWajib').value ? '' : this.formBuilder.get('simpananWajib').value.toString().replace(new RegExp(',', 'g'), ''),
        simpananSukarela: !this.formBuilder.get('simpananSukarela').value ? '' : this.formBuilder.get('simpananSukarela').value.toString().replace(new RegExp(',', 'g'), ''),
        jabatanKeanggotaan: this.formBuilder.get('jabatanKeanggotaan').value,
        namaSaudaraDekat: this.formBuilder.get('namaSaudaraDekat').value,
        hubunganSaudara: this.formBuilder.get('hubunganSaudara').value,
        alamatSaudara: this.formBuilder.get('alamatSaudara').value,
        nomorHpSaudara: this.formBuilder.get('nomorHpSaudara').value,
        emailPribadi: this.formBuilder.get('emailPribadi').value
      };
    }
    // console.log(param);
    if (this.uploadedFileAnggota !== undefined) {
      const formDataFoto: FormData = new FormData();
      formDataFoto.append('file', this.uploadedFileAnggota);
      // formData.append("fotoKtp", this.uploadedFileKtp);
      // formData.append("param", param);
      this.service.doPostUploadFile(formDataFoto, 'anggota', this.selectedAnggota.nomorAnggota).subscribe((res) => {
        this.loading = false;
      }, (err) => {
        this.loading = false;
        this.toastr.error('Terjadi kesalahan dalam upload Foto Anggota!', 'Ooops!');
      });
    }
    if (this.uploadedFileKtp !== undefined) {
      const formDataFotoKtp: FormData = new FormData();
      formDataFotoKtp.append('file', this.uploadedFileKtp);
      this.service.doPostUploadFile(formDataFotoKtp, 'ktp', this.selectedAnggota.nomorKtp).subscribe((res) => {
        this.loading = false;
      }, (err) => {
        this.loading = false;
        this.toastr.error('Terjadi kesalahan dalam upload Foto KTP!', 'Ooops!');
      });
    }
    this.service.doPostAnggota(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.doGetListMember();
        this.toastr.success('Approved Registrasi Berhasil', 'Success!');
      } else {
        this.toastr.error('Approved Registrasi Gagagl', 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  doReject() {
    this.service.doRejectRegistration(this.selectedAnggota.nomorKtp).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.doGetListMember();
        this.toastr.success('Rejected Registrasi Berhasil', 'Success!');
      } else {
        this.toastr.error('Rejected Registrasi Gagal!!', 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  resetPassword() {
    const param: any = {
      userName: this.selectedAnggota.nomorHp,
      // oldPassword: this.encryption.encrypt(this.selectedAnggota.password),
      password: this.encryption.encrypt('123456')
    };
    this.service.resetPassword(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success('Password Berhasil Diganti', 'Success!');
      } else {
        this.toastr.error('Penggantian Password Gagal!!', 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  setSelectedAnggota(data: User) {
    this.selectedAnggota = data;
    // console.log(data);
  }

  setNamaJabatan(value) {
    this.formBuilder.get('namaJabatan').setValue(value);
  }

  setNamaBank(value) {
    this.formBuilder.get('namaBank').setValue(value);
  }

  setPerusahaan() {
    const perusahaan = this.formBuilder.get('selectedPerusahaan').value;
    console.log(perusahaan);
    this.formBuilder.get('kodePerusahaan').setValue(perusahaan.kodePerusahaan);
    this.formBuilder.get('namaPerusahaan').setValue(perusahaan.namaPerusahaan);
    this.formBuilder.get('alamatPerusahaan').setValue(perusahaan.alamatPerusahaan);
    this.formBuilder.get('emailPerusahaan').setValue(perusahaan.emailPerusahaan);
  }

  setKonfederasi() {
    const konfederasi = this.formBuilder.get('selectedKonfederasi').value;
    // console.log(konfederasi);
    this.formBuilder.get('namaKonfederasi').setValue(konfederasi.namaKonfederasi);
    this.formBuilder.get('kodeKonfederasi').setValue(konfederasi.kodeKonfederasi);
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListMember();
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  goToProfile(id: number) {
    this.router.navigate(['/home/recruitment/profile'], { queryParams: { id: id }, skipLocationChange: false });
  }

  gotoPinjaman(nik: number, name: string, company: string) {
    this.router.navigate([]).then(result => {  window.open('#/home/pinjaman/anggota?nik=' + nik + '&name=' + name + '&company=' + company, '_blank'); });
    // this.router.navigate(['/pinjaman/anggota'], { queryParams: { nik: nik, name: name }, skipLocationChange: false });
  }

  gotoSimpanan(nik: number, name: string, company: string) {
    this.router.navigate([]).then(result => {  window.open('#/home/simpanan/anggota?nik=' + nik + '&name=' + name + '&company=' + company , '_blank'); });
    // this.router.navigate(['/simpanan/anggota'], { queryParams: { nik: nik, name: name }, skipLocationChange: false });
  }

  onFileChangedAnggota(event) {
    this.uploadedFileAnggota = event.item(0);
  }

  onFileChangedKtp(event) {
    this.uploadedFileKtp = event.item(0);
  }

  clearFilterKTP() {
    this.filterTextNIK = '';
  }

  clearFilterNIK() {
    this.filterText = '';
  }
}

