import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import {Pengajuan} from '../../../model/pengajuan';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppService} from '../../../service/app.service';
import {DatePipe} from '@angular/common';
import {User} from '../../../model/user';
import {AuthService} from '../../../service/auth.service';
import {Perusahaan} from "../../../model/Perusahaan";
import {Anggota} from "../../../model/anggota";
import {Barang} from "../../../model/Barang";
import {configKOPBI} from '../../../config/application-config';

declare let $: any;

@Component({
  selector: 'app-tmb-penarikan',
  templateUrl: './tmb-penarikan.component.html',
  styleUrls: ['./tmb-penarikan.component.scss'],
  providers: [DatePipe]
})
export class TmbPenarikanComponent implements OnInit {

  @ViewChild('fileUpload') fileUpload: ElementRef;
  formBuilder: FormGroup;
  dataUser: User;

  loading = false;
  addModal;
  selectedPengajuan = new Pengajuan();
  selectedPerusahaan;
  selectedAnggota;
  selectedItem;
  selectedLamaAngsuran;
  selectedJenisAngsuran;

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';
  tipePengajuan     = '';

  listPengajuan:     Pengajuan[] = [];
  listPerusahaan: Perusahaan[] = [];
  listAnggota: Anggota[] = [];
  listLamaAngsuran: any;
  listSimulasi: any[] = [];
  listItem: Barang[] = [];
  listJenisAngsuran: any[] = [];

  configSelectPerusahaan = null;
  configSelectAnggota = null;
  configSelectItem = null;
  configSelectLamaAngsuran = null;
  configSelectJenisAngsuran = null;

  totalAngsuran = 0;
  totalBunga = 0;
  totalBiayaAdmin = 0;
  totalTagihan = 0;

  persentaseBunga = 0;
  bungaPinjaman = 0;
  bungaPinjaman2 = 0;
  biayaAdmin = 0;

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;

  isDesc = true;
  column: string;
  direction: number;
  selectedName = '';
  saldoSimpanan;
  penarikanWajib = 0;
  penarikanPokok = 0;
  penarikanSukarela = 0;

  fileUploadDokumen: File;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    $('select').selectpicker();
    this.dataUser = JSON.parse(this.auth.getToken());
    this.configSelectPerusahaan = {
      displayKey: 'namaPerusahaan',
      search: true,
      height: 'auto',
      placeholder: 'Pilih Perusahaan',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Perusahaan..',
      // searchOnKey: 'departmentCode'
    };
    this.configSelectAnggota = {
      displayKey: 'nama',
      search: true,
      height: 'auto',
      placeholder: 'Pilih Anggota',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Anggota..',
      // searchOnKey: 'departmentCode'
    };

    this.listLamaAngsuran = Array(24).fill([], 0, 24).map((x, i) => i + 1);
    this.formBuilder = this.fb.group({
      kodeTipePengajuan: [''],
      tipePengajuan: [''],
      nomorKtp: [''],
      nama: [''],
      nomorAnggota: [''],
      tanggalPengajuan: [''],
      lamaAngsuran: [''],
      nominalAngsuran: [''],
      nominalPengajuan: [''],
      persenBunga: [''],
      nominalBunga: [''],
      totalBunga: [''],
      biayaAdmin: [''],
      kodeBarang: [''],
      namaBarang: [''],
      ambilDariKas: [''],
      ketKas: [''],
      keterangan: ['', Validators.required],
      statusPengajuan: [''],
      tanggalPerubahan: [''],
      tanggalTempo: [''],
      tanggalJatuhTempo: [''],
      nomorPinjaman: [''],
      kodePerusahaan: [''],
      simpananPokok: ['0'],
      simpananWajib: ['0'],
      simpananSukarela: ['0']
    });
    this.doGetListCompany();
    if (this.dataUser.role === 'AGT') {
      this.doGetSaldoSimpanan(this.dataUser);
    }
  }

  doGetListMember() {
    // this.loading = true;
    const param = {
      kodePerusahaan: !this.selectedPerusahaan ? this.dataUser.kodePerusahaan : JSON.parse(this.selectedPerusahaan).kodePerusahaan,
      search: this.selectedName,
      mulai: 1,
      akhir: 50,
      status: ''
    };
    this.service.getListAllAnggota(param).subscribe((res) => {
      // const perusahaan = !this.selectedPerusahaan ? '' : JSON.parse(this.selectedPerusahaan);
      // this.service.getListAnggotaPerusahaan(perusahaan.kodePerusahaan, 1, 100).subscribe((res) => {
      //   this.loading = false;
      const data = JSON.parse(res.data);
      this.listAnggota = _.sortBy(data.data, ['id']);
    }, (err) => {
      // this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Anggota!', 'Ooops!');
    });
  }

  doGetSaldoSimpanan(member) {
    // this.loading = true;
    this.saldoSimpanan = null;
    const nomorNik = member.nomorAnggota;
    this.service.getSaldoSimpanan(nomorNik, {}).subscribe((res) => {
      // this.loading = false;
      if (res.data) {
        const datas = JSON.parse(res.data);
        this.saldoSimpanan = datas;
      }
    }, (err) => {
      // this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Saldo Simpanan!', 'Ooops!');
    });
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  rounding(nominal) {
    return Math.round(nominal / 100) * 100;
  }

  doSubmitPengajuan() {
    // const penarikan = Math.round(this.formBuilder.get('nominalPengajuan').value.toString().replace(new RegExp(',', 'g'), ''));
    // let simpanan = 0;
    // const tipe = this.formBuilder.get('kodeTipePengajuan').value;
    // let typeName = '';
    // if (tipe === 'POKOK') {
    //   simpanan = parseFloat(this.saldoSimpanan.totalSimpananPokok.toString());
    //   typeName = 'Pokok';
    // } else if (tipe === 'WAJIB') {
    //   simpanan = parseFloat(this.saldoSimpanan.totalSimpananWajib.toString());
    //   typeName = 'Wajib';
    // } else if (tipe === 'SUKARELA') {
    //   simpanan = parseFloat(this.saldoSimpanan.totalSimpananSukarela.toString());
    //   typeName = 'Sukarela';
    // }
    const penarikanPokok = Math.round(this.formBuilder.get('simpananPokok').value.toString().replace(new RegExp(',', 'g'), ''));
    const penarikanWajib = Math.round(this.formBuilder.get('simpananWajib').value.toString().replace(new RegExp(',', 'g'), ''));
    const penarikanSukarela = Math.round(this.formBuilder.get('simpananSukarela').value.toString().replace(new RegExp(',', 'g'), ''));
    const simpananPokok = parseFloat(this.saldoSimpanan.totalSimpananPokok.toString());
    const simpananWajib = parseFloat(this.saldoSimpanan.totalSimpananWajib.toString());
    const simpananSukarela = parseFloat(this.saldoSimpanan.totalSimpananSukarela.toString());
    const file = !this.fileUpload ? '' : this.fileUpload.nativeElement.value;
    if ((penarikanPokok > 0 && file !== '') || (penarikanWajib > 0 && file !== '') || (penarikanSukarela > 0)) {
      console.log(penarikanPokok + ':' + simpananPokok);
      console.log(penarikanWajib + ':' + simpananWajib);
      console.log(penarikanSukarela + ':' + simpananSukarela);
      if (penarikanPokok <= simpananPokok && penarikanWajib <= simpananWajib && penarikanSukarela <= simpananSukarela) {
        if (this.dataUser.role === 'AGT') {
          this.doSubmitPengajuanAnggota();
        } else {
          this.doSubmitPengajuanAdmin();
        }
        if (this.fileUploadDokumen !== undefined) {
          const formDataDokumen: FormData = new FormData();
          formDataDokumen.append('file', this.fileUploadDokumen);
          // formData.append("fotoKtp", this.uploadedFileKtp);
          // formData.append("param", param);
          this.service.doPostUploadFile(formDataDokumen, 'pengajuan', this.dataUser.nomorAnggota).subscribe((res) => {
            this.loading = false;
          }, (err) => {
            this.loading = false;
            this.toastr.error('Terjadi kesalahan dalam upload Dokumen!', 'Ooops!');
          });
        }
      } else {
        // this.toastr.error(`Total Simpanan  ${typeName} tidak cukup untuk melakukan Penarikan!!!`, 'Ooops!');
        this.toastr.error(`Total Simpanan  tidak cukup untuk melakukan Penarikan!!!`, 'Ooops!');
      }
    } else {
      this.toastr.error(`Upload Dokumen terlebih dahulu!!!`, 'Ooops!');
    }
  }

  doSubmitPengajuanAnggota() {
    this.loading = true;
    const lamaAngsuran = !this.selectedLamaAngsuran ? '' : this.selectedLamaAngsuran.split(' ');
    const param: any = {
      kodeUser: this.dataUser.kodeAnggota,
      namaUser: this.dataUser.nama,
      tanggalUpdate: new Date(),
      // kodePengajuan: '',
      kodeTipePengajuan: this.formBuilder.get('kodeTipePengajuan').value,
      tipePengajuan: this.formBuilder.get('tipePengajuan').value,
      nomorKtp: this.dataUser.nomorKtp,
      nomorAnggota: this.dataUser.nomorAnggota,
      nama: this.dataUser.nama,
      nomorNik: this.dataUser.nomorNik,
      nomorHp: this.dataUser.nomorHp,
      kodePerusahaan: this.dataUser.kodePerusahaan,
      namaPerusahaan: this.dataUser.namaPerusahaan,
      alamatPerusahaan: this.dataUser.alamatPerusahaan,
      emailPerusahaan: this.dataUser.emailPerusahaan,
      tanggalPengajuan: new Date(),
      lamaAngsuran: 0,
      nominalPengajuan: Math.round(this.formBuilder.get('nominalPengajuan').value.toString().replace(new RegExp(',', 'g'), '')),
      nominalAngsuran: 0,
      persenBunga: 0,
      nominalBunga: 0,
      totalBunga: 0,
      biayaAdmin: 0,
      kodeBarang: '',
      namaBarang: '',
      // ambilDariKas: this.formBuilder.get('ambilDariKas').value,
      ambilDariKas: '',
      ketKas: 'D',
      keterangan: this.formBuilder.get('keterangan').value,
      statusPengajuan: 'NEW',
      tanggalPerubahan: '',
      tanggalTempo: '',
      tanggalJatuhTempo: '',
      nomorPinjaman: '',
      kategoriPengajuan: 'PENARIKAN',
      simpananWajib: this.penarikanWajib,
      simpananPokok: this.penarikanPokok,
      simpananSukarela: this.penarikanSukarela,
      lokasiPenempatan: this.dataUser.lokasiPenempatan
    };
    this.service.doPostPengajuan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.formBuilder.reset();
        this.toastr.success(this.successMessage, 'Success!');
        if (this.dataUser.role === 'AGT') {
          this.router.navigate(['/home/pengajuan/penarikan/anggota']);
        } else {
          this.router.navigate(['/home/pengajuan/penarikan']);
        }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  backClicked() {
    if (this.dataUser.role === 'AGT') {
      this.router.navigate(['/home/pengajuan/penarikan/anggota']);
    } else {
      this.router.navigate(['/home/pengajuan/penarikan']);
    }
  }

  setPenarikan() {
    const val = Math.round(this.formBuilder.get('nominalPengajuan').value.toString().replace(new RegExp(',', 'g'), ''));
    if (this.formBuilder.get('kodeTipePengajuan').value === 'POKOK') {
      this.penarikanPokok = val;
    } else if (this.formBuilder.get('kodeTipePengajuan').value === 'WAJIB') {
      this.penarikanWajib = val;
    } else if (this.formBuilder.get('kodeTipePengajuan').value === 'SUKARELA') {
      this.penarikanSukarela = val;
    }
  }

  doSubmitPengajuanAdmin() {
    const penarikanPokok = parseFloat(this.formBuilder.get('simpananPokok').value.toString().replace(new RegExp(',', 'g'), ''));
    const penarikanWajib = parseFloat(this.formBuilder.get('simpananWajib').value.toString().replace(new RegExp(',', 'g'), ''));
    const penarikanSukarela = parseFloat(this.formBuilder.get('simpananSukarela').value.toString().replace(new RegExp(',', 'g'), ''));
    this.loading = true;
    // const anggota = JSON.parse(this.selectedAnggota);
    const anggota = this.selectedAnggota;
    const param: any = {
      kodeUser: anggota.kodeAnggota,
      namaUser: anggota.nama,
      tanggalUpdate: new Date(),
      // kodePengajuan: '',
      kodeTipePengajuan: 'UANG',
      tipePengajuan: 'Uang',
      nomorKtp: anggota.nomorKtp,
      nomorAnggota: anggota.nomorAnggota,
      nama: anggota.nama,
      nomorNik: anggota.nomorNik,
      nomorHp: anggota.nomorHp,
      kodePerusahaan: anggota.kodePerusahaan,
      namaPerusahaan: anggota.namaPerusahaan,
      alamatPerusahaan: anggota.alamatPerusahaan,
      emailPerusahaan: anggota.emailPerusahaan,
      tanggalPengajuan: new Date(),
      lamaAngsuran: 0,
      nominalPengajuan: penarikanWajib + penarikanPokok + penarikanSukarela,
      nominalAngsuran: 0,
      persenBunga: 0,
      nominalBunga: 0,
      totalBunga: 0,
      biayaAdmin: 0,
      kodeBarang: '',
      namaBarang: '',
      // ambilDariKas: this.formBuilder.get('ambilDariKas').value,
      ambilDariKas: '',
      ketKas: 'D',
      keterangan: this.formBuilder.get('keterangan').value,
      statusPengajuan: 'APP',
      tanggalPerubahan: '',
      tanggalTempo: '',
      tanggalJatuhTempo: '',
      nomorPinjaman: '',
      kategoriPengajuan: 'PENARIKAN',
      simpananWajib: penarikanWajib,
      simpananPokok: penarikanPokok,
      simpananSukarela: penarikanSukarela,
      lokasiPenempatan: anggota.lokasiPenempatan
    };
    // console.log(this.selectedAnggota);
    this.service.doPostPengajuan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.formBuilder.reset();
        this.toastr.success(this.successMessage, 'Success!');
        if (this.dataUser.role === 'AGT') {
          this.router.navigate(['/home/pengajuan/penarikan/anggota']);
        } else {
          this.router.navigate(['/home/pengajuan/penarikan']);
        }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  onMemberChanged(val: string) {
    const name = val.split(' - ');
    console.log(name[1]);
    if (this.listAnggota.length > 0) {
      const selectedMember = this.getSelectedMemberByNik(name[1]);
      this.selectedAnggota = selectedMember;
      if (this.selectedAnggota !== null) {
        this.doGetSaldoSimpanan(selectedMember);
      }
    }
    // console.log(this.selectedAnggota);
  }

  getSelectedMemberByNik(selectedNik: string) {
    return this.listAnggota.find(member => member.nomorNik === selectedNik);
  }

  onFileChangedDokumen(event) {
    this.fileUploadDokumen = event.item(0);
  }
}
