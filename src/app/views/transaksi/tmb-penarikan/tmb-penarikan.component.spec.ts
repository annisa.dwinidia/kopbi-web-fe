import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmbPenarikanComponent } from './tmb-penarikan.component';

describe('TmbPenarikanComponent', () => {
  let component: TmbPenarikanComponent;
  let fixture: ComponentFixture<TmbPenarikanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmbPenarikanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmbPenarikanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
