import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagihanSimpananAktifComponent } from './tagihan-simpanan-aktif.component';

describe('TagihanSimpananAktifComponent', () => {
  let component: TagihanSimpananAktifComponent;
  let fixture: ComponentFixture<TagihanSimpananAktifComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagihanSimpananAktifComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagihanSimpananAktifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
