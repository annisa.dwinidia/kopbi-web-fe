import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataAnggotaComponent } from './data-anggota.component';

describe('DataAnggotaComponent', () => {
  let component: DataAnggotaComponent;
  let fixture: ComponentFixture<DataAnggotaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataAnggotaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataAnggotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
