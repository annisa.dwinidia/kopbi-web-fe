import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../service/app.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { ActivatedRoute, Router} from '@angular/router';
import { User} from '../../../model/user';
import { Perusahaan} from '../../../model/Perusahaan';
import { AuthService} from '../../../service/auth.service';
import { Location } from '@angular/common';
declare let $: any;
@Component({
  selector: 'app-data-anggota',
  templateUrl: './data-anggota.component.html',
  styleUrls: ['./data-anggota.component.scss']
})
export class DataAnggotaComponent implements OnInit {

  formBuilder: FormGroup;

  sub: any;
  loading = false;
  dataUser: User;

  addModal;
  selectedAnggota = new User();
  selectedPerusahaan;

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';

  listAnggota:     User[] = [];
  listPerusahaan:   Perusahaan[] = [];

  kodePerusahaan;
  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;

  configSelectPerusahaan = null;

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private router: Router, private auth: AuthService, private route: ActivatedRoute,
              private _location: Location) { }

  ngOnInit() {
    $('select').selectpicker();
    this.sub = this.route.queryParams.subscribe(params => {
      this.kodePerusahaan = params['id'];
    });
    this.dataUser = JSON.parse(this.auth.getToken());
    this.doGetListMember();
    this.doGetListCompany();
    this.configSelectPerusahaan = {
      displayKey: 'namaPerusahaan',
      search: true,
      height: 'auto',
      placeholder: 'Pilih Perusahaan',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Perusahaan..',
      // searchOnKey: 'departmentCode'
    };
    this.formBuilder = this.fb.group({
      kodeAnggota: '',
      nomorAnggota:  '',
      nama:  '',
      nomorKtp:  '',
      nomorNik:  '',
      jenisKelamin:  '',
      tempatLahir:  '',
      tanggalLahir:  '',
      status:  '',
      pekerjaan:  '',
      alamat:  '',
      nomorHp:  '',
      kodePerusahaan:  '',
      namaPerusahaan:  '',
      alamatPerusahaan:  '',
      emailPerusahaan:  '',
      lokasiPenempatan: '',
      kodeJabatan:  '',
      namaJabatan:  '',
      kodeBank:  '',
      namaBank:  '',
      cabangBank:  '',
      nomorRekening:  '',
      tanggalRegistrasi: '',
      password: '',
      statusAnggota: '',
      role: '',
      namaKonfederasi: ''
    });
  }

  doGetListMember() {
    this.loading = true;
    let kodePerusahaan;
    if (this.dataUser.role === 'HRD') {
      kodePerusahaan = !this.kodePerusahaan ? this.dataUser.kodePerusahaan : this.kodePerusahaan;
    } else {
      kodePerusahaan = !this.selectedPerusahaan ? '' : JSON.parse(this.selectedPerusahaan).kodePerusahaan;
    }
    const param = {
      kodePerusahaan: kodePerusahaan,
      search: this.filterText,
      mulai: (this.currentPage - 1) * this.itemsPerPage + 1,
      akhir: this.itemsPerPage,
      status: ''
    };
    this.service.getListAllAnggota(param).subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.totalItems = datas.totalRow;
      const data = datas.data;
      this.listAnggota = data;
      // this.listAnggota =  _.orderBy(data, function(dateObj) {
      //   return new Date(dateObj.tanggalRegistrasi);
      // }, 'desc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Anggota!', 'Ooops!');
    });
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  gotoPinjaman(nik: number, name: string, kodePerusahaan: string, kodeAnggota: string) {
    this.router.navigate([]).then(result => {  window.open('#/home/pinjaman/anggota?kodeAnggota=' + kodeAnggota + '&name=' + name, '_blank'); });
    // this.router.navigate(['/pinjaman/anggota'], { queryParams: { nik: nik, name: name }, skipLocationChange: false });
  }

  gotoSimpanan(nik: number, name: string, kodePerusahaan: string, kodeAnggota: string) {
    this.router.navigate([]).then(result => {  window.open('#/home/simpanan/anggota?kodeAnggota=' + kodeAnggota + '&name=' + name + '&company=' + kodePerusahaan, '_blank'); });
    // this.router.navigate(['/simpanan/anggota'], { queryParams: { nik: nik, name: name }, skipLocationChange: false });
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListMember();
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  goToProfile(id: number) {
    this.router.navigate(['/home/recruitment/profile'], { queryParams: { id: id }, skipLocationChange: false });
  }

  backClicked() {
    this._location.back();
  }
}
