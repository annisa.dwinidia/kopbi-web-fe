import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AppService} from '../../../service/app.service';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import {User} from '../../../model/user';
import {AuthService} from '../../../service/auth.service';
import {config} from '../../../config/application-config';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-verifikasi',
  templateUrl: './verifikasi.component.html',
  styleUrls: ['./verifikasi.component.scss']
})
export class VerifikasiComponent implements OnInit {

  loading = false;
  sub: any;
  id: number;
  dataUser: User;

  now = new Date();
  linkImage = config.KOPBI_IMAGES_BASE_URL;
  code;
  today: number = Date.now();
  version: string = environment.VERSION;
  verify = false;

  constructor(private route: ActivatedRoute, private router: Router, private service: AppService, private fb: FormBuilder,
              private _location: Location, private toastr: ToastrService, private auth: AuthService) { }

  ngOnInit() {
    this.dataUser = JSON.parse(this.auth.getToken());
    this.sub = this.route.queryParams.subscribe(params => {
      this.verify = params['verify'];
    });
    this.code = this.route.snapshot.paramMap.get('code');
    console.log(this.code);
  }

  doVerification() {
    this.service.doVerification(this.code).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success('Data anda telah berhasil di verifikasi, tunggu balasan email selanjutnya. Terimakasih', 'Success!');
        this.goToLogin();
      } else {
        this.toastr.error('Proses Verifikasi Gagal!', 'Ooops!');
        this.goToLogin();
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  goToLogin() {
    // this.router.navigate(['/login']);
    // if(navigator.userAgent.toLowerCase().indexOf('iphone') > -1) {
    // window.location.href = 'https://itunes.apple.com/my/app/flipbizz/idexampleapp'; }

    if (navigator.userAgent.toLowerCase().indexOf('android') > -1) {
      window.location.href = 'https://play.google.com/store/apps/details?id=id.or.kopbi.solusi.mobile&hl=in';
    }

    // Update #2
    // if (!navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
    if (!navigator.userAgent.match(/(Android|BlackBerry|IEMobile)/)) {
      this.router.navigate(['/login']);
    }
  }

  backClicked() {
    this._location.back();
  }

}
