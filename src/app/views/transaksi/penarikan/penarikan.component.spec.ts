import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenarikanComponent } from './penarikan.component';

describe('PenarikanComponent', () => {
  let component: PenarikanComponent;
  let fixture: ComponentFixture<PenarikanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenarikanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenarikanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
