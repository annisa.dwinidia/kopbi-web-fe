import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import {Pengajuan} from '../../../model/pengajuan';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppService} from '../../../service/app.service';
import {DatePipe} from '@angular/common';
import {User} from '../../../model/user';
import {AuthService} from '../../../service/auth.service';
import {Perusahaan} from "../../../model/Perusahaan";
import {Anggota} from "../../../model/anggota";
import {Barang} from "../../../model/Barang";
import {configKOPBI} from '../../../config/application-config';
import {Simpanan} from '../../../model/Simpanan';
import {Bank} from '../../../model/bank';
declare let $: any;

@Component({
  selector: 'app-tmb-simpanan',
  templateUrl: './tmb-simpanan.component.html',
  styleUrls: ['./tmb-simpanan.component.scss'],
  providers: [DatePipe]
})
export class TmbSimpananComponent implements OnInit {

  formBuilder: FormGroup;
  dataUser: User;

  loading = false;
  addModal;
  selectedPengajuan = new Pengajuan();
  selectedPerusahaan;
  selectedAnggota;
  selectedItem;
  selectedLamaAngsuran;
  selectedJenisAngsuran;

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';
  tipePengajuan     = '';

  listPengajuan:     Pengajuan[] = [];
  listPerusahaan: Perusahaan[] = [];
  listAnggota: Anggota[] = [];
  listLamaAngsuran: any;
  listSimulasi: any[] = [];
  listItem: Barang[] = [];
  listJenisAngsuran: any[] = [];
  listBank: Bank[] = [];
  saldoSimpanan;

  configSelectPerusahaan = null;
  configSelectAnggota = null;
  totalAngsuran = 0;
  totalBunga = 0;
  totalBiayaAdmin = 0;
  totalTagihan = 0;

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;

  now = new Date();
  currentMonth;
  currentYear;
  isDesc = true;
  column: string;
  direction: number;
  selectedName = '';

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    $('select').selectpicker();
    this.dataUser = JSON.parse(this.auth.getToken());
    this.currentYear = this.now.getFullYear();
    this.currentMonth = this.now.getMonth() + 1;
    this.configSelectPerusahaan = {
      displayKey: 'namaPerusahaan',
      search: true,
      height: 'auto',
      placeholder: 'Pilih Perusahaan',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Perusahaan..',
      // searchOnKey: 'departmentCode'
    };
    this.configSelectAnggota = {
      displayKey: 'nama',
      search: true,
      height: 'auto',
      placeholder: 'Pilih Anggota',
      customComparator: () => {},
      limitTo: 5,
      moreText: 'more',
      noResultsFound: 'Data Tidak Ditemukan!',
      searchPlaceholder: 'Cari Anggota..',
      // searchOnKey: 'departmentCode'
    };
    this.listLamaAngsuran = Array(24).fill([], 0, 24).map((x, i) => i + 1);
    this.formBuilder = this.fb.group({
      namaSimpanan: [''],
      kodeSimpanan: ['', Validators.required],
      nominalSimpanan: ['', Validators.required],
      jenisIuran: ['', Validators.required],
      ketIuran: [''],
      kodeMediaIuran: ['', Validators.required],
      mediaIuran: [''],
      bulan: [''],
      tahun: [''],
      keterangan: ['', Validators.required],
    });
    this.doGetListCompany();
    this.formBuilder.get('kodeSimpanan').valueChanges.subscribe(data => {
      $('select').selectpicker();
    });
    this.doGetListBank();
  }

  doGetListMember() {
    // this.loading = true;
    const param = {
      kodePerusahaan: !this.selectedPerusahaan ? '' : JSON.parse(this.selectedPerusahaan).kodePerusahaan,
      search: this.selectedName,
      mulai: 1,
      akhir: 50,
      status: ''
    };
    this.service.getListAllAnggota(param).subscribe((res) => {
    //   const perusahaan = !this.selectedPerusahaan ? '' : JSON.parse(this.selectedPerusahaan).kodePerusahaan;
    // this.service.getListAnggotaPerusahaan(perusahaan, 1, 100).subscribe((res) => {
    //   this.loading = false;
      const datas = JSON.parse(res.data);
      this.listAnggota = _.sortBy(datas.data, ['id']);
    }, (err) => {
      // this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Anggota!', 'Ooops!');
    });
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetListBank() {
    this.loading = true;
    const param = {
      search: ''
    };
    this.service.getListBank(param).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listBank = data;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Bank!', 'Ooops!');
    });
  }

  doGetSaldoSimpanan(member) {
    this.loading = true;
    this.saldoSimpanan = null;
    const nomorNik = member.nomorAnggota;
    this.service.getSaldoSimpanan(nomorNik, {}).subscribe((res) => {
      this.loading = false;
      if (res.data) {
        const datas = JSON.parse(res.data);
        this.saldoSimpanan = datas;
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Saldo Simpanan!', 'Ooops!');
    });
  }

  doSubmitSimpanan() {
    if (this.dataUser.role === 'AGT') {
      this.doSubmitSimpananAnggota();
    } else {
      this.doSubmitSimpananAdmin();
      const kode = this.formBuilder.get('kodeSimpanan').value;
      const tipe = this.formBuilder.get('jenisIuran').value;
      if (kode === 'SPP') {
        // if (tipe === 'Setoran' && this.selectedAnggota.statusAnggota === 'N') {
        //   this.doUpdateAnggota('A');
        // } else if (tipe === 'Penarikan' && this.selectedAnggota.statusAnggota === 'A') {
        //   this.doUpdateAnggota('I');
        // }
        if (tipe === 'Setoran') {
          this.doUpdateAnggota('A');
        } else if (tipe === 'Penarikan') {
          this.doUpdateAnggota('I');
        }
      }
    }
  }

  doSubmitSimpananAnggota() {
    const param: Simpanan = new Simpanan();
    param.tanggalSimpanan = new Date();
    param.nomorKtp =  this.dataUser.nomorKtp;
    param.nomorAnggota = this.dataUser.nomorAnggota;
    param.nama = this.dataUser.nama;
    param.nomorNik = this.dataUser.nomorNik;
    param.nomorHp = this.dataUser.nomorHp;
    param.kodePerusahaan = this.dataUser.kodePerusahaan;
    param.namaPerusahaan = this.dataUser.namaPerusahaan;
    param.alamatPerusahaan = this.dataUser.alamatPerusahaan;
    param.emailPerusahaan = this.dataUser.emailPerusahaan;
    param.kodeSimpanan = this.formBuilder.get('kodeSimpanan').value;
    param.namaSimpanan = this.formBuilder.get('namaSimpanan').value;
    param.nominalSimpanan = Number(this.formBuilder.get('nominalSimpanan').value.toString().replace(new RegExp(',', 'g'), ''));
    param.jenisIuran = this.formBuilder.get('jenisIuran').value;
    param.ketIuran = this.formBuilder.get('ketIuran').value;
    param.kodeMediaIuran = this.formBuilder.get('kodeMediaIuran').value;
    param.mediaIuran = this.formBuilder.get('mediaIuran').value;
    param.bulan = this.formBuilder.get('bulan').value;
    param.tahun = this.formBuilder.get('tahun').value;
    param.keterangan = this.formBuilder.get('keterangan').value;
    param.kodeUser = this.dataUser.kodeAnggota;
    param.namaUser = this.dataUser.nama;
    param.tanggalUpdate = new Date();
    param.statusAktif = 'AKTIF';
    this.service.doPostSimpanan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        if (this.dataUser.role === 'AGT') {
          this.router.navigate(['/home/simpanan/anggota']);
        } else {
          this.router.navigate(['/home/simpanan']);
        }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  doSubmitSimpananAdmin() {
    const param: Simpanan = new Simpanan();
    // const selectedAnggota = JSON.parse(this.selectedAnggota);
    const selectedAnggota = this.selectedAnggota;
    const selectedPerusahaan = JSON.parse(this.selectedPerusahaan);
    param.tanggalSimpanan = new Date();
    param.nomorKtp =  selectedAnggota.nomorKtp;
    param.nomorAnggota = selectedAnggota.nomorAnggota;
    param.nama = selectedAnggota.nama;
    param.nomorNik = selectedAnggota.nomorNik;
    param.nomorHp = selectedAnggota.nomorHp;
    param.kodePerusahaan = selectedAnggota.kodePerusahaan;
    param.namaPerusahaan = selectedAnggota.namaPerusahaan;
    param.alamatPerusahaan = selectedAnggota.alamatPerusahaan;
    param.emailPerusahaan = selectedAnggota.emailPerusahaan;
    param.kodeSimpanan = this.formBuilder.get('kodeSimpanan').value;
    param.namaSimpanan = this.formBuilder.get('namaSimpanan').value;
    param.nominalSimpanan = Number(this.formBuilder.get('nominalSimpanan').value.toString().replace(new RegExp(',', 'g'), ''));
    param.jenisIuran = this.formBuilder.get('jenisIuran').value;
    param.ketIuran = this.formBuilder.get('ketIuran').value;
    param.kodeMediaIuran = this.formBuilder.get('kodeMediaIuran').value;
    param.mediaIuran = this.formBuilder.get('mediaIuran').value;
    param.bulan = this.formBuilder.get('bulan').value;
    param.tahun = this.formBuilder.get('tahun').value;
    param.keterangan = this.formBuilder.get('keterangan').value;
    param.kodeUser = this.dataUser.kodeAnggota;
    param.namaUser = this.dataUser.nama;
    param.tanggalUpdate = new Date();
    param.statusAktif = 'AKTIF';
    this.service.doPostSimpanan(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.formBuilder.reset();
        this.toastr.success(this.successMessage, 'Success!');
        if (this.dataUser.role === 'AGT') {
          this.router.navigate(['/home/simpanan/anggota']);
        } else {
          this.router.navigate(['/home/simpanan']);
        }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  doUpdateAnggota(status) {
    const param = this.selectedAnggota;
    param.statusAnggota = status;
    this.service.doPostAnggota(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.doGetListMember();
        this.toastr.success(this.successMessage, 'Success!');
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  setNamaSimpanan() {
    const value = this.formBuilder.get('kodeSimpanan').value;
    if (value === 'SPP') {
      this.formBuilder.get('namaSimpanan').setValue('Simpanan Pokok');
    } else if (value === 'SPW') {
      this.formBuilder.get('namaSimpanan').setValue('Simpanan Wajib');
    } else if (value === 'SPS') {
      this.formBuilder.get('namaSimpanan').setValue('Simpanan Sukarela');
    }
  }

  setKeterangan() {
    const value = this.formBuilder.get('jenisIuran').value;
    if (value === 'Setoran') {
      this.formBuilder.get('ketIuran').setValue('D');
    } else if (value === 'Penarikan') {
      this.formBuilder.get('ketIuran').setValue('K');
    }

    const kodeBank = this.formBuilder.get('kodeMediaIuran').value;
    const bank = this.listBank.find(x => x.kodeBank === kodeBank);
    this.formBuilder.get('mediaIuran').setValue(bank.namaBank);
  }

  backClicked() {
    if (this.dataUser.role === 'AGT') {
      this.router.navigate(['/home/simpanan/anggota']);
    } else {
      this.router.navigate(['/home/simpanan']);
    }
  }

  onMemberChanged(val: string) {
    const name = val.split(' - ');
    console.log(name[1]);
    if (this.listAnggota.length > 0) {
      const selectedMember = this.getSelectedMemberByNik(name[1]);
      this.selectedAnggota = selectedMember;
      if (this.selectedAnggota !== null) {
        this.doGetSaldoSimpanan(selectedMember);
      }
    }
    // console.log(this.selectedAnggota);
  }

  getSelectedMemberByNik(selectedNik: string) {
    return this.listAnggota.find(member => member.nomorNik === selectedNik);
  }
}
