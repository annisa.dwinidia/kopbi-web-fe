import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmbSimpananComponent } from './tmb-simpanan.component';

describe('TmbSimpananComponent', () => {
  let component: TmbSimpananComponent;
  let fixture: ComponentFixture<TmbSimpananComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmbSimpananComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmbSimpananComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
