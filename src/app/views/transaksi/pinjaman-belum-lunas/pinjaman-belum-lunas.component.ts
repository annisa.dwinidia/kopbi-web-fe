import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Pengajuan} from '../../../model/pengajuan';
import {AppService} from '../../../service/app.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import {User} from "../../../model/user";
import {AuthService} from "../../../service/auth.service";
import {Pinjaman} from '../../../model/Pinjaman';
import {Perusahaan} from '../../../model/Perusahaan';

@Component({
  selector: 'app-pinjaman-belum-lunas',
  templateUrl: './pinjaman-belum-lunas.component.html',
  styleUrls: ['./pinjaman-belum-lunas.component.scss'],
  providers: [DatePipe]
})
export class PinjamanBelumLunasComponent implements OnInit {

  formBuilder: FormGroup;
  dataUser: User;

  loading = false;

  addModal;

  selectedPinjaman = new Pinjaman();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';

  listPinjaman:     Pinjaman[] = [];
  listPerusahaan:     Perusahaan[] = [];
  listLamaAngsuran: any;

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;
  tanggalMulai = '';
  tanggalAkhir = '';
  kodePerusahaan = '';
  selectedPerusahaan;
  currentMonth = new Date().getMonth() + 1;
  currentYear = new Date().getFullYear();

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    this.formBuilder = this.fb.group({
      angsuranKe: '',
      nominalAngsuran: '',
      totalBayar: ''
    });
    // this.tanggalMulai = this.datePipe.transform(new Date(this.currentYear + '-' + this.currentMonth + '-01'), 'yyyy-MM-dd');
    // this.tanggalAkhir = this.datePipe.transform(new Date(this.currentYear, this.currentMonth, 0), 'yyyy-MM-dd');
    this.doGetListPinjaman();
    this.doGetListCompany();
  }

  doGetListCompany() {
    this.loading = true;
    this.service.getListPerusahaan().subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listPerusahaan = _.orderBy(data, 'namaPerusahaan', 'asc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Perusahaan!', 'Ooops!');
    });
  }

  doGetListPinjaman() {
    this.loading = true;
    this.dataUser = JSON.parse(this.auth.getToken());
    const param: any = {
      tanggalMulai: this.tanggalMulai,
      tanggalAkhir: this.tanggalAkhir,
      // kodePerusahaan: this.kodePerusahaan,
      // search: this.filterText,
      // statusPinjaman: 'belum',
      mulai: ((this.currentPage - 1) * this.itemsPerPage) + 1,
      akhir: this.itemsPerPage * this.currentPage,
      nomorAnggota: ''
    };
    this.service.getListPinjaman(param).subscribe((res) => {
      this.loading = false;
      const datas = JSON.parse(res.data);
      this.totalItems = datas.totalRow;
      const data = datas.data;
      // const list = _.filter(data, ['statusPinjaman', 'Lunas']);
      this.listPinjaman =   _.orderBy(data, function(dateObj) {
        return new Date(dateObj.tanggalPengajuan);
      }, 'desc');
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Pinjaman!', 'Ooops!');
    });
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  gotoNew(id: number) {
    // this.router.navigate(['/transaksi/pengajuan/tmb-pengajuan'], { queryParams: { id: id }, skipLocationChange: false });
    this.router.navigate(['/home/pinjaman/new']);
  }

  gotoList(nik: number, name: string) {
    this.router.navigate(['/home/pinjaman/anggota'], { queryParams: { nik: nik, name: name }, skipLocationChange: false });
  }

  setSelectedPinjaman(data: Pinjaman, title) {
    this.selectedPinjaman = data;
    this.modalTitle = title;
  }

  setDataAngsuran() {
    const totalAngsuran = Number(this.selectedPinjaman.lamaAngsuran);
    this.listLamaAngsuran = Array(totalAngsuran).fill([], 0, totalAngsuran).map((x, i) => i + 1);
    this.formBuilder.get('nominalAngsuran').setValue(numeral(Number(this.selectedPinjaman.nominalAngsuran)).format('0,0'));
  }

  submitAngsuran() {
    const param = {
      nomorPinjaman: this.selectedPinjaman.nomorPinjaman,
      tanggalJatuhTempo: this.selectedPinjaman.tanggalJatuhTempo,
      angsuranKe: this.formBuilder.get('angsuranKe').value,
      nominalAngsuran: this.selectedPinjaman.nominalAngsuran,
      persenBunga: this.selectedPinjaman.persenBunga,
      nominalBunga: this.selectedPinjaman.nominalBunga,
      totalBunga: this.selectedPinjaman.totalBunga,
      biayaAdmin: this.selectedPinjaman.biayaAdmin,
      totalBayar: this.formBuilder.get('totalBayar').value.toString().replace(new RegExp(',', 'g'), ''),
      status: 'PAID',
    };
    this.service.doPostAngsuran(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.gotoListAngsuran(this.selectedPinjaman.nomorPinjaman, this.dataUser.nama);
        // if (this.dataUser.role === 'AGT') {
        //     this.router.navigate(['/pinjaman/anggota']);
        // } else {
        //     this.router.navigate(['/pinjaman']);
        // }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  submit() {
    const param: any = this.selectedPinjaman;
    param.tanggalUpdate = new Date();
    this.service.doPostPinjaman(param).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListPinjaman();
        // if (this.dataUser.role === 'AGT') {
        //     this.router.navigate(['/pinjaman/anggota']);
        // } else {
        //     this.router.navigate(['/pinjaman']);
        // }
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  gotoListAngsuran(nomorPinjaman, nama) {
    this.router.navigate(['/home/pinjaman/angsuran'], { queryParams: { nomorPinjaman: nomorPinjaman, name: nama }, skipLocationChange: false });
  }
}
