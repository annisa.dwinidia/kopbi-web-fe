import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PinjamanBelumLunasComponent } from './pinjaman-belum-lunas.component';

describe('PinjamanBelumLunasComponent', () => {
  let component: PinjamanBelumLunasComponent;
  let fixture: ComponentFixture<PinjamanBelumLunasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PinjamanBelumLunasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PinjamanBelumLunasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
