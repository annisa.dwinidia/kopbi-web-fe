import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Pengajuan} from '../../../model/pengajuan';
import {AppService} from '../../../service/app.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import * as _ from 'lodash';
import * as numeral from 'numeral';
import {User} from "../../../model/user";
import {AuthService} from "../../../service/auth.service";
import {Pinjaman} from '../../../model/Pinjaman';
import {Angsuran} from '../../../model/Angsuran';
import { Location } from '@angular/common';
import {Bank} from '../../../model/bank';
declare let $: any;
@Component({
  selector: 'app-angsuran',
  templateUrl: './angsuran.component.html',
  styleUrls: ['./angsuran.component.scss'],
  providers: [DatePipe]
})
export class AngsuranComponent implements OnInit {

  formBuilder: FormGroup;

  loading = false;

  sub: any;
  nomorPinjaman;
  name;
  dataPinjaman;
  dataUser: User;
  addModal;

  selectedAngsuran = new Angsuran();

  modalTitle        = '';
  successMessage    = 'Data berhasil disimpan';
  errorMesssage     = 'Terjadi kesalan dalam penyimpanan data!';
  filterText        = '';
  sortOrder         = 'desc';

  listAngsuran:     Angsuran[] = [];
  listBank:         Bank[] = [];

  totalItems: number;
  totalFiltered: number;
  currentPage = 1;
  itemsPerPage = 10;
  sisaPinjaman = 0;
  sisaAngsuran = 0;

  isDesc = true;
  column: string;
  direction: number;

  constructor(private service: AppService, private toastr: ToastrService, private fb: FormBuilder,
              private datePipe: DatePipe, private router: Router, private route: ActivatedRoute,
              private auth: AuthService, private _location: Location) { }

  ngOnInit() {
    // $('select').selectpicker();
    this.dataUser = JSON.parse(this.auth.getToken());
    this.sub = this.route.queryParams.subscribe(params => {
      this.nomorPinjaman = params['nomorPinjaman'];
      this.name = params['name'];
      this.dataPinjaman = JSON.parse(params['data']);
    });
    this.formBuilder = this.fb.group({
      angsuranKe: '',
      nominalAngsuran: ['', Validators.required],
      totalBayar: [{value: '', disabled: true}, Validators.required],
      tanggalBayar: ['', Validators.required],
      kodeUntukKas: ['', Validators.required],
      untukKas: ''
    });
    this.doGetListAngsuran();
    // this.getSisaAngsuran(this.listAngsuran);
    this.doGetListBank();
  }

  doGetListBank() {
    this.loading = true;
    const param = {
      search: ''
    };
    this.service.getListBank(param).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listBank = data;
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Bank!', 'Ooops!');
    });
  }

  doGetListAngsuran() {
    this.loading = true;
    this.service.getListAngsuran(this.nomorPinjaman).subscribe((res) => {
      this.loading = false;
      const data = JSON.parse(res.data);
      this.listAngsuran = data;
      this.getSisaAngsuran(data);
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan dalam pengambilan data Angsuran!', 'Ooops!');
    });
  }

  rounding(nominal) {
    return Math.round(parseFloat(nominal.toString()) / 100) * 100;
  }

  getSisaAngsuran(res) {
    this.sisaPinjaman = 0;
    this.sisaAngsuran = 0;
    for (let x = 0; x < res.length; x++) {
      if (res[x].status !== 'PAID') {
        this.sisaAngsuran += 1;
        this.sisaPinjaman += parseFloat(res[x].totalBayar.toString());
        this.listAngsuran[x].angsuranPokok = this.rounding(parseFloat(this.dataPinjaman.nominalPinjaman.toString()) /
        parseFloat(this.dataPinjaman.lamaAngsuran.toString())).toString();
      } else if (res[x].status === 'PAID') {
        this.listAngsuran[x].angsuranPokok = this.rounding(parseFloat(this.dataPinjaman.nominalPinjaman.toString()) /
          parseFloat(this.dataPinjaman.lamaAngsuran.toString())).toString();
      }
    }
  }


  setSelectedAngsuran(data: Angsuran) {
    this.selectedAngsuran = data;
  }

  submitAngsuran() {
    const nominalAngsuran = this.formBuilder.get('nominalAngsuran').value.toString().replace(new RegExp(',', 'g'), '');
    const sisaPinjaman = this.sisaPinjaman - nominalAngsuran;
    const sisaAngsuran = this.sisaAngsuran - 1;
    let statusPinjaman;
    if (sisaAngsuran === 0) {
      statusPinjaman = 'LUNAS';
    } else {
      statusPinjaman = 'BELUM';
    }
    const angsuran = {
      kodeAngsuran: this.selectedAngsuran.kodeAngsuran,
      nomorPinjaman: this.selectedAngsuran.nomorPinjaman,
      tanggalJatuhTempo: this.selectedAngsuran.tanggalJatuhTempo,
      angsuranKe: this.selectedAngsuran.angsuranKe,
      nominalAngsuran: nominalAngsuran,
      persenBunga: this.selectedAngsuran.persenBunga,
      nominalBunga: this.selectedAngsuran.nominalBunga,
      totalBunga: this.selectedAngsuran.totalBunga,
      biayaAdmin: this.selectedAngsuran.biayaAdmin,
      totalBayar: this.selectedAngsuran.totalBayar,
      status: 'PAID',
      tanggalBayar: this.formBuilder.get('tanggalBayar').value,
      kodeUntukKas: this.formBuilder.get('kodeUntukKas').value,
      ketUntukKas: this.formBuilder.get('untukKas').value,
    };
    const pinjaman = {
      kodePinjaman: this.dataPinjaman.kodePinjaman,
      tanggalPengajuan: this.dataPinjaman.tanggalPengajuan,
      nomorPinjaman: this.dataPinjaman.nomorPinjaman,
      kodeTipePengajuan: this.dataPinjaman.kodeTipePengajuan,
      tipePengajuan: this.dataPinjaman.tipePengajuan,
      nomorKtp: this.dataPinjaman.nomorKtp,
      nomorAnggota: this.dataPinjaman.nomorAnggota,
      nama: this.dataPinjaman.nama,
      nomorNik: this.dataPinjaman.nomorNik,
      nomorHp: this.dataPinjaman.nomorHp,
      kodePerusahaan: this.dataPinjaman.kodePerusahaan,
      namaPerusahaan: this.dataPinjaman.namaPerusahaan,
      alamatPerusahaan: this.dataPinjaman.alamatPerusahaan,
      emailPerusahaan: this.dataPinjaman.emailPerusahaan,
      lamaAngsuran: this.dataPinjaman.lamaAngsuran,
      nominalAngsuran: nominalAngsuran,
      persenBunga: this.dataPinjaman.totalBunga,
      nominalBunga: this.dataPinjaman.nominalBunga,
      totalBunga: this.dataPinjaman.totalBunga,
      biayaAdmin: this.dataPinjaman.biayaAdmin,
      kodeBarang: this.dataPinjaman.kodeBarang,
      namaBarang: this.dataPinjaman.namaBarang,
      ambilDariKas: this.dataPinjaman.ambilDariKas,
      ketKas: this.dataPinjaman.ketKas,
      keterangan: this.dataPinjaman.keterangan,
      statusPinjaman: statusPinjaman,
      tanggalPerubahan: new Date(),
      tanggalTempo: this.dataPinjaman.tanggalTempo,
      tanggalJatuhTempo: this.dataPinjaman.tanggalJatuhTempo,
      sisaPinjaman: sisaPinjaman,
      angsuranKe: this.selectedAngsuran.angsuranKe,
      sisaAngsuran: sisaAngsuran,
      kodeUser: this.dataPinjaman.kodeUser,
      namaUser: this.dataPinjaman.namaUser,
      tanggalUpdate: new Date(),
      nominalPinjaman: this.dataPinjaman.nominalPinjaman,
    };

    // const param = {
    //   paramAngsuran, paramPinjaman
    // };
    // const param = [];
    // param.push(paramAngsuran);
    // param.push(paramPinjaman);
    // console.log(angsuran);
    this.service.doPostAngsuran(angsuran).subscribe((res) => {
      this.loading = false;
      if (res.success) {
        this.toastr.success(this.successMessage, 'Success!');
        this.doGetListAngsuran();
      } else {
        this.toastr.error(this.errorMesssage, 'Ooops!');
      }
    }, (err) => {
      this.loading = false;
      this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
    });
  }

  setDataAngsuran() {
    this.formBuilder.get('totalBayar').setValue(numeral(Number(this.selectedAngsuran.totalBayar)).format('0,0'));
  }

  setKeterangan() {
    const kodeBank = this.formBuilder.get('kodeUntukKas').value;
    const bank = this.listBank.find(x => x.kodeBank === kodeBank);
    this.formBuilder.get('untukKas').setValue(bank.namaBank);
  }


  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }

  backClicked() {
    this._location.back();
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    this.doGetListAngsuran();
  }

  gotoNew(id: number) {
    // this.router.navigate(['/home/transaksi/pengajuan/tmb-pengajuan'], { queryParams: { id: id }, skipLocationChange: false });
    // if (this.dataUser.role === 'AGT') {
    //   this.router.navigate(['/home/pinjaman/anggota/new']);
    // } else {
    this.router.navigate(['/home/pinjaman/angsuran/new']);
    // }
  }
}
