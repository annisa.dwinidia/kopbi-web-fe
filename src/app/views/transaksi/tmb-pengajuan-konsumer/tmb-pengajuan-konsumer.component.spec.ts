import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TmbPengajuanKonsumerComponent } from './tmb-pengajuan-konsumer.component';

describe('TmbPengajuanKonsumerComponent', () => {
  let component: TmbPengajuanKonsumerComponent;
  let fixture: ComponentFixture<TmbPengajuanKonsumerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TmbPengajuanKonsumerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TmbPengajuanKonsumerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
