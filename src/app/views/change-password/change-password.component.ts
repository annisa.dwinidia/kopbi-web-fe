import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../model/user';
import { HttpHeaders } from '@angular/common/http';
import { Observable, timer} from 'rxjs/';
import { config } from '../../config/application-config';
import { Title } from '@angular/platform-browser';
import { AppService } from '../../service/app.service';
import { AuthService } from '../../service/auth.service';
import { environment } from '../../../environments/environment';
import { AESEncryptDecryptService } from '../../service/aesencrypt-decrypt-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-change-password',
  templateUrl: 'change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  @ViewChild('userPasswordNewInput')
  userPasswordNewInput: ElementRef;

  @ViewChild('userPasswordInput')
  userPasswordInput: ElementRef;

  pwNew = '';
  pw = '';
  token: string;
  isErrorFound = false;
  today: number = Date.now();
  loading = false;
  errorMessage;

  dataUser: any;

  version: string = environment.VERSION;

  constructor(private router: Router, private projectService: AppService, private titleService: Title,
              private auth: AuthService, private encryption: AESEncryptDecryptService, private toastr: ToastrService) {}

  ngOnInit() {
    this.dataUser = JSON.parse(this.auth.getToken());
  }

  doSetFocusUserPasswd() {
    timer(30).subscribe(() => {
      this.userPasswordNewInput.nativeElement.focus();
    });
  }

  doLogin() {
    if (this.pw !== this.pwNew) {
      this.toastr.error('Konfirmasi Password Baru tidak sesuai', 'Ooops!');
    } else {
      const param: any = {
        userName: this.dataUser.role === 'HRD' ? this.dataUser.emailPerusahaan : this.dataUser.nomorHp,
        oldPassword: this.encryption.encrypt('123456'),
        password: this.encryption.encrypt(this.pw)
      };
      this.projectService.changePassword(param).subscribe((res) => {
        this.loading = false;
        if (res.success) {
          this.toastr.success('Password Berhasil Diganti', 'Success!');
        } else {
          this.toastr.error('Penggantian Password Gagal!! Silahkan coba lagi!', 'Ooops!');
        }
        this.router.navigate(['/home/dashboard'], { queryParams: { imageShow: 'true'}, skipLocationChange: false });
      }, (err) => {
        this.loading = false;
        this.toastr.error('Terjadi kesalahan koneksi!', 'Ooops!');
      });
    }
  }
}

