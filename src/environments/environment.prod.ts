declare var require: any;

export const environment = {
  production: true,
  VERSION: require('../../package.json').version,
  versionCheckURL: window.location.origin + '/version.json'
};
